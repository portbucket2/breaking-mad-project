﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FishSpace;


public class ScoreFlyCanvasScript : MonoBehaviour {
    public Color heroColor;
    public Color heroComboColor;
    public Color enemyColor;
    public Text text;
    public void Init(ScoreFlyType sft, int score)
    {
        switch (sft)
        {
            case ScoreFlyType.PLAYER:
                text.color = heroColor;
                break;
            case ScoreFlyType.PLAYER_COMBO:
                text.color = heroComboColor;
                break;
            case ScoreFlyType.ENEMY:
                text.color = enemyColor;
                break;
        }

        if (sft == ScoreFlyType.PLAYER_COMBO)
        {
                text.text = string.Format("bonus +{0}", score);
        }
        else
        {
            if (score > 0)
                text.text = string.Format("+{0}", score);
            else
                text.text = string.Format("{0}", score);
        }


        Centralizer.Add_DelayedMonoAct(this,
            () => {
                if (this.gameObject) FishSpace.Pool.Destroy(this.gameObject);
            }, 2);
    }
    public enum ScoreFlyType
    {
        PLAYER,
        PLAYER_COMBO,
        ENEMY,
    }
}
