﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboEffectScript : MonoBehaviour {
    public List<Sprite> sprites;
    public SpriteRenderer rend;

    // Use this for initialization
    void Init (int x)
    {
        int i = x - ComboManager.MIN_COMBO;
        if (i < 0 || i >= sprites.Count)
        {
            return;
        }
        else
        {
            rend.sprite = sprites[i];
        }
	}
}
