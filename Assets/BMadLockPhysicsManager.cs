﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BMadLockPhysicsManager : MonoBehaviour {

    Rigidbody rgbd;
    Vector3 startPos;
    Quaternion startRot;
    // Use this for initialization
    bool initialized = false;
	void Start () {
        Init();
	}
    private void Init()
    {
        if (!initialized)
        {
            startPos = this.transform.localPosition;
            startRot = this.transform.localRotation;
            rgbd = this.GetComponent<Rigidbody>();
            initialized = true;
        }
    }

    public void Drop()
    {
        rgbd.isKinematic = false;
        rgbd.AddTorque(0, 0, Random.Range(-50, 50));
    }

    public void Reset()
    {
        if (!initialized) Init();

        this.transform.localPosition = startPos;
        this.transform.localRotation = startRot;
        rgbd.isKinematic = true;
    }
    // Update is called once per frame
    void FixedUpdate () {
        if (!rgbd.isKinematic)
        {
            rgbd.AddForce( new Vector3(0,GlobalSettings.instance.forceAmountInDownPhysics,0),ForceMode.Acceleration);
        }
	}
}
