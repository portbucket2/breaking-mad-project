﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getButterfly : MonoBehaviour {

    public GameObject butterfly;
    private GameObject go;

	// Use this for initialization
	void Start () {

        go = Instantiate(butterfly, transform.position, transform.rotation) as GameObject;
        go.transform.SetParent(transform);
        float r = Random.Range(1.0f, 1.5f);
        go.transform.localScale = new Vector3(r, r,r);
		
	}
	
	// Update is called once per frame
	void Update () {

        butterfly.transform.position = gameObject.transform.position;
		
	}
}
