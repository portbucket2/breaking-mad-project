﻿// Simple script that saves frames from the Game View when on play mode
//
// You can put later the frames togheter and create a video.
// Note: The frames are saved next to the Assets folder.

//using UnityEngine;
//using UnityEditor;

//public class DestroyAllParticleSys : EditorWindow
//{
//    string recordButton = "Destroy";
//    [SerializeField]public int someVar;
//    [MenuItem("DD/FF")]
//    static void Init()
//    {
//        DestroyAllParticleSys window =
//            (DestroyAllParticleSys)EditorWindow.GetWindow(typeof(DestroyAllParticleSys));
//    }

//    void OnGUI()
//    {
//        if (GUILayout.Button(recordButton))
//        {
//            ParticleSystem[] allSys = FindObjectsOfType<ParticleSystem>();
//            foreach (var item in allSys)
//            {
//                DestroyImmediate(item);
//            }
//        }
//    }
//}