﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ScriptableCreator : MonoBehaviour
{
    [MenuItem("Assets/Create/PowerUpProfiles")]
    public static void PowerUpProfiles()
    {
        PowerUpProfiles so = ScriptableObject.CreateInstance<PowerUpProfiles>();
        AssetDatabase.CreateAsset(so, string.Format("Assets/GameAssets/Data/PowerUpProfiles.asset"));
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = so;
    }

    [UnityEditor.MenuItem("Assets/Create/LevelAssets")]
    public static void LevelAssets()
    {
        LevelAssets so = ScriptableObject.CreateInstance<LevelAssets>();
        UnityEditor.AssetDatabase.CreateAsset(so, string.Format("Assets/GameAssets/Data/LevelAssets.asset"));
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }
    [UnityEditor.MenuItem("Assets/Create/LevelProfile")]
    public static void LevelProfile()
    {
        LevelProfile so = ScriptableObject.CreateInstance<LevelProfile>();
        UnityEditor.AssetDatabase.CreateAsset(so, string.Format("Assets/GameAssets/Data/LevelProfiles/Level00.asset"));
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }
    [UnityEditor.MenuItem("Assets/Create/AudioSetting")]
    public static void AudioSetting()
    {
        AudTypeSetting so = ScriptableObject.CreateInstance<AudTypeSetting>();
        UnityEditor.AssetDatabase.CreateAsset(so, string.Format("Assets/AudioSetting000.asset"));
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }
    //[UnityEditor.MenuItem("Assets/Create/ChallengeGroup")]
    //public static void ChallengeGroup()
    //{
    //    ChallengeGroup so = ScriptableObject.CreateInstance<ChallengeGroup>();
    //    UnityEditor.AssetDatabase.CreateAsset(so, string.Format("Assets/GameAssets/Data/LevelProfiles/ChallengeGroup000.asset"));
    //    UnityEditor.AssetDatabase.SaveAssets();
    //    UnityEditor.EditorUtility.FocusProjectWindow();
    //    UnityEditor.Selection.activeObject = so;
    //}

}
