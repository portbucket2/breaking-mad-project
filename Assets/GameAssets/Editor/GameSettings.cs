﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;


// Simple Script that saves and loads custom
// Stand-alone/Web player screen settings among
// Unity Projects

class GameSettings : EditorWindow
{
    class SymbolData
    {
        public string symUIname, symbolName;
        public bool symbolValue;
        public SymbolData(ref bool symbol)
        {
            symbolValue = symbol;
        }

        public void Draw()
        {
            EditorGUILayout.BeginHorizontal();
            symbolValue = EditorGUILayout.Toggle("Debug Message for " + symUIname + "?: ", symbolValue);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.TextArea("Use '" + symbolName + "' in script for conditional compilation.");
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }

        void ProcessSymbolSetting(string symbol, string curSymbols, bool symbolSwitch)
        {
            if (curSymbols == null) { curSymbols = ""; }
            ////CM_Deb"<color=green>symbol is: " + symbol + " and current symbol: " + curSymbols + " and is this switch on?: " + symbolSwitch + "</color>");
            if (symbolSwitch)
            {
                SymbolAd(symbol, curSymbols);
            }
            else
            {
                SymbolDelete(symbol, curSymbols);
            }
        }

        void SymbolDelete(string symbol, string currentlyLoadedSymbols)
        {
            string[] allStrs = Regex.Split(currentlyLoadedSymbols, ";");
            List<string> permittedStrs = new List<string>();
            foreach (var item in allStrs)
            {
                ////CM_Deb"<color=green>item is: " + item + "</color>");
                if (item.Contains(symbol) == false)
                {
                    permittedStrs.Add(item);
                }
            }

            string newSym = "";
            bool firstSet = false;
            for (int i = 0; i < permittedStrs.Count; i++)
            {
                if (permittedStrs[i] == "" || permittedStrs[i] == " " || permittedStrs[i] == ";")
                {
                    continue;
                }
                else
                {
                    if (firstSet == false)
                    {
                        firstSet = true;
                        newSym += permittedStrs[i];
                    }
                    else
                    {
                        newSym += ";" + permittedStrs[i];
                    }
                }
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, newSym);
        }

        void SymbolAd(string symbol, string currentlyLoadedSymbols)
        {
            ////CM_Deb"<color=green>symbol ad time symbols: " + currentlyLoadedSymbols + "</color>");
            if (currentlyLoadedSymbols.Contains(symbol) == false)
            {
                currentlyLoadedSymbols = currentlyLoadedSymbols == string.Empty ? (symbol) : (currentlyLoadedSymbols + ";" + symbol);
            }
            ////CM_Deb"<color=green>symbol that will be set now: " + currentlyLoadedSymbols + "</color>");
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, currentlyLoadedSymbols);
        }
    }


    bool useCloudDebug = false;
    bool useAdDebug = false;
    bool useAnalyticsDebug = false;
    bool useGPGdebug = false;
    bool useIAPdebug = false;
    bool usePlayerLog = false;
    [MenuItem("PortBliss/GameSettingsEditor")]
    static void Init()
    {
        var window = GetWindow<GameSettings>();
        window.Show();
    }

    void OnGUI()
    {
        DrawSymbolRelatedUI("useCloudDebug", "Cloud", ref useCloudDebug);
        DrawSymbolRelatedUI("useAdDebug", "Advertisement", ref useAdDebug);
        DrawSymbolRelatedUI("useAnalyticsDebug", "Analytics", ref useAnalyticsDebug);
        DrawSymbolRelatedUI("useGPGDebug", "GPG", ref useGPGdebug);
        DrawSymbolRelatedUI("useIAPDebug", "IAP", ref useIAPdebug);
        DrawSymbolRelatedUI("usePlayerLogDebug", "Player", ref usePlayerLog);
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Set"))
            SaveSettings();
        EditorGUILayout.EndHorizontal();
    }

    void SaveSettings()
    {
        ProcessSymbolSetting("useCloudDebug", PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup), useCloudDebug);
        ProcessSymbolSetting("useAdDebug", PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup), useAdDebug);
        ProcessSymbolSetting("useAnalyticsDebug", PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup), useAnalyticsDebug);
        ProcessSymbolSetting("useGPGDebug", PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup), useGPGdebug);
        ProcessSymbolSetting("useIAPDebug", PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup), useIAPdebug);
        ProcessSymbolSetting("usePlayerLogDebug", PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup), usePlayerLog);
    }

    void ProcessSymbolSetting(string symbol, string curSymbols, bool symbolSwitch)
    {
        if (curSymbols == null) { curSymbols = ""; }
        ////CM_Deb"<color=green>symbol is: " + symbol + " and current symbol: " + curSymbols + " and is this switch on?: " + symbolSwitch + "</color>");
        if (symbolSwitch)
        {
            SymbolAd(symbol, curSymbols);
        }
        else
        {
            SymbolDelete(symbol, curSymbols);
        }
    }

    void SymbolDelete(string symbol, string currentlyLoadedSymbols)
    {
        string[] allStrs = Regex.Split(currentlyLoadedSymbols, ";");
        List<string> permittedStrs = new List<string>();
        foreach (var item in allStrs)
        {
            ////CM_Deb"<color=green>item is: " + item + "</color>");
            if (item.Contains(symbol) == false)
            {
                permittedStrs.Add(item);
            }
        }

        string newSym = "";
        bool firstSet = false;
        for (int i = 0; i < permittedStrs.Count; i++)
        {
            if (permittedStrs[i] == "" || permittedStrs[i] == " " || permittedStrs[i] == ";")
            {
                continue;
            }
            else
            {
                if (firstSet == false)
                {
                    firstSet = true;
                    newSym += permittedStrs[i];
                }
                else
                {
                    newSym += ";" + permittedStrs[i];
                }
            }
        }
        PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, newSym);
    }

    void SymbolAd(string symbol, string currentlyLoadedSymbols)
    {
        ////CM_Deb"<color=green>symbol ad time symbols: " + currentlyLoadedSymbols + "</color>");
        if (currentlyLoadedSymbols.Contains(symbol) == false)
        {
            currentlyLoadedSymbols = currentlyLoadedSymbols == string.Empty ? (symbol) : (currentlyLoadedSymbols + ";" + symbol);
        }
        ////CM_Deb"<color=green>symbol that will be set now: " + currentlyLoadedSymbols + "</color>");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, currentlyLoadedSymbols);
    }

    void DrawSymbolRelatedUI(string symbol, string symbolName, ref bool symbolBL)
    {
        EditorGUILayout.BeginHorizontal();
        symbolBL = EditorGUILayout.Toggle("Debug Message for " + symbolName + "?: ", symbolBL);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.TextArea("Use '" + symbol + "' in script for conditional compilation.");
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
    }
}