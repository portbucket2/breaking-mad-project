﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup
{
    internal interface IRootPluginInit
    {
        void InitSystem();
    }

    //used to initialize different types of advertisement modules. i.e. video ad, offerwall, banner, interstitial
    internal interface IParticularAdInit
    {
        void InitThisTypeOfAd();
        void CleanupThisTypeOfAd();
    }

    internal interface IIapPurchaseInit
    {
        void InitInAppModule();
    }

    internal interface IGPGInit
    {
        void InitGPGModule();
    }

    internal interface IFullAdSystemInit
    {
        void InitAdSystem(Transform rootPluginGameObjectTransform, IronSourceSegment userSegment);
        void CleanUpAdResources();
    }
}