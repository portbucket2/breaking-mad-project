﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;

namespace Portbliss.NativePlugins.InternalDoNotMessup
{
    [System.Serializable]
    public class InAppPurchaseModule : IIapPurchaseInit, IStoreListener
    {
        internal InAppPurchaseModule()
        {

        }

        void IIapPurchaseInit.InitInAppModule()
        {
            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                // If we have already connected to Purchasing ...
                if (IsInitialized())
                {
                    // ... we are done here.
                    return;
                }

                // Create a builder, first passing in a suite of Unity provided stores.
                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

                if (useConsumable)
                {
                    // Add a product to sell / restore by way of its identifier, associating the general identifier
                    // with its store-specific identifiers.
                    foreach (var product in consumableProductList)
                    {
                        builder.AddProduct(product, ProductType.Consumable);
                    }
                }

                if (useNonConsumable)
                {
                    foreach (var product in nonConsumableProductList)
                    {
                        // Continue adding the non-consumable product.
                        builder.AddProduct(product, ProductType.NonConsumable);
                    }
                }

                if (useSubscription)
                {
                    // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
                    // if the Product ID was configured differently between Apple and Google stores. Also note that
                    // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
                    // must only be referenced here.
#if UNITY_IOS
                IDs idApple = new IDs() { { kProductNameAppleSubscription, AppleAppStore.Name } };
#elif UNITY_ANDROID
                    IDs idGoogle = new IDs() { { kProductNameGooglePlaySubscription, GooglePlay.Name } };
#endif
                    IDs selectedPlatform = null;
#if UNITY_IOS
                selectedPlatform = idApple;
#elif UNITY_ANDROID
                    selectedPlatform = idGoogle;
#endif
                    if (selectedPlatform != null)
                    {
                        foreach (var subcriptionProduct in subscriptionProductList)
                        {
                            builder.AddProduct(subcriptionProduct, ProductType.Subscription, selectedPlatform);
                        }
                    }
                }

                if (useConsumable || useNonConsumable || useSubscription)
                {
                    // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
                    // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
                    UnityPurchasing.Initialize(this, builder);
                    Debug.Log("<color=green>In App purchase system has been initialized!</color>");
                }
            }
        }

        [SerializeField] bool useLog = true, useConsumable = true, useNonConsumable = false, useSubscription = false;
        IStoreController m_StoreController;          // The Unity Purchasing system.
        IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
        // Product identifiers for all products capable of being purchased: 
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values 
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.
        [SerializeField]List<string> consumableProductList = new List<string>();
        [SerializeField]List<string> nonConsumableProductList = new List<string>();
        [SerializeField]List<string> subscriptionProductList = new List<string>();
        public event OnAction OnInAppModuleInitialize;
        // Apple App Store-specific product identifier for the subscription product.
        [SerializeField] string kProductNameAppleSubscription = "com.unity3d.subscription.new";

        // Google Play Store-specific product identifier subscription product.
        [SerializeField] string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

        OnPurchase OnPurchaseComplete = null;

        // Only say we are initialized if both the Purchasing references are set.
        bool IsInitialized() { return m_StoreController != null && m_StoreExtensionProvider != null; }
        string selectedProductID = "";
        public string GetLocalCurrencyForProductID(string ID)
        {
            if (m_StoreController == null)
            {
                return null;
            }
            else
            {
                if (m_StoreController.products == null)
                {
                    return null;
                }
                else
                {
                    Product p = m_StoreController.products.WithID(ID);
                    if (p == null)
                    {
                        return null;
                    }
                    else
                    {
                        if (p.metadata == null)
                        {
                            return null;
                        }
                        else
                        {

                            return p.metadata.isoCurrencyCode + " " + p.metadata.localizedPriceString;
                        }
                    }
                }
            }
        }

        public void BuyProduct(string productId, OnPurchase OnPurchase)
        {
            ////debug code
            //if (OnPurchase != null)
            //{
            //    OnPurchase(IAPresult.fail);
            //}
            //return;
            ////debug code end //delete it later must!!!!

            OnPurchaseComplete = OnPurchase;
            selectedProductID = productId;
            
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    if (useLog)
                        Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    if (useLog)
                        Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                    FireEventAndCleanup(IAPresult.fail);
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                if (useLog)
                    Debug.Log("BuyProductID FAIL. Not initialized.");
                FireEventAndCleanup(IAPresult.fail);
            }
        }

        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchasesForIOS()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                //if (useLog)
                    //CM_Deb("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                //if (useLog)
                    //CM_Deb("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.

                  //  if (useLog)
                     //   Debug.Log("RestorePurchases continuing: " + "" + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                //if (useLog)
                    //CM_Deb("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }

        //  
        // --- IStoreListener
        //
        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs args)
        {
            // A consumable product has been purchased by this user.
            if (IsProductOfThisType(args, consumableProductList))
            {
                if (useLog)
                    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // The consumable item has been successfully purchased
                FireEventAndCleanup(IAPresult.consumable);
            }
            // Or ... a non-consumable product has been purchased by this user.
            else if (IsProductOfThisType(args, nonConsumableProductList))
            {
                if (useLog)
                    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
                FireEventAndCleanup(IAPresult.nonConsumable);
            }
            // Or ... a subscription product has been purchased by this user.
            else if (IsProductOfThisType(args, subscriptionProductList))
            {
                if (useLog)
                    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The subscription item has been successfully purchased, grant this to the player.
                FireEventAndCleanup(IAPresult.subscription);
            }
            // Or ... an unknown product has been purchased by this user. Fill in additional products here....
            else
            {
                if (useLog)
                    Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
                FireEventAndCleanup(IAPresult.fail);
            }

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }
        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            if (useLog)
                Debug.Log("OnInitialized: PASS");
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;

            if (OnInAppModuleInitialize != null)
            {
                OnInAppModuleInitialize();
            }
        }

        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (useLog)
                Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            FireEventAndCleanup(IAPresult.fail);
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            if (useLog)
                Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
            FireEventAndCleanup(IAPresult.fail);
        }

        void FireEventAndCleanup(IAPresult result)
        {
            if (OnPurchaseComplete != null)
            {
                OnPurchaseComplete(result);
                OnPurchaseComplete = null;
                selectedProductID = "";
            }
        }

        bool IsProductOfThisType(PurchaseEventArgs args, List<string> productList)
        {
            bool isIt = false;
            foreach (var prod in productList)
            {
                if (string.IsNullOrEmpty(prod)) continue;
                if (String.Equals(args.purchasedProduct.definition.id, prod, StringComparison.Ordinal))
                {
                    isIt = true;
                    break;
                }
            }
            return isIt;
        }
    }
}