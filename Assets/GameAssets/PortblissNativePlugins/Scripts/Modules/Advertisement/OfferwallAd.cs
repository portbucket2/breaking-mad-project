﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    public partial class OfferwallAd : IParticularAdInit
    {
        OnOfferwall OnOfferwallAction;
        bool couldGetUserCurrency;
        int userCurrency;
        internal OfferwallAd()
        {
            
        }

        void FireOfferwallActionFail(OnOfferwall OnAction)
        {
            if (OnAction != null)
            {
                OnAction(false, -1);
            }
        }

        public bool TryShowOfferwallAd(OnOfferwall OnFinish, string placement = null)
        {
            bool canShow = false;
            couldGetUserCurrency = false;
            userCurrency = -1;
            if (IronSource.Agent.isOfferwallAvailable())
            {
                canShow = true;
                OnOfferwallAction = OnFinish;
                //thus we can send user's various data to our server through server to 
                //server callback setup in both ironsource portal and our own backend system
                //and we need to do this just before we fire 'showofferwall()'
                //Dictionary<string, string> owParams = new Dictionary<string, string>();
                //owParams.Add("ip", "1.0.0.9");
                //owParams.Add("status", "yo! thik ase!");
                //owParams.Add("name", "yo! name is Kaiyum");
                //IronSourceConfig.Instance.setOfferwallCustomParams(owParams);

                if (string.IsNullOrEmpty(placement))
                {
                    IronSource.Agent.showOfferwall();
                }
                else
                {
                    IronSource.Agent.showOfferwall(placement);
                }
            }
            else
            {
                OnOfferwallAction = null;
                FireOfferwallActionFail(OnOfferwallAction);
            }
            return canShow;
        }

        void IParticularAdInit.InitThisTypeOfAd()
        {
            IronSourceEvents.onOfferwallClosedEvent += OfferwallClosedEvent;
            IronSourceEvents.onOfferwallAdCreditedEvent += OfferwallAdCreditedEvent;
            couldGetUserCurrency = false;
            userCurrency = -1;

            ////CM_Deb("<color=green>Offerwall ad has been initialized!</color>");
        }

        void IParticularAdInit.CleanupThisTypeOfAd()
        {
            IronSourceEvents.onOfferwallClosedEvent -= OfferwallClosedEvent;
            IronSourceEvents.onOfferwallAdCreditedEvent -= OfferwallAdCreditedEvent;
        }
    }
}