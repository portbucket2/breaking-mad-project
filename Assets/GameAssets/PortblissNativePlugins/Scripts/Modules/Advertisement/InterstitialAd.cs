﻿using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    [System.Serializable]
    public partial class Interstitial : IParticularAdInit
    {
        OnSuccess OnInterstitialAction;
        bool didUserSeeInterstitial;
        [SerializeField] string defaultPlacementName = "DefaultRewardedVideo";
        //so that outsiders can not create an object of it.
        internal Interstitial()
        {
            
        }

        void FireCallbackIfAnyAndCleanup(bool success)
        {
            if (OnInterstitialAction != null)
            {
                OnInterstitialAction(success);
                OnInterstitialAction = null;
            }
        }

        public bool TryShowInterstitialAd(OnSuccess OnFinish, string placement = null)
        {
            bool canShow = false;
            OnInterstitialAction = OnFinish;
            didUserSeeInterstitial = false;
            if (IronSource.Agent.isInterstitialReady())
            {
                if ((IronSource.Agent.isInterstitialPlacementCapped(defaultPlacementName) && string.IsNullOrEmpty(placement)) ||
               (IronSource.Agent.isInterstitialPlacementCapped(placement) && !string.IsNullOrEmpty(placement)))
                {
                    FireCallbackIfAnyAndCleanup(didUserSeeInterstitial);
                }
                else
                {
                    canShow = true;
                    if (string.IsNullOrEmpty(placement))
                    {
                        IronSource.Agent.showInterstitial();
                    }
                    else
                    {
                        IronSource.Agent.showInterstitial(placement);
                    }
                }
            }
            else
            {
                FireCallbackIfAnyAndCleanup(didUserSeeInterstitial);
                IronSource.Agent.loadInterstitial();
            }
            return canShow;
        }

        void IParticularAdInit.InitThisTypeOfAd()
        {
            IronSourceEvents.onInterstitialAdClosedEvent += IronSourceEvents_onInterstitialAdClosedEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent += IronSourceEvents_onInterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent += IronSourceEvents_onInterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent += IronSourceEvents_onInterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdReadyEvent += IronSourceEvents_onInterstitialAdReadyEvent;
            if (IronSource.Agent.isInterstitialReady() == false)
            {
                IronSource.Agent.loadInterstitial();
            }
            didUserSeeInterstitial = false;

            ////CM_Deb("<color=green>Interstitial ad has been initialized!</color>");
        }

        void IParticularAdInit.CleanupThisTypeOfAd()
        {
            IronSourceEvents.onInterstitialAdClosedEvent -= IronSourceEvents_onInterstitialAdClosedEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent -= IronSourceEvents_onInterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent -= IronSourceEvents_onInterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent -= IronSourceEvents_onInterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdReadyEvent -= IronSourceEvents_onInterstitialAdReadyEvent;
        }
    }
}