﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    public partial class BannerAd
    {

        //Invoked once the banner has loaded
        void BannerAdLoadedEvent()
        {
            isLoaded = true;
            FireBannerAction(true);
        }

        //Invoked when the banner loading process has failed.
        //@param description - string - contains information about the failure.
        void BannerAdLoadFailedEvent(IronSourceError error)
        {
            isLoaded = false;
            FireBannerAction(false);
        }
    }
}