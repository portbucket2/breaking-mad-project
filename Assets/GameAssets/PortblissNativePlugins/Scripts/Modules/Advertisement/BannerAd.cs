﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    [System.Serializable]
    public partial class BannerAd : IParticularAdInit
    {
        OnSuccess OnBannerAction;
        bool isLoaded;
        [SerializeField] string defaultPlacementName = "DefaultBanner";

        internal BannerAd()
        {
            
        }

        void FireBannerAction(bool success)
        {
            if (OnBannerAction != null)
            {
                OnBannerAction(success);
            }
            OnBannerAction = null;
        }

        public bool TryShowBannerAd(IronSourceBannerSize size, IronSourceBannerPosition position, bool willAlwaysTryNewAd, OnSuccess OnFinish, string placement = null)
        {
            bool canShow = false;
            OnBannerAction = OnFinish;
            if ((IronSource.Agent.isBannerPlacementCapped(defaultPlacementName) && string.IsNullOrEmpty(placement)) ||
              (IronSource.Agent.isBannerPlacementCapped(placement) && !string.IsNullOrEmpty(placement)))
            {
                FireBannerAction(false);
                return canShow;
            }

            if (isLoaded)
            {
                if (willAlwaysTryNewAd)
                {
                    IronSource.Agent.destroyBanner();
                    isLoaded = false;
                    TryLoad(size, position, placement);
                }
                else
                {
                    canShow = true;
                    IronSource.Agent.displayBanner();
                    FireBannerAction(true);
                }
            }
            else
            {
                TryLoad(size, position, placement);
            }
            return canShow;
        }

        public void HideBanner()
        {
            IronSource.Agent.hideBanner();
        }

        void TryLoad(IronSourceBannerSize size, IronSourceBannerPosition position, string placement)
        {
            if (string.IsNullOrEmpty(placement))
            {
                IronSource.Agent.loadBanner(size, position);
            }
            else
            {
                IronSource.Agent.loadBanner(size, position, placement);
            }
        }

        void IParticularAdInit.InitThisTypeOfAd()
        {
            IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
            IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
            OnBannerAction = null;
            isLoaded = true;
            ////CM_Deb("<color=green>Banner ad has been initialized!</color>");
        }

        void IParticularAdInit.CleanupThisTypeOfAd()
        {
            IronSourceEvents.onBannerAdLoadedEvent -= BannerAdLoadedEvent;
            IronSourceEvents.onBannerAdLoadFailedEvent -= BannerAdLoadFailedEvent;
        }
    }
}