﻿using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    [System.Serializable]
    public partial class VideoAd : IParticularAdInit
    {
        bool IsVideoAdAvailable, IsOnDemandVideoAdAvailable;
        bool adFinishedSuccessfully;
        OnRewardedAd rewardedAction;
        IronSourcePlacement _placement;
        [SerializeField] string defaultPlacementName = "DefaultRewardedVideo";

        //so that outsiders can not create an object of it.
        internal VideoAd()
        {
            
        }

        void FireCallbackOnFailAndCleanup()
        {
            if (rewardedAction != null)
            {
                rewardedAction(false, "", 0);
                rewardedAction = null;
            }
            _placement = null;
            adFinishedSuccessfully = false;
        }
        
        public bool TryShowVideoAd(OnRewardedAd OnFinish, string placementName = null)
        {
            adFinishedSuccessfully = false;
            _placement = null;
            rewardedAction = OnFinish;

            if (Test_Script.testModeEnabled)
            {
                if (rewardedAction != null)
                {
                    rewardedAction(true, "test reward", 1);
                    rewardedAction = null;
                }
                _placement = null;
                adFinishedSuccessfully = true;
                return true;
            }
            

            if (!IronSource.Agent.isRewardedVideoAvailable() || !IsVideoAdAvailable)
            {
                FireCallbackOnFailAndCleanup();
                return false;
            }
            else
            {
                if ((IronSource.Agent.isRewardedVideoPlacementCapped(defaultPlacementName) && string.IsNullOrEmpty(placementName)) ||
                (IronSource.Agent.isRewardedVideoPlacementCapped(placementName) && !string.IsNullOrEmpty(placementName)))
                {
                    FireCallbackOnFailAndCleanup();
                    return false;
                }
                else
                {
                    if (string.IsNullOrEmpty(placementName))
                    {
                        IronSource.Agent.showRewardedVideo();
                    }
                    else
                    {
                        IronSource.Agent.showRewardedVideo(placementName);
                    }
                    return true;
                }
            }
        }

        void IParticularAdInit.InitThisTypeOfAd()
        {
            rewardedAction = null;
            adFinishedSuccessfully = false;
            _placement = null;

            Debug.Log("<color=green>before onRewardedVideoAdClosedEvent subscription!</color>");
            IronSourceEvents.onRewardedVideoAdClosedEvent += OnRewardedVideoAdClosedEvent;
            Debug.Log("<color=green>after onRewardedVideoAdClosedEvent subscription!</color>");

            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += OnRewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += OnRewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += OnRewardedVideoAdShowFailedEvent;

            Debug.Log("<color=green>Video ad has been initialized!</color>");
            
        }

        void IParticularAdInit.CleanupThisTypeOfAd()
        {
            Debug.Log("<color=green>before onRewardedVideoAdClosedEvent unsubscription!</color>");
            IronSourceEvents.onRewardedVideoAdClosedEvent -= OnRewardedVideoAdClosedEvent;
            Debug.Log("<color=green>after onRewardedVideoAdClosedEvent unsubscription!</color>");
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= OnRewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent -= OnRewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent -= OnRewardedVideoAdShowFailedEvent;
        }
    }
}