﻿using UnityEngine;
namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    public partial class VideoAd
    {
        void OnRewardedVideoAvailabilityChangedEvent(bool isAvailable)
        {
            IsVideoAdAvailable = isAvailable;
        }
        void OnRewardedVideoAdRewardedEvent(IronSourcePlacement placement) { AdSuccess(placement); }
        void OnRewardedVideoAdShowFailedEvent(IronSourceError error) { FireCallbackOnFailAndCleanup(); }
        void OnRewardedVideoAdClosedEvent()
        {
            Debug.Log("AdClose() will call");
            AdClose();
        }//ad problem

        void AdClose()//ad problem
        {
            Debug.Log("AdClose() called");
            if (rewardedAction == null)
            {
                Debug.Log("no callback supplied so nothing will be done aftermath.");
            }
            else
            {
                if (_placement != null)
                {
                    rewardedAction(true, _placement.getRewardName(), _placement.getRewardAmount());
                }
                else
                {
                    Debug.Log("the placement is null");
                    rewardedAction(false, "", 0);
                }
            }
            
            //clean up
            rewardedAction = null;
            _placement = null;
        }

        void AdSuccess(IronSourcePlacement placement)
        {
            _placement = placement;
            adFinishedSuccessfully = true;
            if (rewardedAction != null)
            {
                rewardedAction(true, placement.getRewardName(), placement.getRewardAmount());
            }
            rewardedAction = null;
        }
    }
}