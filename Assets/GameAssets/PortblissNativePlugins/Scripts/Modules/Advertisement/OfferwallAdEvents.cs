﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    public partial class OfferwallAd
    {
        /**
          * Invoked each time the user completes an offer.
          * Award the user with the credit amount corresponding to the value of the ‘credits’ 
          * parameter.
          * @param dict - A dictionary which holds the credits and the total credits.   
          */
        void OfferwallAdCreditedEvent(Dictionary<string, object> dict)
        {
            string totalCreditStr = (string)dict["totalCredits"];
            int currency = 0;
            if (int.TryParse(totalCreditStr, out currency))
            {
                userCurrency = currency;
                couldGetUserCurrency = true;
            }
            ////CM_Deb("rpl-offerwall ad credited event! dictionary count: " + dict.Count);
            ////CM_Deb("I got OfferwallAdCreditedEvent, current credits = "dict["credits"] + "totalCredits = " + dict["totalCredits"]);
        }

        /**
          * Invoked when the user is about to return to the application after closing 
          * the Offerwall.
          */
        void OfferwallClosedEvent()
        {
            if (OnOfferwallAction != null)
            {
                OnOfferwallAction(couldGetUserCurrency, userCurrency);
            }
            OnOfferwallAction = null;
        }
    }
}