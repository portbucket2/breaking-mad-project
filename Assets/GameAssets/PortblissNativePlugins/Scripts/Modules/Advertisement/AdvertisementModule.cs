﻿using System.Collections.Generic;
using UnityEngine;
using Portbliss.NativePlugins.InternalDoNotMessup.AdModules;

namespace Portbliss.NativePlugins.InternalDoNotMessup
{
    [System.Serializable]
    public class AdvertisementModule : IFullAdSystemInit
    {
        [SerializeField] string appKey = "573e7c3d";
        [SerializeField] bool useVideoAd, useInterstitalAd, useOfferwallAd, useBannerAd;
        [SerializeField] VideoAd _videoAd;
        public VideoAd videoAd { get { return _videoAd; } private set { _videoAd = value; } }
        [SerializeField] Interstitial _interstitial;
        public Interstitial interstitial { get { return _interstitial; } private set { _interstitial = value; } }
        [SerializeField] OfferwallAd _offerwall;
        public OfferwallAd offerwall { get { return _offerwall; } private set { _offerwall = value; } }
        [SerializeField] BannerAd _banner;
        public BannerAd banner { get { return _banner; } private set { _banner = value; } }

        //IP2CPP optimization plus nothing outsider can create an object of it
        internal AdvertisementModule()
        {
            //videoAd should be updated from '_videoAd'. cause otherwise 
            //'((IAdModuleInit)videoAd).InitAdModule();' will fail
            videoAd = _videoAd = new VideoAd();
            interstitial = _interstitial = new Interstitial();
            offerwall = _offerwall = new OfferwallAd();
            banner = _banner = new BannerAd();
        }
        
        void IFullAdSystemInit.InitAdSystem(Transform rootPluginTransform, IronSourceSegment userSegment)
        {
            //we need the advertisement related events. i.e "when a video ad closed"   
            //GameObject ironsourceEventGameObject = new GameObject("_IronsourceEvents[GeneratedByScript_DONOT_DELETE!]");
            //ironsourceEventGameObject.AddComponent<IronSourceEvents>();
            //ironsourceEventGameObject.transform.SetParent(rootPluginTransform);

            
            Debug.Log("rpl-will init iron source!");
            List<string> modes = new List<string>();
            if (useVideoAd) { modes.Add(IronSourceAdUnits.REWARDED_VIDEO); }
            if (useInterstitalAd) { modes.Add(IronSourceAdUnits.INTERSTITIAL); }
            if (useOfferwallAd) { modes.Add(IronSourceAdUnits.OFFERWALL); }
            if (useBannerAd) { modes.Add(IronSourceAdUnits.BANNER); }
            IronSource.Agent.shouldTrackNetworkState(true);
            if (useOfferwallAd)
            {
                IronSourceConfig.Instance.setClientSideCallbacks(true);//we are using only client side callback, for now.
            }
            
            if (userSegment != null)
            {
                IronSource.Agent.setSegment(userSegment);
            }
            IronSource.Agent.init(appKey, modes.ToArray());

            
            if (useVideoAd)
                ((IParticularAdInit)videoAd).InitThisTypeOfAd();
            if (useInterstitalAd)
                ((IParticularAdInit)interstitial).InitThisTypeOfAd();
            if (useOfferwallAd)
                ((IParticularAdInit)offerwall).InitThisTypeOfAd();
            if (useBannerAd)
                ((IParticularAdInit)banner).InitThisTypeOfAd();

            Debug.Log("advertisement has been initialized.");
            Debug.Log("<color=green>Advertisement full system has been initialized!</color>");
        }

        public void SetUserConsentDueToRecentEuGdprPolicy()
        {
            IronSource.Agent.setConsent(true);
        }

        public void ValidateIronSource()
        {
            Debug.Log("rpl-will validate integration!");
            IronSource.Agent.validateIntegration();
        }

        void IFullAdSystemInit.CleanUpAdResources()
        {
            if (useVideoAd)
                ((IParticularAdInit)videoAd).CleanupThisTypeOfAd();
            if (useInterstitalAd)
                ((IParticularAdInit)interstitial).CleanupThisTypeOfAd();
            if (useOfferwallAd)
                ((IParticularAdInit)offerwall).CleanupThisTypeOfAd();
            if (useBannerAd)
                ((IParticularAdInit)banner).CleanupThisTypeOfAd();
        }
    }
}