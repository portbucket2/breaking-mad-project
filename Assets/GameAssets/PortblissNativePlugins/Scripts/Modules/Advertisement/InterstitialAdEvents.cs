﻿using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup.AdModules
{
    public partial class Interstitial
    {
        //Invoked when the Interstitial is Ready to shown after load function is called
        private void IronSourceEvents_onInterstitialAdReadyEvent()
        {
            //CM_Deb"rpl-ad loaded");
        }

        //Invoked when the initialization process has failed.
        //@param description - string - contains information about the failure.
        private void IronSourceEvents_onInterstitialAdLoadFailedEvent(IronSourceError obj)
        {
            //CM_Deb"rpl-ad load fail");
        }

        //Invoked right before the Interstitial screen is about to open.
        private void IronSourceEvents_onInterstitialAdShowSucceededEvent()
        {
            didUserSeeInterstitial = true;
        }

        //Invoked when the ad fails to show.
        //@param description - string - contains information about the failure.
        private void IronSourceEvents_onInterstitialAdShowFailedEvent(IronSourceError obj)
        {
            didUserSeeInterstitial = false;
        }

        //Invoked when the interstitial ad closed and the user goes back to the application screen.
        private void IronSourceEvents_onInterstitialAdClosedEvent()
        {
            FireCallbackIfAnyAndCleanup(didUserSeeInterstitial);
        }
    }
}