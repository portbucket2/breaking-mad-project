﻿using UnityEngine;

namespace Portbliss.NativePlugins.InternalDoNotMessup
{
    [System.Serializable]
    public class AnalyticsModule : IRootPluginInit
    {
        [SerializeField] GoogleAnalyticsV4 analyticsV4;
        public GoogleAnalyticsV4 googleAnalyticsAPIentry { get { return analyticsV4; }private set { analyticsV4 = value; } }
        void IRootPluginInit.InitSystem()
        {
            googleAnalyticsAPIentry.StartSession();
            ////CM_Deb"analytics has been initialized.");
            ////CM_Deb"<color=green>Analytics system has been initialized!</color>");
        }
        
    }
}