﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using System;

namespace Portbliss.NativePlugins.InternalDoNotMessup
{
    [System.Serializable]
    public class GooglePlayGamesModule : IGPGInit
    {
        //to prevent outsider of this future assembly to create an instance of this class/module
        //and to optimize IL2CPP code generation
        internal GooglePlayGamesModule()
        {

        }

        [SerializeField] bool requestUserTokenForBackend = false;
        //BackendModule backendInstance;
        void IGPGInit.InitGPGModule()
        {
            PlayGamesClientConfiguration config;
            if (requestUserTokenForBackend)
            {
                config = new PlayGamesClientConfiguration.Builder().RequestIdToken().Build();
            }
            else
            {
                config = new PlayGamesClientConfiguration.Builder().Build();
            }
            
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesPlatform.Activate();
            ////CM_Deb"<color=green>Google play games system has been initialized!</color>");
        }

        public void SignIn(OnSuccess OnSignIn = null)
        {
            
            Social.localUser.Authenticate(success => 
            {
                if (success)
                {
                    string token = ((PlayGamesLocalUser)Social.localUser).GetIdToken();
                    //CM_Deb"the user token for gpg is: " + token);
                    //((IBackendToken)backendInstance).AddGoogleToken(token);
                }
                else
                {
                    Debug.LogError("Google login failed. If you are not running in an actual Android/iOS device," +
                        " this is expected.");
                }

                if (OnSignIn != null)
                {
                    OnSignIn(success);
                } });
        }

        public void UnlockAchievement(string achievement_ID, float progressAmount, OnSuccess OnUnlock = null)
        {
            Social.ReportProgress(achievement_ID, progressAmount, (success) => { if (OnUnlock != null) { OnUnlock(success); } });
        }

        public void UnlockAchievement(string achievement_ID, Action<bool> OnUnlock = null)
        {
            PlayGamesPlatform.Instance.UnlockAchievement(achievement_ID, OnUnlock);
        }

        public void IncrementAchievement(string achievement_ID, int stepsToIncrement, OnSuccess OnIncrement = null)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(achievement_ID, stepsToIncrement,
                success =>
                {
                    if (OnIncrement != null) { OnIncrement(success); }
                });
        }

        public void RevealAchievement(string achievement_ID, Action<bool> OnReveal = null)
        {
            PlayGamesPlatform.Instance.RevealAchievement(achievement_ID, OnReveal);
        }

        public void ShowAchievementsUI()
        {
            Social.ShowAchievementsUI();
        }

        public void AddScoreToLeaderboard(string leaderboard_ID, long score, OnSuccess OnAddScore = null)
        {
            Social.ReportScore(score, leaderboard_ID, success =>
            {
                if (OnAddScore != null) { OnAddScore(success); }
            });
        }

        public void ShowLeaderboardsUI()
        {
            Social.ShowLeaderboardUI();
        }

        public void GetUserScoreOnLeaderboard(string leaderboard_ID, OnGetStringData OnGetUserScore)
        {
            PlayGamesPlatform.Instance.LoadScores(
             leaderboard_ID,
             LeaderboardStart.PlayerCentered,
             1,
             LeaderboardCollection.Public,
             LeaderboardTimeSpan.AllTime,
         (LeaderboardScoreData data) =>
         {
             if (OnGetUserScore != null)
             {
                 OnGetUserScore(data.PlayerScore.formattedValue);
             }
         });
        }
    }
}