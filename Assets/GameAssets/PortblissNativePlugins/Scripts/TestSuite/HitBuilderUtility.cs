﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitBuilderUtility
{
    InputField campaignID, campaignName, campaignSource, campaignKeyword,
        campaignMedium, campaignContent, eventCustomDimensionValue;
    Toggle useCustomEvent, useCampaign;
    public HitBuilderUtility(InputField campaignID, InputField campaignName, InputField campaignSource,
        InputField campaignKeyword, InputField campaignMedium, InputField campaignContent,
        InputField eventCustomDimensionValue, Toggle useCustomEvent, Toggle useCampaign)
    {
        this.campaignID = campaignID;
        this.campaignName = campaignName;
        this.campaignSource = campaignSource;
        this.campaignKeyword = campaignKeyword;
        this.campaignMedium = campaignMedium;
        this.campaignContent = campaignContent;
        this.eventCustomDimensionValue = eventCustomDimensionValue;
        this.useCustomEvent = useCustomEvent;
        this.useCampaign = useCampaign;
    }

    public void SetCustomAndCampaignIfNeeded(ref AppViewHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }

    public void SetCustomAndCampaignIfNeeded(ref EventHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }

    public void SetCustomAndCampaignIfNeeded(ref ExceptionHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }

    public void SetCustomAndCampaignIfNeeded(ref ItemHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }

    public void SetCustomAndCampaignIfNeeded(ref SocialHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }

    public void SetCustomAndCampaignIfNeeded(ref TimingHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }

    public void SetCustomAndCampaignIfNeeded(ref TransactionHitBuilder hit, InputField dimensionNumber)
    {
        int dimNumber = 0;
        int.TryParse(dimensionNumber.text, out dimNumber);

        hit = useCustomEvent.isOn ? hit.SetCustomDimension(dimNumber, eventCustomDimensionValue.text) : hit;
        hit = useCampaign.isOn ? hit.SetCampaignName(campaignName.text)
                                    .SetCampaignSource(campaignSource.text)
                                    .SetCampaignMedium(campaignMedium.text)
                                    .SetCampaignKeyword(campaignKeyword.text)
                                    .SetCampaignContent(campaignKeyword.text)
                                    .SetCampaignID(campaignID.text) : hit;
    }
}
