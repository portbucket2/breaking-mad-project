﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.NativePlugins;

public class AdTest : MonoBehaviour {

    [SerializeField] Dropdown bannerSize, bannerPosition;
    IronSourceBannerPosition position;
    IronSourceBannerSize size;
    [SerializeField] InputField placement;
    [SerializeField] Toggle usePlacement, willAlwaysLoadNewBannerAd;
    [SerializeField] Button validateAd, giveConsent, showBanner, hideBanner, showInterstitial, showOfferwall, showVideo;
    void Start()
    {
        validateAd.onClick.AddListener(delegate { ValidateAdSystem(); });
        giveConsent.onClick.AddListener(delegate { GiveConsent(); });
        showBanner.onClick.AddListener(delegate { ShowBannerAd(); });
        hideBanner.onClick.AddListener(delegate { HideBanner(); });
        showInterstitial.onClick.AddListener(delegate { ShowInterstitial(); });
        showOfferwall.onClick.AddListener(delegate { ShowOfferwall(); });
        showVideo.onClick.AddListener(delegate { ShowVideoAd(); });
    }

    void ValidateAdSystem()
    {
        PortblissPlugins.ad.ValidateIronSource();
    }

    void GiveConsent()
    {
        PortblissPlugins.ad.SetUserConsentDueToRecentEuGdprPolicy();
    }

    void ShowBannerAd()
    {
        PortblissPlugins.ad.banner.TryShowBannerAd(size, position, willAlwaysLoadNewBannerAd.isOn,
            (hasSucced) => 
            {
                if (hasSucced)
                {
                    //CM_Deb"<color=green> Banner test passed! </color>");
                }
                else
                {
                    //CM_Deb"<color=red> Banner test failed! </color>");
                }
            } 
            , usePlacement.isOn ? placement.text : null);
    }

    void HideBanner()
    {
        PortblissPlugins.ad.banner.HideBanner();
    }

    void ShowVideoAd()
    {
        PortblissPlugins.ad.videoAd.TryShowVideoAd((success, rewardName, rewardAmount) =>
        {
            if (success)
            {
                //CM_Deb"<color=green> videoAd test passed! Reward Name: " + rewardName + " and Reward amount: " + rewardAmount + "</color>");
            }
            else
            {
                //CM_Deb"<color=red> videoAd test failed! </color>");
            }
        }
        , usePlacement.isOn ? placement.text : null);
    }

    void ShowOfferwall()
    {
        PortblissPlugins.ad.offerwall.TryShowOfferwallAd((success, currency) => 
        {
            if (success)
            {
                //CM_Deb"<color=green> Offerwall test passed! total currency: " + currency + "</color>");
            }
            else
            {
                //CM_Deb"<color=red> Offerwall test failed! </color>");
            }

        }, usePlacement.isOn ? placement.text : null);
    }

    void ShowInterstitial()
    {
        PortblissPlugins.ad.interstitial.TryShowInterstitialAd((success) =>
        {
            if (success)
            {
                //CM_Deb"<color=green> Interstitial test passed! </color>");
            }
            else
            {
                //CM_Deb"<color=red> Interstitial test failed! </color>");
            }
        }, usePlacement.isOn ? placement.text : null);
    }

    void SetSize()
    {
        if (bannerSize.value == 0)
        {
            size = IronSourceBannerSize.BANNER;
        }
        else if (bannerSize.value == 1)
        {
            size = IronSourceBannerSize.LARGE;
        }
        else if (bannerSize.value == 2)
        {
            size = IronSourceBannerSize.RECTANGLE;
        }
        else
        {
            size = IronSourceBannerSize.SMART;
        }
    }

    void SetPosition()
    {
        if (bannerPosition.value == 0)
        {
            position = IronSourceBannerPosition.TOP;
        }
        else
        {
            position = IronSourceBannerPosition.BOTTOM;
        }
    }
}
