﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.NativePlugins;

public class BreakingMad_Test : MonoBehaviour {

    [SerializeField] Button showAd, purchase, gpgSignin, adIntegration, analytics;
    
	// Use this for initialization
	void Start () {

        showAd.onClick.AddListener(delegate {
            PortblissPlugins.ad.videoAd.TryShowVideoAd((success, rewardName, rewardAmount)=> 
            {
                //CM_Deb"<color=yellow>video ad callback result: success?" + success
                 //   + " , and rewardName: " + rewardName + " , and reward amount: " + rewardAmount + "</color>");
            });
        });
       
        purchase.onClick.AddListener(delegate {

            //CM_Deb"<color=green>Purchase button clicked!</color>");
            //this code has been commented because the list reference's access modifier has been changed
            //in the test mode we needed it, but now we do not.
            //List<string> prodList = PortblissPlugins.inAppPurchase.consumableProductList;
            //if (prodList != null)
            //{
            //    if (prodList.Count > 0)
            //    {
            //        if (string.IsNullOrEmpty(prodList[0]) == false)
            //        {
            //            PortblissPlugins.inAppPurchase.BuyProduct(PortblissPlugins.inAppPurchase.consumableProductList[0], (inappResult) =>
            //            {
            //                //CM_Deb"<color=yellow>purchase callback result: productID: ?" + PortblissPlugins.inAppPurchase.consumableProductList[0]
            //                    + " , and iapResult: " + inappResult + "</color>");
            //            });
            //        }
            //    }
            //}
        });

        gpgSignin.onClick.AddListener(delegate {

            //CM_Deb"<color=green>GPG Signin button clicked!</color>");
            PortblissPlugins.gpg.SignIn((success) => 
            {
                //CM_Deb"<color=yellow>gpg signin callback result: success?" + success
                 //   + "</color>");
            });
        });

        adIntegration.onClick.AddListener(delegate
        {
            //CM_Deb"<color=yellow> Integration will try to run, debug with adb logcat and check result in the dumped text file!</color>");
            PortblissPlugins.ad.ValidateIronSource();
        });

        analytics.onClick.AddListener(delegate
        {
            //CM_Deb"<color=yellow> Will try to log as much as we can and send the data at last to google, check analytics realtime dashboard</color>");
            //event
            EventHitBuilder EventHit = new EventHitBuilder()
               .SetEventCategory("BreakingMad_EventCategory")
               .SetEventAction("BreakingMad_Action")
               .SetEventLabel("BreakingMad_Label")
               .SetEventValue(10);
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogEvent(EventHit);
            //CM_Deb"<color=green>Event logged!</color>");

            //screen
            AppViewHitBuilder screenHit = new AppViewHitBuilder()
            .SetScreenName("BreakingMad_Screen");
            //Builder Hit with all App View parameters (all parameters required):
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogScreen(screenHit);
            //CM_Deb"<color=green>Screen logged!</color>");

            //exception
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogException(new ExceptionHitBuilder()
        .SetExceptionDescription("BreakingMad_ExceptionDesc")
        .SetFatal(true));
            //CM_Deb"<color=green>Exception logged!</color>");

            //loading time
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogTiming(new TimingHitBuilder()
                .SetTimingCategory("BreakingMad_TimingCategory")
                .SetTimingInterval(50L)
                .SetTimingName("BreakingMad_TimingName")
                .SetTimingLabel("BreakingMad_TimingLabel"));
            //CM_Deb"<color=green>Game loading time logged!</color>");

            //social
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogSocial(new SocialHitBuilder()
            .SetSocialNetwork("BreakingMad_SocialNetwork")
            .SetSocialAction("BreakingMad_SocialAction")
            .SetSocialTarget("BreakingMad__twitter.com/googleanalytics/status/482210840234295296"));
            //CM_Deb"<color=green>Social media action logged!</color>");

            //transaction
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogTransaction(new TransactionHitBuilder()
                .SetTransactionID("BreakingMad_TransactionID")
                .SetAffiliation("BreakingMad_PurchaseAffiliation")
                .SetRevenue(3.0)
                .SetTax(0)
                .SetShipping(0.0)
                .SetCurrencyCode("BreakingMad_Currency"));
            //CM_Deb"<color=green>Purchase transaction logged!</color>");

            //item
            PortblissPlugins.analytic.googleAnalyticsAPIentry.LogItem(new ItemHitBuilder()
                .SetTransactionID("BreakingMad_TransactionID")
                .SetName("BreakingMad_PurchaseName")
                .SetSKU("BreakingMad_PurchaseSKU")
                .SetCategory("BreakingMad_PurchaseCategory")
                .SetPrice(3.0)
                .SetQuantity(2)
                .SetCurrencyCode("BreakingMad_Currency"));
            //CM_Deb"<color=green>Purchase item logged!</color>");

            PortblissPlugins.analytic.googleAnalyticsAPIentry.DispatchHits();
            //CM_Deb"<color=green>Data sent to google!</color>");
        });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
