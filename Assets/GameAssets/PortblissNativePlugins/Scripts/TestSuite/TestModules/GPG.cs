﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.NativePlugins;

public partial class TestSuite : MonoBehaviour {

	// Use this for initialization
	void Start () {

        listenBtnActive = false;
	}

    #region GPG
    [SerializeField] Text test_score_showTXT;
    int sc2 = 0;
    public void Test_Score()
    {
        //sc2++;
        //AddScoreToLeaderboard(GPGSIds.leaderboard_gp_leader_2, sc2, (success) =>
        //{
        //    //CM_Deb"score added to respective leaderboard?" + success);
        //});
    }

    public void Test_GetScore()
    {
        //GetUserScoreOnLeaderboard(GPGSIds.leaderboard_gp_leader_2, (scoreFromStore) =>
        //{
        //    test_score_showTXT.text = scoreFromStore;
        //    //CM_Deb"score of respective leaderboard and user obtained: " + scoreFromStore);
        //});
    }

    public void Test_Unlock()
    {
        //UnlockAchievement(GPGSIds.achievement_ach_normal_1,
        //    (success) =>
        //    {
        //        //CM_Deb"achievement 1 normal unlocked!");
        //    });
    }

    public void Test_Reveal()
    {
        //RevealAchievement(GPGSIds.achievement_ach5_hidden,
        //    (success) =>
        //    {
        //        //CM_Deb"achievement revealed!!");
        //    });
    }

    public void Test_Increment()
    {
        //IncrementAchievement(GPGSIds.achievement_ach_3_incremental_normal, 1,
        //    (success) =>
        //    {
        //        //CM_Deb"incremented on the way to achievement");
        //    });
    }

    public void Test_ShowAchievements()
    {
        //ShowAchievementsUI();
    }

    public void Test_ShowLeaderboards()
    {
        //ShowLeaderboardsUI();
    }
    #endregion


    void WaitButtons(float waitTime)
    {
        interval = waitTime;
        Button[] allBtn = FindObjectsOfType<Button>();
        foreach (var b in allBtn) { b.interactable = false; }
        listenBtnActive = true;
    }

    bool listenBtnActive;
    float timer = 0;
    float interval = 1.2f;
    void Update()
    {
        if (listenBtnActive)
        {
            timer += Time.deltaTime;
            if (timer > interval)
            {
                timer = 0;
                Button[] allBtn = FindObjectsOfType<Button>();
                foreach (var b in allBtn) { b.interactable = true; }
                listenBtnActive = false;
            }
        }
        else
        {
            timer = 0;
        }
    }
}
