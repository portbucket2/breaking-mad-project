﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.NativePlugins;

public class AnalyticsTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    [SerializeField]
    InputField eventCategoryName, eventActionName, eventLabelName,
        eventValueName, eventCustomDimensionNumber, eventCustomDimensionValue,
        screenName, exceptionName, loadTimeName, loadTimeCategory, loadTimeLabel,
        loadTime, campaignID, campaignName, campaignSource, campaignKeyword,
        campaignMedium, campaignContent;
    [SerializeField] Toggle useCustomEvent, isExceptionFatal, useCampaign;
    [SerializeField] Button eventLog, sendData;
    HitBuilderUtility hitUtils;
    void LogEvent()
    {
        int eventValue = 0;
        int.TryParse(eventValueName.text, out eventValue);

        

        // An Event hit example, but custom metrics can be sent with all hit types.
        EventHitBuilder hit = new EventHitBuilder()
                .SetEventCategory(eventCategoryName.text)
                .SetEventAction(eventActionName.text)
                .SetEventLabel(eventLabelName.text)
                .SetEventValue(eventValue);

        
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogEvent(hit);
        ////.Log("<color=yellow> Event should have been sent to analytics, check the dashboard! </color>");
    }

    

    void SendDataToGoogle()
    {
        PortblissPlugins.analytic.googleAnalyticsAPIentry.DispatchHits();
        ////.Log("<color=yellow> Data has been sent to google, dashboard should be updated now. </color>");
    }

    void LogScreen()
    {
        AppViewHitBuilder hit = new AppViewHitBuilder()
            .SetScreenName(screenName.text);


        //Builder Hit with all App View parameters (all parameters required):
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogScreen(hit);
        //CM_Deb("<color=yellow> Screen has been logged. </color>");
    }

    public void LogException()
    {
        // Builder Hit with all Exception parameters.
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogException(new ExceptionHitBuilder()
        .SetExceptionDescription(exceptionName.text)
        .SetFatal(isExceptionFatal.isOn));
        //CM_Deb("<color=yellow> Exception has been sent. </color>");
    }

    public void LogLoadingTime()
    {
        // Builder Hit with all Timing parameters.
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogTiming(new TimingHitBuilder()
            .SetTimingCategory("MuktiCamp_Loading")
            .SetTimingInterval(50L)
            .SetTimingName("MuktiCamp_Main Menu")
            .SetTimingLabel("MuktiCamp_First load"));

        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogTiming(new TimingHitBuilder()
    .SetTimingCategory("MuktiCamp_Loading")
    .SetTimingInterval(50L)
    .SetTimingName("MuktiCamp_Main Menu")
    .SetTimingLabel("MuktiCamp_First load")
    .SetCampaignName("MuktiCamp_Summer Campaign")
    .SetCampaignSource("MuktiCamp_google")
    .SetCampaignMedium("MuktiCamp_cpc")
    .SetCampaignKeyword("MuktiCamp_games")
    .SetCampaignContent("MuktiCamp_Free power ups")
    .SetCampaignID("Summer1"));
        //CM_DebWarning("Log load time!");
    }

    public void LogSocialActivities()
    {
        // Builder Hit with all Social parameters.
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogSocial(new SocialHitBuilder()
            .SetSocialNetwork("MuktiCamp_Twitter")
            .SetSocialAction("MuktiCamp_Retweet")
            .SetSocialTarget("MuktiCamp_twitter.com/googleanalytics/status/482210840234295296"));
        //.LogWarning("Log social!");
    }

    public void LogPurchase()
    {
        // Builder Hit with all Transaction parameters.
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogTransaction(new TransactionHitBuilder()
            .SetTransactionID("MuktiCamp_TRANS001")
            .SetAffiliation("MuktiCamp_Coin Store")
            .SetRevenue(3.0)
            .SetTax(0)
            .SetShipping(0.0)
            .SetCurrencyCode("MuktiCamp_USD"));

        // Builder Hit with all Item parameters.
        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogItem(new ItemHitBuilder()
            .SetTransactionID("MuktiCamp_TRANS001")
            .SetName("MuktiCamp_Sword")
            .SetSKU("MuktiCamp_SWORD1223")
            .SetCategory("MuktiCamp_Weapon")
            .SetPrice(3.0)
            .SetQuantity(2)
            .SetCurrencyCode("MuktiCamp_USD"));

        //.LogWarning("Log purchase!");
    }
}
