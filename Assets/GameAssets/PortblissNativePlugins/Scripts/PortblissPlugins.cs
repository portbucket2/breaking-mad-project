﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.NativePlugins.InternalDoNotMessup;
using System;
using UnityEngine.Profiling;

/// <summary>
/// This plugin is specifically tweaked for "Breaking Mad" game project.
/// The main plugin located at "D:\Rumman\NativePlugins\07.Push-OneSignal\0.1PushNotif" should
/// be merged with this one. This one contains some fix. The documentation should also be 
/// completed, so far only analytics documentation has been done.
/// </summary>

namespace Portbliss.NativePlugins
{
    public delegate void OnAction();
    public delegate void OnPurchase(IAPresult result);
    public delegate void OnSuccess(bool hasSucceeded);
    public delegate void OnRewardedAd(bool hasSucceeded, string rewardName, int rewardAmount);
    public delegate void OnOfferwall(bool success, int currency);
    public delegate void BackEndCallbackOnGet(bool hasSucceed, string data);
    public delegate void OnGetStringData(string strData);
    public enum IAPresult {consumable, nonConsumable, subscription, fail };
    public partial class PortblissPlugins : MonoBehaviour, IRootPluginInit
    {
        [SerializeField] GooglePlayGamesModule _gpg;
        public static GooglePlayGamesModule gpg { get; private set; }

        [SerializeField] InAppPurchaseModule _iap;
        public static InAppPurchaseModule inAppPurchase { get; private set; }

        [SerializeField] AnalyticsModule _analytic;
        public static AnalyticsModule analytic { get; private set; }

        [SerializeField] AdvertisementModule _ad;
        public static AdvertisementModule ad { get; private set; }

        private static bool rootPluginWrapperExists;
        // Use this for initialization
        void Awake()
        {
            //Profiler.enabled = true;
          
            if (!rootPluginWrapperExists)
            {
                rootPluginWrapperExists = true;
                DontDestroyOnLoad(gameObject);
                //gpg = new GooglePlayGamesModule();//to avoid null reference exception
                
                IRootPluginInit init_root = null;
                init_root = ((IRootPluginInit)this);
                if (init_root != null)
                {
                    init_root.InitSystem();
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void OnApplicationQuit()
        {
            ////CM_Deb"Application ending after " + Time.time + " seconds");
            //ubsubscribe all events and other ad related resources if any
            IFullAdSystemInit cleanup_ad_intfs = null;
            cleanup_ad_intfs = ((IFullAdSystemInit)ad);
            if (cleanup_ad_intfs != null)
            {
                cleanup_ad_intfs.CleanUpAdResources();
            }
        }

        void IRootPluginInit.InitSystem()
        {            
            /*/
            IronSourceSegment segment = new IronSourceSegment();
            segment.segmentName = "nameOfSegment";
            // Create segment object
            IronSourceSegment segment = new IronSourceSegment();
            // Set user age
            segment.age = age;
            // Set user gender
            segment.gender = "gender";
            // Set user's level
            segment.level = levelnumber;
            // Set user creation date
            segment.userCreationDate = creationData
            // Set user's total in-app purchases
            segment.iapt = numberOfInAppPurchases;
            // Set user's paying status, 1 is yes 0 is no
            segment.isPaying = payingStatus;
            // Set custom parameters (up to 5)
            Dictionary<string, string> customParams = new Dictionary<string, string>();
            customParams.Add("customKey", "value");
            segment.customs = customParams;
            IronSource.Agent.setSegment(segment);
            */

            ad = _ad;
            IFullAdSystemInit init_ad = null;
            init_ad = ((IFullAdSystemInit)ad);
            if (init_ad != null)
            {
                init_ad.InitAdSystem(transform, null);
            }


            IGPGInit init_gpg = null;
            gpg = _gpg;
            init_gpg = ((IGPGInit)gpg);
            if (init_gpg != null)
            {
                init_gpg.InitGPGModule();
            }


            inAppPurchase = _iap;
            IIapPurchaseInit init_inApp = null;
            init_inApp = ((IIapPurchaseInit)inAppPurchase);
            if (init_inApp != null)
            {
                init_inApp.InitInAppModule();
            }

            analytic = _analytic;
            IRootPluginInit init_analytics = null;
            init_analytics = ((IRootPluginInit)analytic);
            if (init_analytics != null)
            {
                init_analytics.InitSystem();
            }

            Debug.Log("<color=green>All plugin systems have been initialized!</color>");
        }

        //because of ironsource we have to do it
        void OnApplicationPause(bool isPaused)
        {
            IronSource.Agent.onApplicationPause(isPaused);
        }
    }
}