﻿using FishSpace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDragon : AIGuard, ICharacter//,IAttacker
{
    //public ParticleSystem attackParticle;
    bool ICharacter.canFly
    {
        get
        {
            return true;
        }
    }

    //void IAttacker.TriggerHitParticle(Vector3 point)
    //{
    //    attackParticle.Play();
    //}
    public override void ForceToAbyss(Dir dir)
    {
        Vector3 forceVec = MovementController.Dir2Vec(dir) * 1f + new Vector3(0, 1.5f, 0);
        float x = Random.Range(-0.5f, 0.5f);
        //float y = Random.Range(-1.0f, 1.0f);
        float z = Random.Range(-1.0f, 1.0f);
        Vector3 torqueVec = new Vector3(x, 0, z).normalized * 0.1f;
        controller.Command_DropToAbyss(dir, forceVec, torqueVec);
    }

    public override void OnUpdate()
    {
        if (controller.IsMoving())
            return;
        if (GetTarget() != null)
        {
            controller.Command_TakeAction(GetTarget().GetRelativeDirectionFrom(this));
        }
        else if (shouldTryToPatrol)
        {
            if (controller.CanSafeFly_Forward())
            {
                //if (enableLog) //CM_Deb("can forward");
                controller.Command_TakeAction(controller.GetFaceDir());
            }
            else if (controller.CanSafeFly_BackWard())
            {
                controller.Command_SetDirection(controller.GetTailDir());
                //if (enableLog) //CM_Deb("can backward");
                controller.Command_TakeAction(controller.GetFaceDir());
            }
            else
            {
                StopTryingToPatrol();
            }
        }
    }

    public override void PlayTypeSpecificDeathSound()
    {
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Nurse, this.transform.position);
    }
    public override void PlayTypeSpecificAttackSound()
    {
        SoundKeeper.PlayEnemySound(EnemyAudioType.nurse_attack, this.transform.position);
        SoundKeeper.PlayEnemySound(EnemyAudioType.nurse_attackgas, this.transform.position);
    }

}
