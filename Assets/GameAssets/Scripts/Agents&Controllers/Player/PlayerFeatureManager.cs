﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;
public class PlayerFeatureManager : MonoBehaviour {
    public PlayerManager playerManager;
    public GameObject go_shield;
    public GameObject go_hardbootRight;
    public GameObject go_hardbootLeft;
    public GameObject go_firehammer;
    public GameObject go_lightninghammer;
    public GameObject go_magnet;

    public GameObject BombParticle;
    public GameObject MagnetTrigger;
    public Animator camAnim;

    //public ParticleSystem protectionParticle;

    public void EnableUpgrade_FireHammer(bool enabled)
    {
        go_firehammer.SetActive(enabled);
    }
    public void EnableUpgrade_LightningHammer(bool enabled)
    {
        go_lightninghammer.SetActive(enabled);
    }
    public void EnableUpgrade_Magnet(bool enabled)
    {
        go_magnet.SetActive(enabled);
        MagnetTrigger.SetActive(enabled);
    }
    public void EnableUpgrade_HardBoot(bool enabled)
    {
        go_hardbootLeft.SetActive(enabled);
        go_hardbootRight.SetActive(enabled);
    }
    public void EnableUpgrade_Shield(bool enabled)
    {
        go_shield.SetActive(enabled);
    }
    public void Enable_ProtectionParticle(bool enabled)
    {
        //protectionParticle.enableEmission = enabled;
        //if (enabled)
        //{
        //    protectionParticle.Play();
        //}
        //else
        //{
        //    protectionParticle.Stop();
        //}
    }


    IEnumerator RunProtection(int time)
    {
        //protectionParticle.Play();
        yield return new WaitForSecondsRealtime(time);
        //protectionParticle.Stop();
    }

    //public Transform basicAttackParticle_tr;
    public Transform doubleAttackParticle_tr;
    //public ParticleSystem basicAttackParticle;
    //public ParticleSystem doubleAttackParticle;
    public Transform particlPosition_Up;
    public Transform particlPosition_Down;
    public Transform particlPosition_Forward;
    public void PlayAttackParticle(Dir dir)
    {

        //SoundKeeper.PlayHeroSound(CharacterAudioType.Hit_Swish, this.transform.position);
        //if (playerManager.IsAttackUpgraded())
        //    SoundKeeper.PlayHeroSound(CharacterAudioType.Hit_Lightning, this.transform.position);

        //Transform pTr = playerManager.IsAttackUpgraded() ? doubleAttackParticle_tr : null;
        ////ParticleSystem psys = playerManager.IsAttackUpgraded() ? doubleAttackParticle : null;
        ////if (psys == null) return;
        //Transform par = null;
        //switch (dir)
        //{
        //    case Dir.UP:
        //        par = particlPosition_Up;
        //        break;
        //    case Dir.DOWN:
        //        par = particlPosition_Down;
        //        break;
        //    case Dir.LEFT:
        //    case Dir.RIGHT:
        //        par = particlPosition_Forward;
        //        break;
        //}

        //pTr.SetParent(par);
        //pTr.rotation = par.rotation;
        //pTr.position = par.position;
       // psys.Stop();
        //psys.Play();
        //Transform pTr = playerManager.IsFireEnabled() ? doubleAttackParticle_tr : basicAttackParticle_tr;
        //ParticleSystem psys = (playerManager.IsFireEnabled() ? doubleAttackParticle : basicAttackParticle);
        //Transform par = null;
        //switch (dir)
        //{
        //    case Dir.UP:
        //        par = particlPosition_Up;
        //        break;
        //    case Dir.DOWN:
        //        par = particlPosition_Down;
        //        break;
        //    case Dir.LEFT:
        //    case Dir.RIGHT:
        //    default:
        //        par = particlPosition_Forward;
        //        break;
        //}

        //pTr.SetParent(par);
        //pTr.rotation = par.rotation;
        //pTr.position = par.position;
        //psys.Stop();
        //psys.Play();
    }

   // public ParticleSystem LandingParticle;
    public void PlayLandingEffects()
    {
        if (playerManager.IsLandingingUpgradeEnabled())
        {
           // LandingParticle.Play();
            SoundKeeper.PlayHeroSound(CharacterAudioType.Land_Fire, this.transform.position);
        }
        else
        {
            SoundKeeper.PlayHeroSound(CharacterAudioType.Land_Normal, this.transform.position);
        }
    }

    public void ShowBombParticleAt(Vector3 pos)
    {
        GameObject go = Pool.Instantiate(BombParticle, pos, Quaternion.identity);
        //go.GetComponent<ParticleSystem>().Play();
        Centralizer.Add_DelayedAct(()=> { Pool.Destroy(go); }, 5);
    }

   // public ParticleSystem takeHitParticle;
    public void Play_TakeHitParticle()
    {
        //camAnim.SetTrigger("Shake");
        // takeHitParticle.Play();
        //takeHitParticle.Stop();
    }
}
