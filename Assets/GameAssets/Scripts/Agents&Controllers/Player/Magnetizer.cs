﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class Magnetizer : MonoBehaviour {
    public Transform magnet;
    public float speed = 20;
    // Use this for initialization
    private List<FishSpace.PooledItem> collectibles = new List<FishSpace.PooledItem>();
    private void OnEnable()
    {
        collectibles.Clear();
    }

    void Update () {
        for (int i = collectibles.Count - 1; i >= 0; i--)
        {
            PooledItem pi = collectibles[i];
            if (pi == null)
            {
                Debug.LogError("Unpooled or null item!");
                continue;
            }
            if (pi.alive)
            {
                pi.transform.position = Vector3.Lerp(pi.transform.position,magnet.position,speed*Time.deltaTime);//MoveTowards(pi.transform.position, magnet.position, speed * Time.deltaTime);//
                if (Vector3.Distance(pi.transform.position, magnet.position) < 0.5f)
                {
                    collectibles.Remove(pi);
                    IHitable hitable = pi.GetComponent<IHitable>();
                    PlayerManager.playerAttacker.AddScore(hitable.GetScoreOnHit());
                    hitable.TakeHit(Dir.DOWN, PlayerManager.playerAttacker);
                }
            }
            else
            {
                collectibles.Remove(pi);
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        Transform t = other.transform;
        if (t.CompareTag("Butterfly"))
        {
            t.GetComponentInParent<Butterfly>().TurnColliderToTrigger();
            PooledItem pi = t.GetComponentInParent<Butterfly>().GetComponent<PooledItem>();
            collectibles.Add(pi);
        }
    }
}
