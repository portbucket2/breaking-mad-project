﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(MovementController))]
public class PlayerManager : MonoBehaviour, IHitable, ICharacter, IAttacker, IVerticalCollidable {
    public static PlayerManager instance;
    public static IAttacker playerAttacker{get{return instance;} }
    //public static IHitable playerHitable { get { return instance; } }
    public static int CoinsAvailable()
    {
        return instance.scorePoints;
    }
    public static void PurchasePowerUp(PowerUpProfile profile, int cost)
    {
        playerAttacker.AddScore(-cost);
        instance.AddPowerUp(profile);
    }
    public event System.Action<int> OnPlayerReachedNewRow;

    public ComboManager comboMan
    {
        get
        {
            if (_comboMan == null)
                _comboMan = new ComboManager(LevelManager.instance.assets.comboPrefab,instance);
            return _comboMan;
        }
    }
    private ComboManager _comboMan;
    #region power up management

    public void AddPowerUp(PowerUpProfile profile)
    {
        if (newPowerUpAdded != null) newPowerUpAdded(profile);
        switch (profile.type)
        {
            case PowerUpType.BOMB:
                AddBombs(1);
                break;
            case PowerUpType.BOMB3:
                AddBombs(3);
                break;
            case PowerUpType.LIGHTNING:
            case PowerUpType.FIRE:
            case PowerUpType.HARDBOOT:
            case PowerUpType.SHIELD:
            case PowerUpType.MAGNET:
                AddBuff(profile);
                break;
            default:
                break;
        }
    }

    public List<PowerUpProfile> buffs = new List<PowerUpProfile>();
    void AddBuff(PowerUpProfile newBuff)
    {
        buffs.Add(newBuff);
        if (buffListChanged != null) buffListChanged(buffs);
        switch (newBuff.type)
        {
            case PowerUpType.LIGHTNING:
                EnableUpgrade_LightningHammer(true);
                break;
            case PowerUpType.FIRE:
                EnableUpgrade_FireHammer(true);
                break;
            case PowerUpType.MAGNET:
                EnableUpgrade_Magnet(true);
                break;
            case PowerUpType.HARDBOOT:
                EnableUpgrade_HardBoot(true);
                break;
            case PowerUpType.SHIELD:
                EnableUpgrade_Shield(true);
                break;
            default:
                break;
        }
    }

    void RemoveBuff(PowerUpType type)
    {
        for (int i = buffs.Count - 1; i >= 0; i++)
        {
            if (buffs[i].type == type)
            {
                buffs.RemoveAt(i);
                if (buffListChanged != null) buffListChanged(buffs);

                switch (type)
                {
                    case PowerUpType.LIGHTNING:
                        EnableUpgrade_LightningHammer(false);
                        break;
                    case PowerUpType.FIRE:
                        EnableUpgrade_FireHammer(false);
                        break;
                    case PowerUpType.MAGNET:
                        EnableUpgrade_Magnet(false);
                        break;
                    case PowerUpType.HARDBOOT:
                        EnableUpgrade_HardBoot(false);
                        break;
                    case PowerUpType.SHIELD:
                        EnableUpgrade_Shield(false);
                        break;
                    default:
                        break;
                }
                return;
            }
        }

    }
    private void AddBombs(int change)
    {
        bombCount += change;
        if (bombCountHasChanged != null) bombCountHasChanged(bombCount);
    }
    public void UseBomb()
    {
        GridPropagatingExplosive gpe = new GridPropagatingExplosive(playerController.GetCurrentLocus(),5);
        StartCoroutine(BombCo(gpe));
        AddBombs(-1);
    }
    IEnumerator BombCo(GridPropagatingExplosive gpe)
    {
        LevelAssets ass = LevelManager.instance.assets;
        foreach (List<IGridLocus> glist in gpe.nodes)
        {
            yield return new WaitForSeconds(0.2f);
            ////CM_Debglist.Count);
            foreach (IGridLocus item in glist)
            {
                if (ass.rc_HitableLayers_withColliderOrTrigger.DoubleCastAll(
                    ass.rc_HitableLayers_withColliderWithoutTrigger,
                    item.relativeLocus(Dir.DOWN).position,
                    Vector3.up,
                    LevelManager.gridUnitDimension))
                {
                    ////CM_Debass.rc_HitableLayers_withColliderOrTrigger.lastHitList.Count);
                    foreach (var rch in ass.rc_HitableLayers_withColliderOrTrigger.lastHitArray)
                    {
                        ////CM_Debrch.collider.GetComponent<MonoBehaviour>().name);
                        IHitable ihitable = rch.collider.GetComponentInParent<IHitable>();
                        if (ihitable != null && ihitable!=((IHitable)this) && !(ihitable as ShopItemTerrain))
                        {
                            ihitable.TakeHit(Dir.DOWN,this);
                            ((IAttacker)this).AddScore(ihitable.GetScoreOnHit());
                        }
                    }
                }
                featureManager.ShowBombParticleAt(item.position);
            }
        }
    }

    private bool hasLandExplosion = false;
    private bool hasDoubleRange = false;
    private bool hasMagnet = false;
    private bool hasHardBoot = false;
    private bool hasShield = false;

    public bool IsAttackUpgraded()
    {
        return hasDoubleRange;
    }
    public bool IsLandingingUpgradeEnabled()
    {
        return hasLandExplosion;
    }

    private void EnableUpgrade_FireHammer(bool enabled)
    {
        hasLandExplosion = enabled;
        featureManager.EnableUpgrade_FireHammer(enabled);
    }
    private void EnableUpgrade_LightningHammer(bool enabled)
    {
        hasDoubleRange = enabled;
        attackRange = enabled ? 2 : 1;
        featureManager.EnableUpgrade_LightningHammer(enabled);

    }
    private void EnableUpgrade_Magnet(bool enabled)
    {
        hasMagnet = enabled;
        featureManager.EnableUpgrade_Magnet(enabled);
    }
    private void EnableUpgrade_HardBoot(bool enabled)
    {
        hasHardBoot = enabled;
        featureManager.EnableUpgrade_HardBoot(enabled);
    }
    private void EnableUpgrade_Shield(bool enabled)
    {
        hasShield = enabled;
        featureManager.EnableUpgrade_Shield(enabled);
    }



    #endregion

    public GameObject gameOverObject;
    public PlayerFeatureManager featureManager;
    public bool logEnabled = false;
    public bool isDead = false;
    //public bool isUntargetable = false;

    [SerializeField] GameObject attackParticle;
    [SerializeField] int scorePoints = 0;
    [SerializeField] float attackCooldown = 0.5f;
    [SerializeField] float attackRange = 1;
    [SerializeField] int bombCount = 0;

    [Range(0, 1)]
    public float playerAttackHitTimeRatioForAllExceptDown = 0.1f;

    [Range(0, 1)]
    public float playerAttackHitTimeRatioForDownBlock = 0.8f;

    public event System.Action<int> scoreHasChanged;
    public event System.Action<int> bombCountHasChanged;
    public event System.Action<PowerUpProfile> newPowerUpAdded;
    public event System.Action<List<PowerUpProfile>> buffListChanged;
    public event System.Action playerTookFatalHit;

    float lastSquishTime;
    const float squishCD = 3;



    MovementController playerController;
    public int rowIndex { get { return playerController.rowIndex; } }
    public int colOffset { get { return playerController.colOffset; } }
    public bool isMoving { get { return playerController.IsMoving(); } }

    public bool isTutorialBusy { get { return playerController.IsFalling() || playerController.IsMoving() || playerController.IsAttacking();  } }
    public bool hasHitableAbove { get { return playerController.SearchHitablesJustAboveInRange() != null; } }
#if UNITY_EDITOR
    public bool runningInTestMode = false;
#else
    const bool runningInTestMode = false;
#endif

    private bool gameOver { get { return isDead &&!runningInTestMode; } }
    private void Awake()
    {
        instance=this;
        //gameOverObject.SetActive(false);
        playerController = this.GetComponent<MovementController>();
        playerController.Init(Dir.UP);

        playerController.rowChanged += (int row)=> { if (OnPlayerReachedNewRow != null) OnPlayerReachedNewRow(row); };
    }
    void Start()
    {
        attackRange = 1;
        bombCount = 0;
        scorePoints = 0;
        AddBombs(0);

        isProtected = false;

        playerAttacker.AddScore(0);
        EnableUpgrade_Shield(false);
        EnableUpgrade_HardBoot(false);
        EnableUpgrade_FireHammer(false);
        EnableUpgrade_LightningHammer(false);
        EnableUpgrade_Magnet(false);

        featureManager.Enable_ProtectionParticle(false);
        this.GetComponent<InputManager>().OnSwipe += OnSwipe;

        UIManager.instance.OnPlayerWasSaved += OnPlayerSaved;
    }
    private class ActionCommand
    {
        private Dir dir;
        private MovementController mc;
        public bool executedSuccesfully;
        public ActionCommand(Dir dir, MovementController mc)
        {
            this.mc = mc;
            this.dir = dir;
        }
        public void ExecuteIfControllerIsNotBusy()
        {
            executedSuccesfully = mc.Command_TakeAction(dir);
        }

    }

    ActionCommand pendingCommand;
    void OnSwipe(Dir d)
    {
        if (!gameOver)
        {
            bool success = playerController.Command_TakeAction(d);
            if (!success)
            {
                //if(pendingCommand!=null)NLog.B_AppendLastLog(" X ");
                pendingCommand = new ActionCommand(d, playerController);
            }
            else
            {
                //NLog.B_Log("Direct");
                pendingCommand = null;
            }
        }
    }

    void Update()
    {


        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Break();
        }
        if (gameOver) return;
        
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            OnSwipe(Dir.UP);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            OnSwipe(Dir.DOWN);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            OnSwipe(Dir.LEFT);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            OnSwipe(Dir.RIGHT);
        }
        else if (pendingCommand!=null)
        {
            pendingCommand.ExecuteIfControllerIsNotBusy();
            if (pendingCommand.executedSuccesfully) {

                NLog.B_Log("Pending");
                pendingCommand = null;
            }
        }
    }

    
    float ICharacter.GetPosXDifferenceFrom(ICharacter from)
    {
        return ((ICharacter)this).GetGO().transform.position.x - from.GetGO().transform.position.x;
    }
    Dir ICharacter.GetRelativeDirectionFrom(ICharacter form)
    {
        if (((ICharacter)this).GetPosXDifferenceFrom(form) > 0) return Dir.RIGHT;
        else return Dir.LEFT;
    }
    bool ICharacter.logEnabled
    {
        get
        {
            return logEnabled;
        }
    }
    void ICharacter.Init()
    {
        isDead = false;
    }
    bool ICharacter.isDead
    {
        get;
        set;
    }
    bool ICharacter.canCrawlWalls
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.canRetreat
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.canFly
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.isUntargetable
    {
        get { return isDead; }
        set { isDead = false; }
    }
    MovementController ICharacter.GetMovementController()
    {
        return playerController;
    }
    Collider ICharacter.togglingCollider_OnMove
    {
        get
        {
            return null;
        }
    }

    IEnumerator AttackParticleCoroutine(Vector3 point)
    {
        //ParticleSystem ps = FishSpace.Pool.Instantiate(attackParticle, point, Quaternion.identity).GetComponent<ParticleSystem>();
        //ps.Play();
        yield return new WaitForSeconds(0.2f);
        //ps.Stop();
        //FishSpace.Pool.Destroy(ps.gameObject);
    }

    public int GetLastScore()
    {
        return scorePoints;
    }

    IAttacker assasinReference;
    IGridLocus preDeathLocus;
    Vector3 preDeathCamPos;
    void TakeFatalHit(Dir fatalDir,IAttacker assasin)
    {
        isDead = true;
        assasinReference = assasin;
        preDeathLocus = playerController.GetCurrentLocus();
        preDeathCamPos = DampedFollower.mainCamPosTransform.position;

        Vector3 forceVec = MovementController.Dir2Vec(fatalDir) *3f + new Vector3(0, 2f, 0);
        float x = Random.Range(-0.5f, 0.5f);
        //float y = Random.Range(-1.0f, 1.0f);
        float z = Random.Range(-1.0f, 1.0f);
        Vector3 torqueVec = new Vector3(x, 0, z).normalized * 0.1f;


        playerController.Command_DropToAbyss(fatalDir,forceVec,torqueVec);

        if (playerTookFatalHit != null) playerTookFatalHit();
    }
    void OnPlayerSaved()
    {
        isDead = false;
        StartCoroutine(RunProtection());
        

        playerController.Command_ResetDropEffects(Dir.UP, preDeathLocus);
        DampedFollower.mainCamPosTransform.position = preDeathCamPos;
        ////CM_DebassasinReference);
        if (assasinReference != null)
        {
            assasinReference.NullifyThreat(this);
            assasinReference = null;
        }
    }
    bool isProtected;
    IEnumerator RunProtection()
    {
        isProtected = true;
        featureManager.Enable_ProtectionParticle(true);
        yield return new WaitForSecondsRealtime(2.5f);

        featureManager.Enable_ProtectionParticle(false);
        yield return new WaitForSecondsRealtime(1);
        UIManager.instance.NowActivateWatchVideoButton();
        isProtected = false;
    }


    GameObject IHitable.GetGO()
    {
        return this.gameObject;
    }
    int IHitable.GetScoreOnHit()
    {
        return 0;
    }
    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        ////CM_Deb"Killed by {0}", (attacker as MonoBehaviour));
        //if (attacker as HayCart)
        //{
        //    Debug.Break();
        //}
        if (GlobalSettings.instance.invincibility || isProtected) return;
        if (isDead) return;

        if (attacker != null)
        {
            BMadAnalyticsHandler.ReportLevelOfDeath(LevelManager.instance.currentLevelNumber);
            BMadAnalyticsHandler.ReportReasonOfDeath(attacker.attackerid);
            ////CM_Deb"Killed by {0} at level {1}", attacker.attackerid, LevelManager.instance.GetCurrentLevelNumber());

        }
        else
        {
            BMadAnalyticsHandler.ReportLevelOfDeath(LevelManager.instance.currentLevelNumber);
            BMadAnalyticsHandler.ReportReasonOfDeath("unknown");
            Debug.LogErrorFormat("Killed by {0} at level {1}", "unknown", LevelManager.instance.currentLevelNumber);
        }

        if (hasShield)
        {
            RemoveBuff(PowerUpType.SHIELD);
        }
        else
        {
            TakeFatalHit(dir,attacker);
        }
        featureManager.Play_TakeHitParticle();
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Body, this.transform.position);
    }

    float IHitable.xPosition { get { return this.transform.position.x; } }
    float IHitable.MaxHitDistance()
    {
        return 10000;
    }

    string IAttacker.attackerid
    {
        get
        {
            return "player";
        }
    }
    void IAttacker.AddScore(int score)
    {
        //if (isDead)
        //{
        //    //CM_Deb"Score not accepted due to being dead...");
        //    return;
        //}

        scorePoints += score;
        if (scoreHasChanged != null) scoreHasChanged(scorePoints);
    }

    float IAttacker.GetAttackHittingTime()
    {
        return attackCooldown;
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return 0;
    }
    float IAttacker.GetAttackRange()
    {
        return attackRange +0.495f;
    }
    void IAttacker.TriggerAttackEffects(Vector3 point)
    {
        //StartCoroutine(AttackParticleCoroutine(point));
        featureManager.PlayAttackParticle(playerController.GetFaceDir());
    }
    void IAttacker.NullifyThreat(IAttacker nullifier)
    {

    }

    //[SerializeField]
    //SparkEventDescriptor unBreakableUp, unBreakableDown, unBreakableLeft, unBreakableRight,
    //    breakableUp, breakableDown, breakableLeft, breakableRight, enemyUp, enemyDown, enemyLeft, enemyRight;
    //[System.Serializable]
    //class SparkEventDescriptor
    //{
    //    public AnimationClip clip;
    //    public float whenSparkHappenInNormalizedTime;
    //}

    

    //float GetDelay(SparkEventDescriptor descrip)
    //{
    //    float cooldown = ((IAttacker)this).GetAttackCooldown();
    //    float damageDelay = playerController.attackHitTimeRatio;
    //    float offset = cooldown * damageDelay;

    //    float animationTime = descrip.clip.length;
    //    float sparkNormalizedTime = descrip.whenSparkHappenInNormalizedTime;
    //    float sparkHappenTime = animationTime * sparkNormalizedTime;
    //    float finalTime = sparkHappenTime - offset;
    //    //CM_Deb"cooldown: " + cooldown + " damageDelay: " + damageDelay + " offset: " + offset + " animation time: " + animationTime
    //        + " sparkNormalized time: " + sparkNormalizedTime + " spark happen time: " + sparkHappenTime + " final time: " + finalTime);
    //    return finalTime;
    //}

    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable)
    {

        //    if (dir == Dir.UP) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play up unbreakable particle"); }, PlayerManager.instance.upNonHitableParticleDelay); }
        //    else if (dir == Dir.DOWN) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play down unbreakable particle"); }, PlayerManager.instance.downNonHitableParticleDelay); }
        //    else if (dir == Dir.LEFT) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play left unbreakable particle"); }, PlayerManager.instance.leftNonHitableParticleDelay); }
        //    else { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play right unbreakable particle"); }, PlayerManager.instance.rightNonHitableParticleDelay); }

        //value = normalized time spark event * animation length - cooldown time * dynamic ratio
        //float up = unbreakable ? GetDelay(unBreakableUp) : GetDelay(breakableUp); //
        //up = up < 0 ? 0 : up;

        //float down = unbreakable ? GetDelay(unBreakableDown) : GetDelay(breakableDown); //
        //down = down < 0 ? 0 : down;

        //float left = unbreakable ? GetDelay(unBreakableLeft) : GetDelay(breakableLeft); //
        //left = left < 0 ? 0 : left;

        //float right = unbreakable ? GetDelay(unBreakableRight) : GetDelay(breakableRight); //
        //right = right < 0 ? 0 : right;

        //if (direction == Dir.UP) { //CM_Deb"up is: " + up); return up; }
        //else if (direction == Dir.DOWN) { //CM_Deb"down is: " + down); return down; }//else if (direction == Dir.DOWN) { return unbreakable ? GetDelay(nonBreakableDown) : GetDelay(breakableDown); } //
        //else if (direction == Dir.LEFT) { //CM_Deb"left is: " + left); return left; }
        //else { //CM_Deb"right is: " + right); return right; }
        return 0f;

    }

    float IAttacker.EnemyHitParticleDelay(Dir direction) {

        //float up = GetDelay(enemyUp); //
        //up = up < 0 ? 0 : up;

        //float down = GetDelay(enemyDown); //
        //down = down < 0 ? 0 : down;

        //float left = GetDelay(enemyLeft); //
        //left = left < 0 ? 0 : left;

        //float right = GetDelay(enemyRight); //
        //right = right < 0 ? 0 : right;

        //if (direction == Dir.UP) { return up; }
        //else if (direction == Dir.DOWN) { return down; }
        //else if (direction == Dir.LEFT) { return left; }
        //else { return right; }
        return 0f;
    }

    #region ISquishParticipant    
    void IVerticalCollidable.ResolveAsMasterEntity(IVerticalCollidable slave, Dir squishDir)
    {
        int score = slave.GetEnslaved_AndReturnScore(squishDir, this);
        ((IAttacker)this).AddScore(score);

    }

    int IVerticalCollidable.GetEnslaved_AndReturnScore(Dir squishDir, IAttacker squishAttacker)
    {
        ////CM_Deb"tried to squish player: {0}", playerController.IsFalling());
        if (Time.time < lastSquishTime + squishCD) return 0;
        lastSquishTime = Time.time;
        ////CM_Deb"got squished  mefalling: {0}", playerController.IsFalling());
        IHitable hitable = ((IHitable)this);
        hitable.TakeHit(squishDir,squishAttacker);
        return hitable.GetScoreOnHit();
    }
    bool IVerticalCollidable.HasTopSpike()
    {
        return false;
    }
    bool IVerticalCollidable.HasHardBoot()
    {
        return hasHardBoot;
    }
    bool IVerticalCollidable.IsFalling()
    {
        return playerController.IsFalling();
    }
    #endregion
}

