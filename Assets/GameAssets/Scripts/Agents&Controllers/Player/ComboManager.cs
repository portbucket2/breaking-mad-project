﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class ComboManager
{
    public ComboManager(GameObject prefab,MonoBehaviour mono)
    {
        this.prefab = prefab;
        this.mono = mono;
    }
    private GameObject prefab;
    private MonoBehaviour mono;

    //private static float[] max_time = new float[] {1.5f,  1.0f,   1.0f,   1.0f };
    private const float FIRST_COMBO_TIMER = 1.1f;
    private const float NEXT_COMBO_TIMER = 0.75f;

    public const int MIN_COMBO = 2;
    public const int MAX_COMBO = 5;
    public float timeAllowedToRaiseCombo
    {
        get
        {
            if (comboCounter < 1)
            {
                Debug.LogError("this should never happen,plz check it!");
                return -1;
            }
            else if (comboCounter == 1)
            {
                return FIRST_COMBO_TIMER;
            }
            else
            {
                return NEXT_COMBO_TIMER;
            }

        }
    }


    float lastKillTime=-100;
    int comboCounter = 1;
    public void ReportKill(Vector3 position,int score)
    {
        if (Time.time > lastKillTime + timeAllowedToRaiseCombo)
        {
            comboCounter = 1;
        }
        else
        {
            comboCounter++;
        }
        lastKillTime = Time.time;
        if(comboCounter>=MIN_COMBO)
        {
            TriggerCombo(comboCounter,position,score);
        }
    }
    private void TriggerCombo(int x,Vector3 position,int score)
    {
        //Debug.LogFormat("COMBO!!! {0}x", x);
        Animator anim= Pool.Instantiate(prefab,position,Quaternion.identity).GetComponent<Animator>();
        int bonusScore = score * (x-1);
        PlayerManager.playerAttacker.AddScore(score);
        LevelManager.Show_ComboScoreEarned(score);

        switch (x)
        {
            case 2:
                anim.SetTrigger("x2");
                break;
            case 3:
                anim.SetTrigger("x3");
                break;
            case 4:
                anim.SetTrigger("x4");
                break;
            default:
                anim.SetTrigger("xMega");
                break;
        }

        Centralizer.Add_DelayedMonoAct(mono,
            ()=>{
                Pool.Destroy(anim.gameObject);
            },2);
    }
}
