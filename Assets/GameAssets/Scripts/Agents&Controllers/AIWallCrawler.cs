﻿using FishSpace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWallCrawler : AIGuard, ICharacter, IHitable, IDestructible, IAttacker, IVerticalCollidable {
    public override void OnUpdate()
    {
        if (controller.IsMoving() || controller.IsAttacking())
        {
            //ICharacter overlappingWith = controller.SearchEnemiesJustAhead(0.5f);//any character overlapping?
            //if (overlappingWith != null)
            //{
            //    MovementController ovCon = overlappingWith.GetMovementController();
            //    controller.RequestRetreat();
            //    if (ovCon != null)
            //    {
            //        ovCon.RequestRetreat(true);
            //    }
            //}
        }
        else if (!isDead)
        {
            bool success = false;
            if (controller.SearchCharactersJustAhead() != null)
            {
                if (enableLog) Debug.Log("Enemy");
                success = controller.Command_Attack();
            }
            else
            {
                if (enableLog) Debug.Log("NoEnemy");
                success =  controller.Command_Forward(null);
            }

            if (!success) (this as IHitable).TakeHit(Dir.DOWN,this);
        }
    }

    public override void OnInit()
    {
        base.OnInit();      
        //mainCol.isTrigger = false;
    }
    public override void ForceToAbyss(Dir dir)
    {
        Vector3 forceVec = MovementController.Dir2Vec(dir) * 2.5f + new Vector3(0, 5f, 0);
        float x = 0;
        float y = 0;
        float z = Random.Range(-1.0f, 1.0f);
        Vector3 torqueVec = new Vector3(x, y, z).normalized * 5;
        controller.Command_DropToAbyss(dir, forceVec, torqueVec);
    }

    public override void PlayTypeSpecificDeathSound()
    {
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Spider, this.transform.position);
    }
    public override void PlayTypeSpecificAttackSound()
    {
        SoundKeeper.PlayEnemySound(EnemyAudioType.spider_attack, this.transform.position);
    }

    void IVerticalCollidable.ResolveAsMasterEntity(IVerticalCollidable slave, Dir squishDir)
    {
        slave.ResolveAsMasterEntity(this, MovementController.OppositeDir(squishDir));
    }

    //public void OnCollisionEnter(Collision collision)
    //{
    //    ICharacter ch = collision.collider.GetComponentInParent<ICharacter>();
    //    if (ch != null)
    //    {

    //    }
    //}
    #region Interface Overrides
    bool ICharacter.canCrawlWalls
    {
        get
        {
            return true;
        }
    }
    bool ICharacter.canRetreat
    {
        get
        {
            return false;
        }
    }
    #endregion
}
