﻿using FishSpace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIGuard : MonoBehaviour,ICharacter, IHitable, IDestructible, IAttacker,IVerticalCollidable {

    private static int AIIndex;

    public bool enableLog;

    [SerializeField] protected string agentTypeName;
    [SerializeField] protected bool isPatrol;
    [SerializeField] protected bool isDead;
    [SerializeField] protected bool isUntargetable;
    //[SerializeField] bool isTargetable;
    [SerializeField] bool hasSpike;
    [SerializeField] List<Collider> deathDisableColliders;

    [Header("Attack Information")]
    [SerializeField] int scorePoints = 10;
    [SerializeField] float attackPrepTime= 0;
    [SerializeField] float attackCooldown = 1;
    [SerializeField] float attackRange = 1;
   // [SerializeField] ParticleSystem attackParticle;

    protected MovementController controller;
    private bool initializedOnce = false;
    private int killPoints;

    private void Start()
    {
        gameObject.name = string.Format("Agent {0}({1})", AIIndex++, agentTypeName);
        if (!initializedOnce)
        {
            OnInit();
        }

        HitBaseTerrain.OnPlayerHitTerrain += EnablePatrolPossibilityCheck;
    }

    int didntPatrolTicks = 0;
    const int maxNoPatrolTick = 60;
    private void Update()
    {
        if (isDead) return;
        if (!patrolMayBePossible)
        {
            didntPatrolTicks++;
            if (didntPatrolTicks > maxNoPatrolTick)
            {
                didntPatrolTicks = 0;
                EnablePatrolPossibilityCheck();
            }
        }
        
        OnUpdate();
    }
    public virtual void OnInit()
    {
        //if (enableLog) //CM_Deb("Ai is being initialized");
        patrolMayBePossible = true;
        initializedOnce = true;
        isDead = false;
        isUntargetable = false;
        killPoints = 0;

        controller = this.GetComponent<MovementController>();
        Dir d = (Random.Range(0, 2) == 0) ? Dir.RIGHT : Dir.LEFT;
        controller.Init(d);

        StopAllCoroutines();
        didntPatrolTicks = Random.Range(0,maxNoPatrolTick);

        foreach (var item in deathDisableColliders)
        {
            item.enabled = true;
        }
    }

    protected bool patrolMayBePossible;
    protected bool shouldTryToPatrol
    {
        get
        {
            if (!isPatrol) return false;
            else return patrolMayBePossible;
        }
    }
    protected void StopTryingToPatrol()
    {
        patrolMayBePossible = false;
    }
    private void EnablePatrolPossibilityCheck()
    {
        patrolMayBePossible = true;
    }

    public virtual void OnUpdate()
    {
        if (controller.IsMoving())
        {
            ICharacter overlappingWith = controller.SearchCharactersJustAhead(0.5f); //any character overlapping?
            if (overlappingWith == null) overlappingWith= controller.GetOverLapingCharacter();
            if (overlappingWith != null)
            {
                MovementController ovCon = overlappingWith.GetMovementController();

                controller.RequestRetreat();
                if (ovCon != null)
                {
                    ovCon.RequestRetreat(true);
                }
            }
        }
        else
        {
            ICharacter targetJustAhead = GetTarget();

            if (TutorialScript.isTutorialRunning)
            {
                if (targetJustAhead != null)
                {
                    controller.Command_Attack();
                }
                else if (controller.ShouldTurnAround())
                {
                    controller.Command_SetDirection(controller.GetTailDir());
                }

            }
            else
            {
                if (targetJustAhead != null)
                {
                    controller.Command_TakeAction(targetJustAhead.GetRelativeDirectionFrom(this));
                }
                else if (shouldTryToPatrol)
                {
                    if (controller.CanSafeWalk_Forward(allowToJump: false))
                    {
                        //if (enableLog) //CM_Deb("can forward");
                        controller.Command_Forward(null);
                    }
                    else if (controller.ShouldTurnAround())
                    {
                        controller.Command_SetDirection(controller.GetTailDir());
                    }
                    else
                    {
                        StopTryingToPatrol();
                    }
                }
            }


               
        }
    }

    public virtual ICharacter GetTarget()
    {
        return controller.SearchCharactersJustAhead();
    }

    public virtual void ForceToAbyss(Dir dir)
    {
        Vector3 forceVec = MovementController.Dir2Vec(dir) * 2.5f + new Vector3(0, 5f, 0);
        float x = Random.Range(-1.0f, 1.0f);
        float y = Random.Range(-1.0f, 1.0f);
        float z = Random.Range(-1.0f, 1.0f);
        Vector3 torqueVec = new Vector3(x, y, z).normalized * 5;
        controller.Command_DropToAbyss(dir, forceVec, torqueVec);
    }

    public virtual void PlayTypeSpecificDeathSound()
    {
        SoundKeeper.PlayHitSound(hasSpike?HitAudioType.Hit_Porcupine:HitAudioType.Hit_Guard, this.transform.position);
    }
    public virtual void PlayTypeSpecificAttackSound()
    {
        SoundKeeper.PlayEnemySound(hasSpike ? EnemyAudioType.porcupine_attack : EnemyAudioType.guard_attack, this.transform.position);
    }
    public void SelfDestruct()
    {
        foreach (var item in deathDisableColliders)
        {
            item.enabled = false;
        }
        Pool.Destroy(gameObject);
        isUntargetable = true;
    }

    //IEnumerator AttackParticleCoroutine(Vector3 point)
    //{
    //    //ParticleSystem ps = FishSpace.Pool.Instantiate(attackParticle, point, Quaternion.identity).GetComponent<ParticleSystem>();
    //    attackParticle.Play();
    //    yield return new WaitForSeconds(ps.main.duration);
    //    attackParticle.ps.Stop();
    //    //FishSpace.Pool.Destroy(ps.gameObject);
    //}

    #region IHitable and IDestructible
    int IHitable.GetScoreOnHit()
    {
        return scorePoints + killPoints;
    }
    GameObject IHitable.GetGO()
    {
        return this.gameObject;
    }
    void IHitable.TakeHit(Dir dir, IAttacker attacker)/*HOT*/
    {
        //if (this as AIChaser)
        //{
        //    Debug.LogError(dir);
        //}
        if (attacker == PlayerManager.playerAttacker)
        {
            PlayerManager.instance.comboMan.ReportKill(this.transform.position,scorePoints);
        }

        if (TutorialScript.isTutorialRunning) TutorialScript.FightIsOver();

        SoundKeeper.PlayHitSound(HitAudioType.Hit_Body, this.transform.position);
        PlayTypeSpecificDeathSound();
        isUntargetable = true;
        isDead = true;

        ForceToAbyss(dir);
        //Pool.Destroy(gameObject);
        Centralizer.Add_DelayedMonoAct(this, () => {
            SelfDestruct();
        }, 2);

        ////CM_DebWarning("here!");

        float delay = attacker == null ? 0 : attacker.EnemyHitParticleDelay(dir);
        FishSpace.Centralizer.Add_DelayedAct(() =>
        {
            LevelManager.Play_DefaultEnemyHitParticle(this);
            //if (attacker == PlayerManager.playerAttacker)
            //{
            //    //CM_DebWarning("Hit particle will be played because enemy was hit from direction: " + dir);
            //    Debug.Break();
            //}
            
        }, delay);

        LevelManager.Show_ScoreEarned(this.transform.position, (this as IHitable).GetScoreOnHit(), attacker);
        //rumman
        //if (dir == Dir.UP) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultEnemyHitParticle(this); }, PlayerManager.instance.upHitableParticleDelay); }
        //else if (dir == Dir.DOWN) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultEnemyHitParticle(this); }, PlayerManager.instance.downHitableParticleDelay); }
        //else if (dir == Dir.LEFT) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultEnemyHitParticle(this); }, PlayerManager.instance.leftHitableParticleDelay); }
        //else { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultEnemyHitParticle(this); }, PlayerManager.instance.rightHitableParticleDelay); }

        //rumman
        //LevelManager.Play_DefaultEnemyHitParticle(this);
    }
    float IHitable.MaxHitDistance()
    {
        return 10000;
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }
    void IDestructible.SelfDestruct()/*HOT*/
    {
        SelfDestruct();
    }
    #endregion
    #region IAttacker
    string IAttacker.attackerid
    {
        get
        {
            return agentTypeName;
        }
    }
    void IAttacker.AddScore(int score)/*HOT*/
    {
        killPoints+=score;
    }
    float IAttacker.GetAttackHittingTime()
    {
        return attackCooldown;
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return attackPrepTime;
    }
    float IAttacker.GetAttackRange()
    {
        return attackRange;
    }
    void IAttacker.TriggerAttackEffects(Vector3 point)
    {
        //if (attackParticle != null) attackParticle.Play();
        PlayTypeSpecificAttackSound();
        //StartCoroutine(AttackParticleCoroutine(point));
    }
    void IAttacker.NullifyThreat(IAttacker nullifier)
    {
        (this as IHitable).TakeHit(Dir.DOWN,nullifier);
    }

    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable){ return 0; }
    float IAttacker.EnemyHitParticleDelay(Dir direction) { return 0; }
    #endregion
    #region ICharacter
    void ICharacter.Init()
    {
        OnInit();
    }
    float ICharacter.GetPosXDifferenceFrom(ICharacter from)
    {
        return ((ICharacter)this).GetGO().transform.position.x - from.GetGO().transform.position.x;
    }
    Dir ICharacter.GetRelativeDirectionFrom(ICharacter form)
    {
        if (((ICharacter)this).GetPosXDifferenceFrom(form) > 0) return Dir.RIGHT;
        else return Dir.LEFT;
    }
    bool ICharacter.logEnabled
    {
        get
        {
            return enableLog;
        }
    }
    bool ICharacter.canCrawlWalls
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.canRetreat
    {
        get
        {
            return true;
        }
    }
    bool ICharacter.canFly
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.isDead
    {
        get
        {
            return isDead;
        }
        set
        {
            isDead = value;
        }
    }
    bool ICharacter.isUntargetable
    {
        set { isUntargetable = value; }
        get { return isUntargetable; }
    }
    Collider ICharacter.togglingCollider_OnMove
    {
        get
        {
            return null;
        }
    }
    MovementController ICharacter.GetMovementController()
    {
        return controller;
    }
    #endregion
    #region ISquishParticipant
    void IVerticalCollidable.ResolveAsMasterEntity(IVerticalCollidable slave, Dir squishDir)
    {
        int score = slave.GetEnslaved_AndReturnScore(squishDir, this);
        ((IAttacker)this).AddScore(score);

    }
    int IVerticalCollidable.GetEnslaved_AndReturnScore(Dir squishDir, IAttacker masterAttacker)
    {
        IHitable hitable = ((IHitable)this);
        hitable.TakeHit(squishDir,masterAttacker);
        return hitable.GetScoreOnHit();
    }
    bool IVerticalCollidable.HasTopSpike()
    {
        return hasSpike;
    }
    bool IVerticalCollidable.HasHardBoot()
    {
        return false;
    }
    bool IVerticalCollidable.IsFalling()
    {
        return controller.IsFalling();
    }
    #endregion
}
