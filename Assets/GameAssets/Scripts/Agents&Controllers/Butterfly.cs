﻿using UnityEngine;
using System.Collections;
using FishSpace;

public class Butterfly : MonoBehaviour, ICharacter, IHitable, IDestructible,IVerticalCollidable
{
    [SerializeField] bool isUntargetable;
    [SerializeField] int scoreAvailable = 50;
    [SerializeField] GameObject destroyParticle;
    [SerializeField] Collider colliderRef;
    public void TurnColliderToTrigger()
    {
        colliderRef.isTrigger = true;
    }
    public void SelfDestruct()
    {
        FishSpace.Pool.Destroy(gameObject);
    }
    private void OnInit()
    {
        colliderRef.isTrigger = false;
        isUntargetable = false;
    }

    #region hitable and destructible
    GameObject IHitable.GetGO()
    {
        return this.gameObject;
    }
    int IHitable.GetScoreOnHit()
    {
        return scoreAvailable;
    }
    float IHitable.MaxHitDistance()
    {
        return 10000;
    }
    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Butterfly, this.transform.position);
        SelfDestruct();
        isUntargetable = true;
        GameObject pg = Pool.Instantiate(destroyParticle, this.transform.position + new Vector3(0, LevelManager.gridUnitDimension*0.5f, -2.5f), Quaternion.identity);
        Centralizer.Add_DelayedAct(() => { if(pg!=null)Pool.Destroy(pg); }, 4);


        LevelManager.Show_ScoreEarned(this.transform.position, scoreAvailable,attacker);

    }
    float IHitable.xPosition { get { return this.transform.position.x; } }
    void IDestructible.SelfDestruct()
    {
        SelfDestruct();
    }
    #endregion
    #region character
    bool ICharacter.logEnabled
    {
        get
        {
            return false;
        }
    }
    void ICharacter.Init()
    {
       OnInit();
    }

    float ICharacter.GetPosXDifferenceFrom(ICharacter from)
    {
        return ((ICharacter)this).GetGO().transform.position.x - from.GetGO().transform.position.x;
    }
    Dir ICharacter.GetRelativeDirectionFrom(ICharacter form)
    {
        if (((ICharacter)this).GetPosXDifferenceFrom(form) > 0) return Dir.RIGHT;
        else return Dir.LEFT;
    }

    bool ICharacter.isDead
    {
        get
        {
            return false;
        }
        set
        {
        }
    }

    bool ICharacter.canCrawlWalls
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.canRetreat
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.isUntargetable
    {
        get { return isUntargetable; }
        set { isUntargetable = value; }
    }
    MovementController ICharacter.GetMovementController()
    {
        return null;
    }
    bool ICharacter.canFly
    {
        get
        {
            return false;
        }
    }
    Collider ICharacter.togglingCollider_OnMove
    {
        get
        {
            return null;
        }
    }
    #endregion
    #region ISquishParticipant
    void IVerticalCollidable.ResolveAsMasterEntity(IVerticalCollidable slave,Dir squishDir)
    {
        slave.ResolveAsMasterEntity(this, MovementController.OppositeDir(squishDir));
    }
    int IVerticalCollidable.GetEnslaved_AndReturnScore(Dir squishDir, IAttacker masterAttacker)
    {
        IHitable hitable = ((IHitable)this);
        hitable.TakeHit(squishDir,masterAttacker);
        return hitable.GetScoreOnHit();
    }
    bool IVerticalCollidable.HasTopSpike()
    {
        return false;
    }
    bool IVerticalCollidable.HasHardBoot()
    {
        return false;
    }
    bool IVerticalCollidable.IsFalling()
    {
        return false;
    }
    #endregion
}
