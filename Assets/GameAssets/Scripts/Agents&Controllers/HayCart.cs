﻿using FishSpace;
using UnityEngine;

public class HayCart : MonoBehaviour,ICharacter,IHitable,IDestructible,IVerticalCollidable,IAttacker {

    public bool enableLog;
    protected MovementController controller;
    bool isInitialized;
    bool isDead;
    public GameObject smashParticle;
    public Collider triggerToggleCollider;

    ICharacter selfChar;
    private void Start()
    {
        if (!isInitialized)
            OnInit();

        selfChar = this;

    }
    private void OnInit()
    {
        isInitialized = true;
        isDead = false;
        if (enableLog) Debug.Log("haycart is being initialized");

        controller = this.GetComponent<MovementController>();
        controller.Init(Dir.UP);
    }

    private void Update()
    {
        if (controller.IsMoving())
        {
            ICharacter ichar = controller.SearchCharactersJustAhead();
            if (ichar != null)
            {
                ichar.TakeHit(controller.GetFaceDir(),this);
                ((IAttacker)this).AddScore(ichar.GetScoreOnHit());
            }
        }
    }

    bool isSliding;
    Dir slidingDir;
    //private void Slide(Dir dir,bool explodeIfCantSlide)
    //{
    //    controller.Command_SetDirection(dir, false);
    //    if (controller.CanSlideForward())
    //    {
    //        isSliding = true;
    //        controller.Command_Forward(

    //            () =>
    //            {
    //                Slide(dir,false);
    //            });
    //    }
    //    else
    //    {
    //        isSliding = false;

    //        if (explodeIfCantSlide)
    //        {
    //            //Debug.Log("cant slied will die");
    //            StartSelfDestruct();
    //        }
    //    }

    //    //else Debug.Log("cant slied not today");
    //}


    void SlideStart(Dir dir)
    {
        isSliding = true;
        slidingDir = dir;
        MakeSlidingMove(dir);
    }
    void MakeSlidingMove(Dir dir)
    {
        controller.Command_SetDirection(dir, false);
        controller.Command_Forward(

            () =>
            {
                OnSlideEnd(dir);
            });
    }
    void OnSlideEnd(Dir dir)
    {
        if (controller.CanSlideForward(dir))
        {
            MakeSlidingMove(dir);
        }
        else
        {
            isSliding = false;
        }
    }



    GameObject IHitable.GetGO()
    {
        return this.gameObject;
    }
    int IHitable.GetScoreOnHit()
    {
        return 0;
    }
    float IHitable.MaxHitDistance()
    {
        return 10000;
    }
    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        if (isDead) return;
        if (isSliding || !controller.CanSlideForward(dir))
        {

            StartSelfDestruct();
        }
        else
        {
            SlideStart(dir);
        }
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }
    

    #region character
    bool ICharacter.logEnabled
    {
        get
        {
            return enableLog;
        }
    }
    void ICharacter.Init()
    {
        OnInit();
    }
    float ICharacter.GetPosXDifferenceFrom(ICharacter from)
    {
        return ((ICharacter)this).GetGO().transform.position.x - from.GetGO().transform.position.x;
    }
    Dir ICharacter.GetRelativeDirectionFrom(ICharacter form)
    {
        if (((ICharacter)this).GetPosXDifferenceFrom(form) > 0) return Dir.RIGHT;
        else return Dir.LEFT;
    }
    bool ICharacter.canCrawlWalls
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.canRetreat
    {
        get
        {
            return false;
        }
    }
    bool ICharacter.canFly
    {
        get
        {
            return false;
        }
    }
    MovementController ICharacter.GetMovementController()
    {
        return controller;
    }


    bool ICharacter.isDead
    {
        get
        {
            return false;
        }
        set
        {
        }
    }
    bool ICharacter.isUntargetable
    {
        get; set;
    }

    Collider ICharacter.togglingCollider_OnMove
    {
        get
        {
            return triggerToggleCollider;
        }
    }

    #endregion
    #region random
    void IVerticalCollidable.ResolveAsMasterEntity(IVerticalCollidable slave, Dir squishDir)
    { 

        int score = slave.GetEnslaved_AndReturnScore(squishDir,this);
        ((IAttacker)this).AddScore(score);

    }
    int IVerticalCollidable.GetEnslaved_AndReturnScore(Dir squishDir, IAttacker masterAttacker)
    {
        return 0;
    }
    bool IVerticalCollidable.HasTopSpike()
    {
        return false;
    }
    bool IVerticalCollidable.HasHardBoot()
    {
        return true;
    }
    bool IVerticalCollidable.IsFalling()
    {
        return controller.IsFalling();
    }
    void IDestructible.SelfDestruct()
    {
        StartSelfDestruct();
    }
    public void StartSelfDestruct()
    {
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Cart, this.transform.position);
        isDead = true;
        FishSpace.Pool.Destroy(gameObject);

        GameObject pg = Pool.Instantiate(smashParticle, this.transform.position + new Vector3(0, LevelManager.gridUnitDimension * 0.5f, -2.5f), Quaternion.identity);
        Centralizer.Add_DelayedAct(() => { Pool.Destroy(pg); }, 4);
    }

    string IAttacker.attackerid
    {
        get
        {
            return "haycart";
        }
    }
    void IAttacker.AddScore(int score)
    {
        LevelManager.Show_ScoreEarned(this.transform.position, score, PlayerManager.playerAttacker);
        PlayerManager.playerAttacker.AddScore(score);
    }

    float IAttacker.GetAttackHittingTime()
    {
        return 1000;
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return 0;
    }
    float IAttacker.GetAttackRange()
    {
        return 0.6f;
    }

    void IAttacker.TriggerAttackEffects(Vector3 point)
    {
        
    }
    void IAttacker.NullifyThreat(IAttacker nullifier)
    {
        (this as IHitable).TakeHit(Dir.DOWN, nullifier);
    }
    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable) { return 0; }
    float IAttacker.EnemyHitParticleDelay(Dir direction) { return 0; }
    #endregion

}
