﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class Pressurizer : MonoBehaviour, IAttacker {
    
    [SerializeField]bool initialFlyPending = false;
    public Animator anim;
    public BossModelScript modelRef;
    public Transform modelPart;
    public Transform firePos;
    public GameObject fireWithParticle;
    public GameObject fireNoParticle;
    //public ParticleSystem trailGasParticle;
    public Animator camAnim;
    public float fireSpeed;

    bool flying = false;//sound only
    public bool IsFlying()
    {
        return flying;
    }

    bool isBusy = false;


    public static Pressurizer instance;
    private WindowTerrain window;
	// Use this for initialization
	void Start () {
        instance = this;
        sleeping = true;
        initialFlyPending = true;
        flying = false;
        isBusy = false;
        HitBaseTerrain.OnPlayerHitTerrain += WakeUp;
	}
    //private void OnDestroy()
    //{
    //    HitBaseTerrain.onTerrainBroken -= WakeUp;
    //}

    bool sleeping = true;
    public void WakeUp()
    {
        HitBaseTerrain.OnPlayerHitTerrain -= WakeUp;
        if (sleeping != true)
            return;

        modelRef.Wake();
        sleeping = false;
    }
    public static void OnFlyOutComplete()
    {
        instance.initialFlyPending = false;
    }

    //public static void WentOutOfScreen()
    //{
    //    instance.busy = false;
    //    //CM_Deb"I am away");
    //}
    //public static void ReturnedToScreen()
    //{
    //    instance.busy = true;
    //    //CM_Deb"I am back");
    //}

    int lastPressureIndex = -20;
    const int minDepthDiference = 10;
    public static void CreatePressure(WindowTerrain wall)
    {

        
        if (!instance) return;
        if (instance.initialFlyPending || instance.isBusy) return;
        if(wall.rowID>=instance.lastPressureIndex + minDepthDiference)
        {

            instance.StopAllCoroutines();
            instance.anim.SetFloat("flyin_speed", LevelManager.instance.currentLevelProfile.GetBossFlySpeed());
            instance.flying = true;
            instance.lastPressureIndex = wall.rowID;
            //instance.trailGasParticle.Stop();
            instance.AttackRoutine(wall);
            instance.isBusy = true;
        }
    }
    void AttackRoutine(WindowTerrain window)
    {
        this.transform.position = window.transform.position;
        modelPart.localScale = window.model.transform.localScale;
        this.window = window;
        ////CM_Debwall.transform.position);
        modelRef.FlyIn();
    }
    public static void OnFlyInComplete()
    {
        instance.flying = false;
        instance.StartCoroutine(instance.FinalizeAttack());
    }



    private void BreathFire(bool noParticles)
    {

        Rigidbody rb = Pool.Instantiate(noParticles?fireNoParticle:fireWithParticle, firePos.position, Quaternion.identity).GetComponent<Rigidbody>();
        ////CM_Deb"source: " + transform.name + " and fireprefab name: " + firePrefab.name + " and rigidbody name: "+rb.gameObject.name);
        rb.GetComponent<Fire>().Init(this, window.pressurizerDir);
        ////CM_Deb"dir value at use {0}", direction);
        rb.velocity = window.shootDirVec * fireSpeed;

    }

    public static void TimeToShake()//animation event
    {
        instance.flying = false;
        //instance.camAnim.SetTrigger("Shake");
    }
    IEnumerator FinalizeAttack()
    {
        isBusy = false;
        modelRef.Fire();
        yield return new WaitForSeconds(0.2f);
        //trailGasParticle.Play();
        SoundKeeper.PlayATS(SoundKeeper.instance.boss_attack);
        BreathFire(false);
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(1f);
            BreathFire(true);
        }
        //anim.SetTrigger("ascend");

        //yield return new WaitForSeconds(5f);
        //busy = false;
    }
    string IAttacker.attackerid
    {
        get
        {
            return "boss";
        }
    }
    void IAttacker.AddScore(int score)
    {
    }

    float IAttacker.GetAttackHittingTime()
    {
        return -1;
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return 0;
    }
    float IAttacker.GetAttackRange()
    {
        return -1;
    }

    void IAttacker.TriggerAttackEffects(Vector3 point)
    {
    }

    void IAttacker.NullifyThreat(IAttacker nullifier)
    {
        //trailGasParticle.Stop();
    }
    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable) { return 0; }
    float IAttacker.EnemyHitParticleDelay(Dir direction) { return 0; }
}
