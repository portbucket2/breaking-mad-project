﻿using FishSpace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIChaser : AIGuard, ICharacter, IHitable, IDestructible, IAttacker {
    bool isAgitated;
    //bool isPeaceful
    //{
    //    get
    //    {
    //        return Time.time < initTime + peacefulPeriod;
    //    }
    //}
    //float initTime;
    //const float peacefulPeriod = 0.1f;
    public override void OnInit()
    {
        base.OnInit();
        //initTime = Time.time;
        isAgitated = false;
        targetList.Clear();
    }

    private List<ICharacter> targetList = new List<ICharacter>();

#if UNITY_EDITOR
    public List<GameObject> targetVList = new List<GameObject>();
#endif
    private void UpdateVList()
    {
#if UNITY_EDITOR
        targetVList.Clear();
        foreach (var item in targetList)
        {
            targetVList.Add(item.GetGO());
        }
#endif
    }

    public override void PlayTypeSpecificDeathSound()
    {
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Dog, this.transform.position);
    }
    public override void PlayTypeSpecificAttackSound()
    {
        SoundKeeper.PlayEnemySound(EnemyAudioType.dog_attack, this.transform.position);
    }
    public override void OnUpdate()
    {

        if (controller.IsMoving())
        {
            ICharacter overlappingWith = controller.SearchCharactersJustAhead(0.5f);//any character overlapping?
            if (overlappingWith != null)
            {
                MovementController ovCon = overlappingWith.GetMovementController();
                controller.RequestRetreat();
                if (ovCon != null)
                {
                    ovCon.RequestRetreat(true);
                }
            }
        }
        else
        {
            ICharacter currentTarget = GetTarget();
            if (currentTarget != null && isAgitated)
            {
                Dir newDir = currentTarget.GetRelativeDirectionFrom(this);
                controller.Command_SetDirection(newDir);
                ICharacter targetInRange = controller.SearchCharactersJustAhead();
                
                if (targetInRange != null)
                    controller.Command_Attack();
                else if (controller.CanSafeWalk_Forward(allowToJump: true))
                {
                    controller.Command_Move();
                }
            }
            UpdateVList();
        }

    }
    //private void SearchTargets()
    //{
    //    ICharacter character = controller.FirstCharacterInSight(false, 10);
    //    if (character == null) character = controller.FirstCharacterInSight(true, 10);

    //    if (character == null) return;
    //    if (character == (ICharacter)this) Debug.LogError("RC hit self!");
    //    if (!targetList.Contains(character))
    //    {
    //        ////CM_Deb"target added");
            
    //        targetList.Add(character);
    //        if (!isPeaceful) isAgitated = true;
    //        ////CM_Deb"OnEnter T: {0} GameObject: {1}", t, other.gameObject);
    //    }

    //}

    private void OnTriggerEnter(Collider other)
    {
        ////CM_Deb"target triggered");
        //if(this.GetComponentInParent<ICharacter>().logEnabled) 
        ICharacter character =  other.gameObject.GetComponentInParent<ICharacter>();
        if (character == null) return;
        if (!targetList.Contains(character))
        {
            ////CM_Deb"target added");

            targetList.Add(character);
            if(character == PlayerManager.instance as ICharacter)
            //if (!isPeaceful)
                    isAgitated = true;
            ////CM_Deb"OnEnter T: {0} GameObject: {1}", t, other.gameObject);
        }
    }

    public override ICharacter GetTarget()
    {
        int N = targetList.Count;
        for (int i =N-1; i >=0; i--)
        {
            if (targetList[i] == null)
            {
                //if (enableLog) //CM_Deb"{0}", targetList[i]);
                targetList.RemoveAt(i);
            }
            else if (targetList[i].isUntargetable)
            {
                //if (enableLog) //CM_Deb"{0}", targetList[i].isUntargetable);
                targetList.RemoveAt(i);
            }
            else
            {
                return targetList[i];
            }
        }
        return null;
    }
}
