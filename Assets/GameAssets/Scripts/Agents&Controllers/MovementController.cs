﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    #region static or constant
    private const float maxFallSpeed = 20;

    static PredefinedRayCast rc_hitableA;
    static PredefinedRayCast rc_hitableB;

    static PredefinedRayCast rc_locus;
    static PredefinedRayCast rc_walkable;
    static PredefinedRayCast rc_character;


    public static Vector3 Dir2Vec(Dir dir)
    {
        Vector3 dirV = Vector3.up;
        switch (dir)
        {
            case Dir.UP:
                dirV = Vector3.up;
                break;
            case Dir.DOWN:
                dirV = Vector3.down;
                break;
            case Dir.LEFT:
                dirV = Vector3.left;
                break;
            case Dir.RIGHT:
                dirV = Vector3.right;
                break;
        }
        return dirV;
    }
    public static Dir OppositeDir(Dir dir)
    {
        switch (dir)
        {
            case Dir.UP:
                return Dir.DOWN;
            case Dir.DOWN:
                return Dir.UP;
            case Dir.LEFT:
                return Dir.RIGHT;
            case Dir.RIGHT:
                return Dir.LEFT;
            default:
                Debug.LogError("Unexpected Situation");
                return Dir.DOWN;
        }
    }
    #endregion


    public enum MCAutoChargeMode
    {
        NEVER = 0,
        PLAYER = 1,
        ENEMY = 2,
    }
    public enum HitTimingMode
    {
        NORMALIZED =0,
        EVENT_BASED =1,
    }
    enum MoveMode { STRAIGHT, BLOCK, AXIAL, STRAIGHT_ATTACK }

    #region Editor Fields
    [Range(0,1)]
    public float attackHitTimeRatio = 0.45f;

    
    [Range(0, 1)]
    [SerializeField] float attackChargeTimeRatio = 0.1f;
    [SerializeField] MCAutoChargeMode autoChargeMode;
    [SerializeField] HitTimingMode hitTimingMode = HitTimingMode.NORMALIZED;
    [SerializeField] float movementTime = 0.5f;
    [SerializeField] Transform model;
    #endregion
    ICharacter selfCharacter;
    IAttacker selfAttacker;
    IHitable selfHitable;
    bool isPlayer { get { return selfAttacker == PlayerManager.playerAttacker; } }
    Rigidbody rgbd;
    ModelController modelController;

    bool initializedOnce;
    int rowRegister;
    int colRegister;
    Dir faceDir;
    Dir baseDir;
    bool isMoving;
    bool isAttacking;
    bool isFalling;

    public event System.Action<int> rowChanged;
    public int rowIndex { get { return rowRegister; } }
    public int colOffset { get { return colRegister; } }

    void CheckIfFalling_AndSetIfTrue()
    {
        if (rgbd.isKinematic) isFalling = false;
        else
        {
            isFalling = (GetConcreteElement(Dir.DOWN) == null);
        }
        if (!isAttacking)
        {
            if (isFalling)
            {
                modelController.SetModelOrientation(Dir.UP);
            }
            modelController.SetFallingState(isFalling);
        }
    }

    private void Start()
    {
        if (!initializedOnce)
        {
            ICharacter ic = this.GetComponentInParent<ICharacter>();
            Debug.LogErrorFormat("An Agent was predeployed {0}",ic.GetGO().name);
            Init(Dir.UP);
        }
        rc_locus = LevelManager.instance.assets.rc_Locus;
        rc_walkable = LevelManager.instance.assets.rc_Walkable;

        rc_hitableA = LevelManager.instance.assets.rc_HitableLayers_withColliderOrTrigger;
        rc_hitableB = LevelManager.instance.assets.rc_HitableLayers_withColliderWithoutTrigger;
        rc_character = LevelManager.instance.assets.rc_TargetableCharacters;

        //rc_hitableA.DebugRaySettings(Color.magenta,2);
        //rc_locus.DebugRaySettings(Color.blue, 2);
        //rc_walkable.DebugRaySettings(Color.white, 2);
        //rc_character.DebugRaySettings(Color.yellow, 2);
        //if (character.logEnabled)rc_hitableA.EnableLog(true);

        //squisherBottom_enabled = false;
        //squisherLeft_enabled = false;
        //squisherRight_enabled = false;

    }

    private void OneTimeInitJob()
    {
        if (initializedOnce) return;
        selfCharacter = this.GetComponent<ICharacter>();
        selfAttacker = this.GetComponent<IAttacker>();
        selfHitable = this.GetComponent<IHitable>();
        rgbd = this.GetComponent<Rigidbody>();
        modelController = model.GetComponent<ModelController>();
        if (!modelController)
        {
            modelController = model.gameObject.AddComponent<ModelController>();
        }
        initializedOnce = true;
    }
    RigidbodyConstraints startConstraint = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    RigidbodyConstraints fallConstraint = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX;
    public void Init(Dir d)
    {
        OneTimeInitJob();

        isAttacking = false;
        modelController.SetHittingState(isAttacking);
        isMoving = false;
        isFalling = false;
        rowRegister = -1;
        colRegister = 0;


        modelController.OnInit();
        modelController.SetMoveTimeMultiplier(movementTime);
        if (selfAttacker != null)
        {
            modelController.SetHitTimeMultiplier(selfAttacker.GetAttackHittingTime());
        } 
        
        Command_ResetDropEffects(d,null);
    }
    
    public void FixedUpdate()
    {
        if (rgbd != null && !rgbd.isKinematic)
        {
            
            rgbd.AddForce(new Vector3(0, GlobalSettings.instance == null? -20f : GlobalSettings.instance.forceAmountInDownPhysics, 0), ForceMode.Acceleration);
            if (Mathf.Abs( rgbd.velocity.y) > maxFallSpeed)
            {
                rgbd.velocity = new Vector3(rgbd.velocity.x, -maxFallSpeed, rgbd.velocity.z);
            }
            //if (isFalling) Debug.LogFormat("{0} - {1}",character.GetGO().name, rgbd.velocity);
        }
        CheckIfFalling_AndSetIfTrue();
        //model.localRotation = Quaternion.Slerp(model.rotation, modelController.targetRotation, 30*Time.deltaTime);
    }

    #region External Commands/Requests
    public void Command_SetDirection(Dir d, bool editModelOrientation = true)
    {
        if (editModelOrientation)
            modelController.SetModelOrientation(d);
        faceDir = d;
    }

    //float lastTime;
    public bool Command_TakeAction(Dir dir)
    {
        if (!canTakeCommand)
            return false;
        //float gap = Time.realtimeSinceStartup - lastTime;
        //lastTime = Time.realtimeSinceStartup;
        //Debug.LogFormat("Command gap {0}",gap);
        Command_SetDirection(dir);

        if (selfAttacker == null || GetFirstHitableInRange(extendHitBehind: isPlayer)==null)
        {
            if (selfCharacter.logEnabled) Debug.Log("Will Prioritize Move!");
            if (!WillMove(null)) return WillAttack();
            else return true;
        }
        else
        {
            if (selfCharacter.logEnabled) Debug.Log("Will Prioritize Attack!");
            if (!WillAttack()) return WillMove(null);
            else return true;
        }
        
    }    
    public bool Command_Forward(System.Action callback)
    {
        if (!canTakeCommand)
            return false;
        return WillMove(callback);
    }
    public bool Command_Attack()
    {
        if (!canTakeCommand)
            return false;
        return WillAttack();
    }
    public void Command_Move()
    {
        if (!canTakeCommand)
            return;
        WillMove(null);
    }


    public void Command_DropToAbyss(Dir dir, Vector3 force, Vector3 torque)
    {
        //rgbd.constraints = fallConstraint;
        rgbd.isKinematic = false;
        rgbd.constraints = RigidbodyConstraints.None;
        StopAllCoroutines();
        isMoving = false;
        isAttacking = false;
        modelController.SetHittingState(isAttacking);
        this.transform.position = this.transform.position + new Vector3(0, 0, -5);
        rgbd.AddForce(force, ForceMode.Impulse);
        rgbd.AddTorque(torque, ForceMode.Impulse);
    }
    public void Command_ResetDropEffects(Dir resetFaceDir, IGridLocus preDeathLocus)
    {
        rgbd.useGravity = false;
        rgbd.constraints = startConstraint;
        if (selfCharacter.canCrawlWalls || selfCharacter.canFly) rgbd.isKinematic = true;

        baseDir = Dir.DOWN;
        Command_SetDirection(resetFaceDir);

        if (preDeathLocus != null)
        {
            this.transform.position = preDeathLocus.position;
            this.transform.rotation = Quaternion.identity;
        }
    }
    public void RequestRetreat(bool secondary = false)
    {
        if (isAttacking) return;
        if (!selfCharacter.canRetreat) return;
        if (!secondary)
        {
            retreatAfterFrames = Random.Range(0, 3);
        }
        else
        {
            if (!selfCharacter.GetGO().CompareTag("Player"))
            {
                retreatAfterFrames = Random.Range(0,3);
            }
        }
    }
    #endregion

    #region helping methods/properties
    Dir tailDir { get { return OppositeDir(faceDir); } }
    Vector3 faceVec
    {
        get
        {
            return Dir2Vec(faceDir);
        }
    }
    float range { get { return selfAttacker.GetAttackRange()* LevelManager.gridUnitDimension; } }
    Quaternion GetRootRotationForBaseDir(Dir dir)
    {
        switch (dir)
        {
            case Dir.UP:
                return Quaternion.Euler(0, 0, 180);
            case Dir.DOWN:
                return Quaternion.Euler(0, 0, 0);
            case Dir.LEFT:
                return Quaternion.Euler(0, 0, 270);
            case Dir.RIGHT:
                return Quaternion.Euler(0, 0, 90);
            default:
                return Quaternion.identity;
        }
    }
    Vector3 raySource { get { return this.transform.position + Dir2Vec(OppositeDir(baseDir)) * LevelManager.gridUnitDimension * 0.5f; } }
    void DrawRay(Vector3 vec, Color col, float time = 2)
    {
#if UNITY_EDITOR
        Debug.DrawRay(raySource, vec * LevelManager.gridUnitDimension, col, time, false);
#endif
    }
    #endregion
    #region Queries
    bool canMove
    {
        get
        {
            if (isMoving) return false;
            if (isFalling) return false;
            if (isAttacking) return false;
            return true;
        }
    }

    bool canAttack
    {
        get
        {
            //if (selfAttacker != PlayerManager.playerAttacker && GlobalSettings.instance.disableEnemyAttack) return false; 
            if (isMoving) return false;
            if (isAttacking) return false;
            if ((!isPlayer) && isFalling) return false;

            return true;
        }
    }

    bool canTakeCommand { get { return !isMoving && !isAttacking; } }
    bool IsFacePerpendicularToBase()
    {
        switch (baseDir)
        {
            case Dir.UP:
            case Dir.DOWN:
                switch (faceDir)
                {
                    case Dir.LEFT:
                    case Dir.RIGHT:
                        return true;
                }
                break;
            case Dir.LEFT:
            case Dir.RIGHT:
                switch (faceDir)
                {
                    case Dir.UP:
                    case Dir.DOWN:
                        return true;
                }
                break;
        }
        return false;
    }
    public bool IsMoving()
    {
        return isMoving;
    }
    public bool IsAttacking()
    {
        return isAttacking;
    }
    public bool IsAttacking(Dir dir)
    {
        return isAttacking && faceDir==dir;
    }
    public bool IsFalling()
    {
        return isFalling;
    }
    public Dir GetFaceDir() { return faceDir; }
    public Dir GetTailDir() { return tailDir; }
    public bool CanSlideForward(Dir dir)
    {
        switch (dir)
        {
            case Dir.UP:
            case Dir.DOWN:
                return false;
        }

        if (!canMove) return false;
        IGridLocus cg = GetGridLocus(baseDir);
        if (cg.IsBlank()) return false;
        //IGridLocus fg = GetGridLocus(faceDir);
        if (GetConcreteElement(dir) != null) return false;
        IGridLocus fg = cg.relativeLocus(dir).relativeLocus(OppositeDir(baseDir));
        if (!(fg==null ||fg.IsBlank())) return false;
        return true;
    }
    public bool CanSafeWalk_Forward(bool allowToJump)
    {
        if (!canMove) return false;
        IGridLocus cg = GetGridLocus(baseDir);
        if (cg.IsBlank()) return false;
        IGridLocus ng = cg.relativeLocus(faceDir);
        if (!allowToJump && ng.IsBlank()) return false;
        if (GetConcreteElement(faceDir) != null) return false;
        ICharacter fallingChar = SearchCharacterUntilObstruction(raySource + Dir2Vec(faceDir), Dir.UP, 1.35f, true);
        if (fallingChar != null)
        {
            MovementController mc = fallingChar.GetMovementController();
            if (mc && mc.isFalling) return false;
        }
        return true;
    }
    public bool ShouldTurnAround()//sequence sensitive
    {
        if (!canMove) return false;
        IGridLocus cg = GetGridLocus(baseDir);
        IGridLocus ng = cg.relativeLocus(tailDir);
        IGridLocus tg = ng.relativeLocus(OppositeDir(baseDir));
        if (cg.IsBlank()) return false;
        if (!tg.IsBlank()) return false;

        if (SearchCharacterUntilObstruction(raySource, tailDir, 1.0f,false) != null)
            return true;

        if (ng.IsBlank()) return false;
        return true;
    }

    public bool CanSafeFly_Forward()
    {
        if (!canMove) return false;
        IGridLocus rootGL = GetGridLocus(baseDir);
        IGridLocus rootForwardGL = rootGL.relativeLocus(faceDir);
        IGridLocus floorForwardGL = rootForwardGL.relativeLocus(Dir.UP);
        IGridLocus skyForwardGL = floorForwardGL.relativeLocus(Dir.UP);
        if (skyForwardGL.IsBlank() && floorForwardGL.IsBlank())
            return true;
        else return false;
    }
    public bool CanSafeFly_BackWard()
    {
        if (!canMove) return false;
        IGridLocus rootGL = GetGridLocus(baseDir);
        IGridLocus rootForwardGL = rootGL.relativeLocus(tailDir);
        IGridLocus floorForwardGL = rootForwardGL.relativeLocus(Dir.UP);
        IGridLocus skyForwardGL = floorForwardGL.relativeLocus(Dir.UP);
        if (skyForwardGL.IsBlank() && floorForwardGL.IsBlank())
            return true;
        else return false;
    }

    private const float hitBehindExtendLength = 0.495f;
    Vector3 GetExtendHitVec(Dir dir)
    {
        return Dir2Vec(OppositeDir(dir)) * hitBehindExtendLength;
    }
    List<IHitable> _templist = new List<IHitable>(capacity:10);
    List<IHitable> _templist2 = new List<IHitable>(capacity: 10);
    private IHitable GetFirstHitableInRange(Dir dir, bool extendHitBehind)//bool useSphere = false)
    {
            float effectiveRange = range;
            Vector3 casterPos = raySource;
            if (extendHitBehind)
            {
                casterPos += GetExtendHitVec(dir);
                effectiveRange += hitBehindExtendLength;
            }

            float hitDistance = 100000;
            IHitable hitable = null;
            bool didhit = false;
            //if (!useSphere)
            //{
            didhit = rc_hitableA.DoubleCastAll(rc_hitableB, casterPos, Dir2Vec(dir), effectiveRange);
            //}
            //else
            //{
            //    didhit = rc_hitableA.SphereDoubleCastAll(rc_hitableB, raySource, faceVec, range);
            //}

            if (didhit)
            {
                hitDistance = Vector3.Distance(casterPos, rc_hitableA.point);
                hitable = rc_hitableA.collider.gameObject.GetComponentInParent<IHitable>();
            }
            if (hitable != null && hitDistance < hitable.MaxHitDistance())
            {
                if (selfCharacter.logEnabled) Debug.LogFormat("hitguy {0} mhd {1}", hitable.GetGO(), hitable.MaxHitDistance());
                return hitable;
            }
            return null;
    }
    private IHitable GetFirstHitableInRange(bool extendHitBehind)//bool useSphere = false)
    {
        if (!isPlayer)
            return GetFirstHitableInRange(faceDir, extendHitBehind);
        else
        {       
            List<IHitable> hitables = GetAllHitableInRange(raySource,extendHitBehind);

            if (hitables.Count > 0)
            {
                return hitables[0];
            }
            else return null;
        }
    }
    private List<IHitable> GetAllHitableInRange(Vector3 originalCasterpos, bool extendHitBehind)//bool useSphere = false)
    {
        Vector3 casterPos = originalCasterpos;
        float effectiveRange = range;
        if (extendHitBehind)
        {
            casterPos += GetExtendHitVec(faceDir);
            effectiveRange += hitBehindExtendLength;
        }


        if (isPlayer && !isFalling && !selfCharacter.isDead  && (faceDir == Dir.LEFT || faceDir == Dir.RIGHT) )
        {
            //Debug.Log("style N");
            IGridLocus cloc = GetCurrentLocus();
            Vector3 longRaySource = cloc.row.position;
            switch (faceDir)
            {
                case Dir.LEFT:
                    longRaySource += new Vector3(5, 0, 0);
                    break;
                case Dir.RIGHT:
                    longRaySource -= new Vector3(5, 0, 0);
                    break;
            }


            bool didhit = false;

            rc_hitableA.DebugRaySettings(hitColor: Color.green, missColor: Color.red, time: 2);
            didhit = rc_hitableA.SphereDoubleCastAll(rc_hitableB, longRaySource, faceVec, 0.2f, 10, true);

            _templist.Clear();
            if (didhit)
            {
                foreach (var rch in rc_hitableA.lastHitArray)
                {
                    IHitable hitable = rch.collider.GetComponentInParent<IHitable>();
                    if (hitable != null && hitable != selfHitable)
                    {
                        //float xD_Transform = hitable.xPosition - casterPos.x;
                        float xD_Hitpoint = rch.point.x - casterPos.x;
                        float xD = xD_Hitpoint;

                        if (Mathf.Abs(Mathf.Abs(xD_Hitpoint) - hitBehindExtendLength) > hitable.MaxHitDistance()) continue;

                        if (faceDir == Dir.LEFT && xD <= 0 && xD > -effectiveRange)
                        {
                            _templist.Add(hitable);
                        }
                        else if (faceDir == Dir.RIGHT && xD >= 0 && xD < effectiveRange)
                        {
                            _templist.Add(hitable);
                        }
                    }
                }
            }
            _templist2.Clear();

            for (int i = 0; i < _templist.Count; i++)
            {
                _templist2.Add(_templist[i]);

                HitBaseTerrain hbt = _templist[i] as HitBaseTerrain;
                if (hbt && hbt.isUnbreakable) break;

            }

            return _templist2;
        }
        else
        {
            //Debug.Log("style O");
            bool didhit = false;

            rc_hitableA.DebugRaySettings(hitColor: Color.green, missColor: Color.red, time: 2);
            didhit = rc_hitableA.DoubleCastAll(rc_hitableB, casterPos, faceVec, effectiveRange, true);

            _templist.Clear();
            if (didhit)
            {
                foreach (var item in rc_hitableA.lastHitArray)
                {
                    IHitable hitable = item.collider.GetComponentInParent<IHitable>();
                    if (hitable != null && hitable != selfHitable) {
                        _templist.Add(hitable);
                        HitBaseTerrain hbt = hitable as HitBaseTerrain;
                        if (hbt && hbt.isUnbreakable) break;
                    }
                }
            }
            return _templist;

        }
        
        
    }
    //private List<IHitable> GetAllHitableInRange(Vector3 customRaySource)//bool useSphere = false)
    //{

    //}

    private Collider GetConcreteElement(Dir dir)
    {
        Vector3 vecdir = Dir2Vec(dir);
        if (!selfCharacter.canCrawlWalls)
        {
            if (rc_walkable.Cast(raySource, vecdir, LevelManager.gridUnitDimension))
            {
                return rc_walkable.collider;
            }
        }
        else
        {
            if (rc_walkable.DoubleCastAll(rc_character ,raySource, vecdir, LevelManager.gridUnitDimension))
            {
                return rc_walkable.collider;
            }
        }
        //if (rc_walkable.Cast(raySource, vecdir, LevelManager.gridUnitDimension))
        //{
        //    return rc_walkable.collider;
        //}
        //if (seflCharacter.logEnabled) Debug.LogFormat("walkable : not found");
        return null;
    }
    private IGridLocus GetGridLocus(Dir direction)
    {
        Vector3 vecdir = Dir2Vec(direction);
        if (rc_locus.Cast(raySource, vecdir, LevelManager.gridUnitDimension))
        {
            //if (seflCharacter.logEnabled) Debug.Log(rc_locus.collider.gameObject);
            return rc_locus.collider.gameObject.GetComponentInParent<IGridLocus>();
        }
        return null;
    }
    public IGridLocus GetCurrentLocus()
    {
        return GetGridLocus(baseDir).relativeLocus(OppositeDir(baseDir));
    }

    

    private ICharacter SearchCharacters(Vector3 source, Dir dir, float units)
    {

        Vector3 vecdir = Dir2Vec(dir);

        if (rc_character.Cast(source, vecdir, units * LevelManager.gridUnitDimension))
        {

            return rc_character.collider.gameObject.GetComponentInParent<ICharacter>();
        }


        return null;
    }
    private ICharacter SearchCharacterUntilObstruction(Vector3 source, Dir dir, float sightRange, bool debugRay)
    {
        Vector3 sightVec = Dir2Vec(dir);
        if (rc_hitableA.DoubleCastAll(rc_hitableB, source, sightVec, sightRange * LevelManager.gridUnitDimension))
        {
            ICharacter found = rc_hitableA.collider.gameObject.GetComponentInParent<ICharacter>();
            if (debugRay)
            {
                if (found == null)
                    Debug.DrawRay(source, sightVec * rc_hitableA.lastHit.distance, Color.magenta / 2, 2);
                else
                    Debug.DrawRay(source, sightVec * rc_hitableA.lastHit.distance, Color.magenta, 2);
            }
            return found;
        }

        return null;
    } 
    public ICharacter SearchCharactersJustAhead(float aheadRange =1)
    {
        return SearchCharacters(raySource, faceDir,aheadRange);
    }

    public ICharacter GetOverLapingCharacter()
    {
        if (rc_character == null) return null;

        if (rc_character.CastAll(GetGridLocus(Dir.DOWN).StandingPosition(Dir.DOWN), Vector3.up, LevelManager.gridUnitDimension))
        {
            foreach (var item in rc_character.lastHitArray)
            {
                ICharacter ic = item.collider.gameObject.GetComponentInParent<ICharacter>();

                if (ic != selfCharacter && ic !=null)
                    return ic ;
            }
        }

        return null;
    }
    public ICharacter FirstCharacterInSight(bool reverseDirection, float sightRange)
    {
        Dir d = faceDir;
        if (reverseDirection) d = tailDir;
        return SearchCharacterUntilObstruction(raySource,d,sightRange,false);
    }
    public IHitable SearchHitablesJustAboveInRange()
    {
        return GetFirstHitableInRange(OppositeDir(baseDir),isPlayer);
    }
    #endregion

    bool WillAttack()
    {
        if (!canAttack) return false;
        //if (seflCharacter.logEnabled) Debug.Log("Chose to attack!");
        StartCoroutine(AttackRoutine());
        return true;

    }

    bool WillMove(System.Action callback)
    {
        if (!canMove) return false;

        if (!IsFacePerpendicularToBase()) return false;
        //if (seflCharacter.logEnabled) Debug.Log("Chose to move!");

        //if (character.canCrawlWalls) rgbd.isKinematic = true;
        //Debug.LogFormat("Move {0} Command accepted", dir);
        Collider solidGround = GetConcreteElement(baseDir);
        //if (seflCharacter.logEnabled) Debug.Log(solidGround);
        IGridLocus baseLoc = GetGridLocus(baseDir);
        if (solidGround != null || selfCharacter.canFly)
        {
            IGridLocus nextLoc = baseLoc.relativeLocus(faceDir);
            IGridLocus faceLoc = GetGridLocus(faceDir);// nextLoc.relativeLocus(OppositeDir(baseDir));
            if (faceLoc!=null && !faceLoc.IsBlank())
            {
                if (selfCharacter.canCrawlWalls)
                {
                    StartCoroutine(MoveRoutine(baseLoc.StandingPosition(baseDir), faceLoc.StandingPosition(faceDir), MoveMode.BLOCK, callback));
                    return true;
                }
                else
                {
                    if(selfCharacter.logEnabled)Debug.LogError("Unexpected situation.");
                    return false;
                }
            }
            else
            {
                MoveMode mm = MoveMode.STRAIGHT;

                if ((nextLoc==null || nextLoc.IsBlank()) && selfCharacter.canCrawlWalls)
                {
                    ICharacter standableEnemy = SearchCharacters(raySource + Dir2Vec(faceDir),baseDir,LevelManager.gridUnitDimension);
                    if(standableEnemy ==null) mm = MoveMode.AXIAL;
                }

                if (mm == MoveMode.STRAIGHT)
                {
                    if (nextLoc == null) Debug.LogFormat("loc is null for - {0}",selfCharacter.GetGO().name);
                    StartCoroutine(MoveRoutine(baseLoc.StandingPosition(baseDir), nextLoc.StandingPosition(baseDir), mm, callback));
                    return true;
                }
                else if (mm == MoveMode.AXIAL)
                {
                    StartCoroutine(MoveRoutine(baseLoc.StandingPosition(baseDir), baseLoc.StandingPosition(OppositeDir(faceDir)), mm, callback));
                    return true;
                }
            }
        }
        if (selfCharacter.logEnabled) Debug.LogFormat("something wrong with base {0}!", baseLoc);
        if (baseLoc != null) if (selfCharacter.logEnabled) Debug.LogFormat("something wrong with base is blank {0}!", baseLoc.IsBlank());

        return false;
    }

    //PlayerManager playerManIfAny;
    bool didPerformAttack;
    IEnumerator AttackRoutine()
    {

        isAttacking = true;
        didPerformAttack = false;
        yield return new WaitForSeconds(selfAttacker.GetAttackPreparationTime());
        NLog.A_Log("Attack");

        selfAttacker.TriggerAttackEffects(raySource + faceVec);
        modelController.SetHittingState(isAttacking);
        //AutoChargeSetting===========================
        bool autoCharge;
        IGridLocus baseGL = GetGridLocus(baseDir);
        IGridLocus nextGL = baseGL.relativeLocus(faceDir);
        IGridLocus frontGL = nextGL.relativeLocus(OppositeDir(baseDir));
        if (isFalling) autoCharge = false;
        else
        {
            switch (autoChargeMode)
            {
                default:
                case MCAutoChargeMode.NEVER:
                    autoCharge = false;
                    break;
                case MCAutoChargeMode.ENEMY:
                    autoCharge = true;
                    break;
                case MCAutoChargeMode.PLAYER:
                    autoCharge = true;
                    if (nextGL.IsBlank() || nextGL.HasSpike())
                    {
                        autoCharge = false;
                        break;
                    }
                    else
                    {
                        ICharacter c = SearchCharacters(frontGL.position, OppositeDir(baseDir), 1);
                        if (c != null)
                        {
                            autoCharge = false;
                            break;
                        }
                        if (!frontGL.IsBlank() && frontGL.IsUnbreakable())
                        {
                            autoCharge = false;
                            break;
                        }

                        break;
                    }
            }
        }
        float startTime = Time.time;
        bool didPerformCharge = false;
        float rsX = raySource.x;

        System.Action cb = () => {
            //Debug.LogWarning("through event!");
            Hit_ifHitIsStillPending(rsX);
        }; 
        CheckIfFalling_AndSetIfTrue();

        if (isPlayer)
        {
            modelController.TriggerAttackForPlayer(faceDir, frontGL.IsUnbreakable(), cb);
        }
        else
        {
            modelController.TriggerAttack(faceDir, cb);
        }




        while (Time.time<=startTime+selfAttacker.GetAttackHittingTime())
        {
            if ((!didPerformCharge) && (Time.time >= (startTime + (selfAttacker.GetAttackHittingTime() * attackChargeTimeRatio))) && autoCharge)
            {
                //Debug.Log(nextGL.GetGameObject());
                //Debug.Log(autoCharge);

                StartCoroutine(MoveRoutine(baseGL.StandingPosition(baseDir), nextGL.StandingPosition(baseDir), MoveMode.STRAIGHT_ATTACK, null));
                didPerformCharge = true;

                //if (seflCharacter as AIChaser) Debug.LogError("dog charging");
                if (autoChargeMode == MCAutoChargeMode.PLAYER)Hit_ifHitIsStillPending(rsX);
            }


            if (!isPlayer)
            {
                if ((Time.time >= (startTime + (selfAttacker.GetAttackHittingTime() * attackHitTimeRatio))))
                {
                    //if (seflCharacter as AIChaser) Debug.LogError("dog damaging");
                    Hit_ifHitIsStillPending(rsX);
                }
            }
            else
            {
                if (isFalling && (Time.time >= (startTime + (selfAttacker.GetAttackHittingTime() * attackHitTimeRatio))))
                {
                    Hit_ifHitIsStillPending(rsX);
                }
            }


            yield return null;
        }
        Hit_ifHitIsStillPending(rsX);
        isAttacking = false;
        modelController.SetHittingState(isAttacking);
    }


    void Hit_ifHitIsStillPending(float rsX)
    {
        if (didPerformAttack) return;
        didPerformAttack = true;
        if (isRetreating) Debug.LogWarning("did damage while retreating");

        IGridLocus faceLoc = GetGridLocus(faceDir);
        //if (faceLoc!=null && faceLoc.IsUnbreakable())
        //{
        //    IHitable hitable = GetFirstHitableInRange(isPlayer);
        //    if (hitable != null)
        //    {
        //        hitable.TakeHit(faceDir, selfAttacker);
        //        selfAttacker.AddScore(hitable.GetScoreOnHit());
        //    }
        //}
        //else
        //{
        List<IHitable> hitables = GetAllHitableInRange(new Vector3(rsX, raySource.y, raySource.z), isPlayer);
        IGridLocus botLoc = GetGridLocus(Dir.DOWN);
        if (botLoc == null) return;
        IGridLocus secondBottomLocus = botLoc.relativeLocus(Dir.DOWN);
        IHitable secBotLocHitable = secondBottomLocus as IHitable;
        //Debug.Log(secBotLocHitable);
        if (isFalling && faceDir == Dir.DOWN && secBotLocHitable != null && !hitables.Contains(secBotLocHitable))
        {
            hitables.Add(secBotLocHitable);
        }
        //if (faceDir == Dir.UP)
        //{
        //    IGridLocus topLoc = GetGridLocus(Dir.UP);
        //    ICharacter fallingChar = SearchCharacters(topLoc.StandingPosition(Dir.DOWN), Dir.UP, 1);
        //    if (fallingChar != null)
        //    {
        //        if (fallingChar.GetMovementController().isFalling)
        //        {
        //            IHitable topFallingGuy= fallingChar.GetGO().GetComponent<IHitable>();
        //            if (topFallingGuy != null && !hitables.Contains(topFallingGuy))
        //            {
        //                Debug.LogErrorFormat("Falling guy will be killed {0}",fallingChar.GetGO().name);
        //                //Debug.Break();
        //                hitables.Add(topFallingGuy);
        //            }
        //        }
        //    }
        //}
        
        foreach (var hitable in hitables)
        {
            if (hitable == selfHitable) continue;
            bool doDamage = true;
            if (faceDir == Dir.DOWN)
            {
                IGridLocus igl = hitable.GetGO().GetComponent<IGridLocus>();
                if (!igl.HasSpike() && (igl == secondBottomLocus) && !isFalling)
                {
                    doDamage = false;
                }
            }
            PlayerManager pm = hitable as PlayerManager;
            if (pm != null && (pm as ICharacter).GetMovementController().isFalling)
            {
                doDamage = false;
            }

            if (doDamage)
            {
                hitable.TakeHit(faceDir, selfAttacker);
                selfAttacker.AddScore(hitable.GetScoreOnHit());
            }

        }
        //}

    }


    int retreatAfterFrames = -1;
    bool isRetreating { get { return _isOnRetreat; } }
    bool _isOnRetreat = false;
    IEnumerator MoveRoutine(Vector3 startPos, Vector3 targetpos, MoveMode mm, System.Action callback)
    {
        _isOnRetreat = false;
        //Debug.LogWarning("Move routine start at time: " + Time.time);
        isMoving = true;
        if (selfCharacter.togglingCollider_OnMove) selfCharacter.togglingCollider_OnMove.isTrigger = true;
        retreatAfterFrames  = -1;
        modelController.SetMovingState(true);
        
        
        //int N = Mathf.RoundToInt(movementTime / Time.deltaTime);



        Dir newFaceDir = faceDir;
        Dir newBaseDir = baseDir;
        switch (mm)
        {
            case MoveMode.STRAIGHT:
            case MoveMode.STRAIGHT_ATTACK:
                break;
            case MoveMode.BLOCK:
                newFaceDir = OppositeDir(baseDir);
                newBaseDir = faceDir;
                break;
            case MoveMode.AXIAL:
                newBaseDir = OppositeDir(faceDir);
                newFaceDir = baseDir;
                break;
        }
        Quaternion startRotation = GetRootRotationForBaseDir(baseDir);
        Quaternion targetRotation = GetRootRotationForBaseDir(newBaseDir);
        
        float iniDist = Vector3.Distance(startPos, targetpos);
        float totalEstimatedTime = iniDist * movementTime;
        float startTime = Time.time;
        while (true)
        {
            float elapsedPositive = Time.time - startTime;
            if (retreatAfterFrames==0 || elapsedPositive > totalEstimatedTime) break;
            retreatAfterFrames--;
            transform.position = Vector3.Lerp(startPos, targetpos, elapsedPositive / totalEstimatedTime);
            transform.rotation = Quaternion.Slerp(startRotation, targetRotation, elapsedPositive / totalEstimatedTime);
            yield return null;

        }

        if (retreatAfterFrames == 0)
        {
            _isOnRetreat = true;
            //Debug.LogFormat("Retreating with movemode {0}", mm);
            if (mm != MoveMode.STRAIGHT_ATTACK) Command_SetDirection(tailDir, true);
            float retreatTimePerUnitDistance = movementTime; // 1/retreatSpead
            float reverseProgress = ((Time.time - startTime) / movementTime);
            float estimatedRetreatTime =  reverseProgress*LevelManager.gridUnitDimension* retreatTimePerUnitDistance;
            float retreatStartTime = Time.time;
            while (true)
            {
                float elapsedNegative = Time.time - retreatStartTime;
                if (elapsedNegative > estimatedRetreatTime) break;

                float step = ((estimatedRetreatTime - elapsedNegative) / (totalEstimatedTime * retreatTimePerUnitDistance));
                //if (seflCharacter.logEnabled) Debug.LogFormat("ert {0} - en {1} / tet {2} = step {3}",estimatedRetreatTime,elapsedNegative,totalEstimatedTime,step);
                transform.position = Vector3.Lerp(startPos, targetpos, step);
                transform.rotation = Quaternion.Slerp(startRotation, targetRotation, step);
                yield return null;

            }
            this.transform.position = startPos;
            this.transform.rotation = startRotation;
            if (mm != MoveMode.STRAIGHT_ATTACK) Command_SetDirection(tailDir, true);
        }
        else
        {
            this.transform.position = targetpos;
            this.transform.rotation = targetRotation;
            faceDir = newFaceDir;
            baseDir = newBaseDir;
        }

        _isOnRetreat = false;


        if (selfCharacter.togglingCollider_OnMove) selfCharacter.togglingCollider_OnMove.isTrigger = false;
        isMoving = false;
        if (callback != null) callback();
        CheckIfFalling_AndSetIfTrue();
        colRegister = GetCurrentLocus().rowOffset;
        yield return null;
        modelController.SetMovingState(isMoving);
        //Debug.LogWarning("Move routine end at time: " + Time.time);
    }

    #region Collision & Triggers
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Sand"))
        {
            collision.collider.GetComponent<SandTerrain>().StartShatter();
        }
        IGridLocus igl = collision.collider.GetComponentInParent<IGridLocus>();
        if (igl != null)
        {
            if (igl.row.rowID != rowRegister)
            {
                rowRegister = igl.row.rowID;
                if (rowChanged != null) rowChanged(rowRegister);
                if (isPlayer)
                {
                    PlayerManager pm = selfCharacter as PlayerManager;
                    pm.featureManager.PlayLandingEffects();
                    if (pm.IsLandingingUpgradeEnabled())
                    {
                        SimpleHitCharOnDirection(Dir.LEFT);
                        SimpleHitCharOnDirection(Dir.RIGHT);

                    }
                }


            }
        }

        //ICharacter ich = collision.collider.GetComponentInParent<ICharacter>();
        //if (ich != null && isMoving)
        //{
        //    RequestRetreat();
        //}
    }
    void SimpleHitCharOnDirection(Dir dir)
    {
        //DrawRay(Dir2Vec(dir), Color.magenta);
        ICharacter ic = SearchCharacters(raySource, dir,1);
        if (ic == null) return;
        //Debug.LogFormat("found on side {0}",ic.GetGO());
        IHitable ih = ic.GetGO().GetComponentInParent<IHitable>();
        ih.TakeHit(dir,selfAttacker);
        selfAttacker.AddScore(ih.GetScoreOnHit());
    }

    private void OnTriggerEnter(Collider other)
    {
        IGridLocus ig = other.GetComponentInParent<IGridLocus>();
        if (ig != null)
        {
            if (ig.IsNextLevelTransitionGrid())
            {
                if (gameObject.CompareTag("Player"))
                    ig.CheckAndLoadLevelIfNeeded();
                else
                {
                    selfHitable.TakeHit(Dir.UP, selfAttacker);
                }

            }
            
        }
    }

    #endregion
}

