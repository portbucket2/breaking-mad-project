﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyAdMan : MonoBehaviour
{
    public static void ShowRewardAd(System.Action<bool> callback)
    {
        FishSpace.Centralizer.Add_DelayedAct(()=> {
            bool success = (Random.Range(0, 4)!=0);
            if (callback!=null) callback(success);



        }, 2,true);

    }
}
