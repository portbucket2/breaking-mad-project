﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Portbliss.NativePlugins;
using UnityEngine.Profiling;

public class LoadingSceneScript : MonoBehaviour {

    //// Use this for initialization
    //IEnumerator Start () {
    //       Application.targetFrameRate = 60;
    //      yield return new WaitForSeconds(1);
    //      yield return SceneManager.LoadSceneAsync(1);
    //}
    public GameObject companyMod;
    public GameObject bmadMod;
    public string introSceneName;
    public string gameSceneName;
    [Header("company")]
    public SpriteRenderer companyBG;
    public SpriteRenderer companyText;
    [Header("default")]
    public BMadLockPhysicsManager defaultRGBD;
    public SpriteRenderer defaultBG;
    public GameObject butterfly01;
    public GameObject butterfly02;
    public GameObject butterfly03;
    //public ParticleSystem explode01;
    //public ParticleSystem explode02;
    //public ParticleSystem explode03;


    public static LoadingSceneScript instance;
    private static FishSpace.HardData<bool> introWasDisplayedOnce;
    private static bool shouldDisplayIntro
    {
        get
        {
            return !introWasDisplayedOnce.value;
        }
    }
    public static void OnIntroOver()
    {
        introWasDisplayedOnce.value = true;
        instance.StartCoroutine(instance.NewLoad(true));
    }

    bool firstLoad = true;
    float initialOrthoSize;


    [SerializeField] GameObject consentPanel;
    bool gaveConsent = false;

    [SerializeField] Button giveConsent, termsOfServices;
    [SerializeField] [Multiline] string termsOfServicesLink;
    void Awake()
    {
        giveConsent.onClick.AddListener(()=>{
            gaveConsent = true;
        });
        termsOfServices.onClick.AddListener(()=> {
            Application.OpenURL(termsOfServicesLink);
        });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    IEnumerator Start()
    {
        consentPanel.SetActive(false);
        if (instance)
        {
            Destroy(this.gameObject);
            instance.StartCoroutine( instance.NewLoad() );
        }
        else
        {
            //first time
            ////CM_Deb"first time check");

            
            initialOrthoSize = Camera.main.orthographicSize;
            instance = this;
            DontDestroyOnLoad(gameObject);
            introWasDisplayedOnce = new FishSpace.HardData<bool>("INTROTESTKEY_005", false);
            Application.backgroundLoadingPriority = ThreadPriority.High;
            Application.targetFrameRate = 60;
            yield return StartCoroutine(ConsentTask());
            //yield return new WaitForSeconds(0.8f);
            yield return StartCoroutine(  NewLoad());
        }
    }

    IEnumerator ConsentTask()
    {
        string flagConsent = PlayerPrefs.GetString("GaveConsent", "");
        if (flagConsent != "yap")
        {
            ////CM_Deb"consent was not set! we will show consent window!");
            consentPanel.SetActive(true);
            while (true)
            {
                ////CM_Deb"lets see whether user gives any consent.....");
                if (gaveConsent == true)
                {
                    PlayerPrefs.SetString("GaveConsent", "yap");
                    consentPanel.SetActive(false);
                    PortblissPlugins.ad.SetUserConsentDueToRecentEuGdprPolicy();
                    break;
                }
                yield return null;
            }
        }
    }

    IEnumerator NewLoad(bool fromIntroScene = false)
    {
        if (shouldDisplayIntro)
        {
            EnableCompanyMod();
            DisableBMadMod();
            yield return new WaitForSeconds(1.5f);
            yield return StartCoroutine(CompanyTextFadeOut(1f));
            yield return SceneManager.LoadSceneAsync(introSceneName);
            StartCoroutine(CompanyBGFadeOut(1));
        }
        else
        {
            if (firstLoad)
            {
                EnableCompanyMod();
                yield return new WaitForSeconds(1f);
                //yield return StartCoroutine(CompanyTextFadeOut(1f));
                //StartCoroutine(CompanyBGFadeOut(1));
                EnableBMadMod();
                DisableCompanyMod();
            }
            else
            {
                DisableCompanyMod();
                EnableBMadMod();
            }

            yield return new WaitForSeconds(1);
            if(fromIntroScene)
                yield return new WaitForSeconds(1.75f);

            yield return SceneManager.LoadSceneAsync(gameSceneName);
            //yield return StartCoroutine(ToGameRoutine(0.75f));
            DisableBMadMod();

        }
        firstLoad = false;
    }


    IEnumerator ToGameRoutine(float totalTime)
    {
        butterfly01.SetActive(false);
        //explode01.Play();
        yield return new WaitForSeconds(0.2f);
        butterfly02.SetActive(false);
        //explode02.Play();
        yield return new WaitForSeconds(0.3f);
        butterfly03.SetActive(false);
        //explode03.Play();

        //deal with gravity
        defaultRGBD.Drop();
        yield return new WaitForSeconds(0.75f);

        float startTime = Time.time;
        while (Time.time < startTime + totalTime)
        {
            float step = Mathf.Clamp01((Time.time - startTime) / totalTime);
            defaultBG.color = Color.Lerp(Color.white, new Color(1, 1, 1, 0), step);
            yield return null;
        }

    }


    private void EnableCompanyMod()
    {
        companyBG.color = Color.white;
        companyText.color = Color.white;
        companyMod.SetActive(true);
    }
    private void EnableBMadMod()
    {
        defaultRGBD.Reset();
        butterfly01.SetActive(true);
        butterfly02.SetActive(true);
        butterfly03.SetActive(true);
        defaultBG.color = Color.white;
        bmadMod.SetActive(true);
    }
    private void DisableCompanyMod()
    {
        companyMod.SetActive(false);
    }
    private void DisableBMadMod()
    {
        bmadMod.SetActive(false);
    }

    IEnumerator CompanyTextFadeOut(float totalTime)
    {

        float startTime = Time.time;
        while (Time.time < startTime + totalTime)
        {
            float step = Mathf.Clamp01((Time.time-startTime)/totalTime);
            companyText.color = Color.Lerp(Color.white, new Color(1, 1, 1, 0), step);
            yield return null;
        }
    }
    IEnumerator CompanyBGFadeOut(float totalTime)
    {

        float startTime = Time.time;
        while (Time.time < startTime + totalTime)
        {
            float step = Mathf.Clamp01((Time.time - startTime) / totalTime);
            companyBG.color = Color.Lerp(Color.white, new Color(1, 1, 1, 0), step);
            yield return null;
        }

        DisableCompanyMod();
    }


    private Camera _cam;
    private Camera cam
    {
        get
        {
            if (!_cam) { 
                _cam= Camera.main;
            }
            return _cam;
        }

    }

    private void LateUpdate()
    {
        if (cam != null)
        {
            this.transform.position = cam.transform.position;
            this.transform.rotation = cam.transform.rotation;
            this.transform.localScale = Vector3.one*cam.orthographicSize/ initialOrthoSize;
        }
    }

}
