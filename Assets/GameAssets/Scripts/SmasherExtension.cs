﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmasherExtension : MonoBehaviour {

    public Transform spriteRef;


    Transform parent;
    Vector3 scale;
    public void Awake()
    {
        scale = spriteRef.localScale;
        parent = this.transform.parent;
    }
    public void SetSpriteYScaleMult (float sY) {

        spriteRef.localScale = new Vector3(scale.x, scale.y*sY, scale.z);

    }
    public void OnMoverUpdate(Vector3 moverPosition)
    {
        Vector3 distanceVec = moverPosition - parent.position;
        this.transform.localScale = new Vector3(distanceVec.magnitude, 1, 1);
        //float d = Vector3.Distance(, parent.position);
        if (distanceVec.x > 0)
        {

            distanceVec.x += 1;
        }
        else
        {

            distanceVec.x -= 1;
        }
        this.transform.position = parent.position + distanceVec/2;// + new Vector3((d+1)*0.5f,0,0);
    }

    public void Hide()
    {
        this.transform.position = parent.position;

        this.transform.localScale = Vector3.one*0.5f;
    }
}
