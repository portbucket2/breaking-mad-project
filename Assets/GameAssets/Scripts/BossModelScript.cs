﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossModelScript : MonoBehaviour {
    //public Pressurizer mainRef;
    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void FlyOut()
    {
        anim.SetTrigger("flyOut");
    }
    public void FlyIn()
    {
        anim.SetTrigger("flyIn");
    }
    public void Fire()
    {
        anim.SetTrigger("fire");
    }

    public void Wake()
    {
        anim.SetTrigger("wake");
    }

    public void FlyOutComplete()
    {
        Pressurizer.OnFlyOutComplete();
    }
    public void FlyInComplete()
    {
        Pressurizer.OnFlyInComplete();
    }
    public void TimeToShake()
    {
        Pressurizer.TimeToShake();
    }
    //public void FireComplete()
    //{
    //    Pressurizer.OnFireComplete();
    //}

}
