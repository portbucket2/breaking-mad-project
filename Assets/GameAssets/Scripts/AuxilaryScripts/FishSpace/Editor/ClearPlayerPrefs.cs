﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishSpace
{

    public class EditorUtilities
    {
        [UnityEditor.MenuItem("Utility/Clear Player Prefs")]
        public static void ClearPP()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}