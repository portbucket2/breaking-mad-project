﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
namespace FishSpace
{
    public static class HardDataCleaner
    {
        public static void Clean()
        {
            HardData<int>.ResetAllHardData();
            HardData<float>.ResetAllHardData();
            HardData<string>.ResetAllHardData();
            HardData<bool>.ResetAllHardData();
        }
    }
    public class HardData<T>
    {
        string savedKey;
        T localValue;
        SettingTypes type;

        public static List<string> keyList = new List<string>();
        public static void ResetAllHardData()
        {
            foreach (string key in keyList)
            {
                PlayerPrefs.HasKey(key);
                PlayerPrefs.DeleteKey(key);
            }
        }

        public HardData(string Key, T initValue){
            #region defineType
            if (typeof(T) == typeof(bool)) 
            {
                type = SettingTypes._bool;
            }
            else if (typeof(T) == typeof(int)) 
            {
                type = SettingTypes._int;
            }
            else if (typeof(T) == typeof(float)) 
            {
                type = SettingTypes._float;
            }
            else if (typeof(T) == typeof(string)) 
            {
                type = SettingTypes._string;
            }
            else 
            {
                type = SettingTypes._UNDEFINEDTYPE;
                Debug.LogError ("Undefined setting type!!!");
            }
            #endregion
            savedKey = Key;
            if (keyList.Contains(savedKey))
                //Debug.LogWarningFormat("Duplicate keys: {0}!",savedKey);
            keyList.Add(savedKey);
            localValue = initValue;
            loadFromPref ();
        }

        public T value
        {
            set
            { 
                localValue = value;
                saveToPref ();
            }
            get
            {
                return localValue;
            }
        }

        void saveToPref()
        {
            switch (type) {
                default:
                    Debug.LogError ("Pref saving not defined for this type");
                    break;
                case SettingTypes._bool:
                    {
                        bool locBoolValue = (bool)Convert.ChangeType (localValue, typeof(bool));
                        int prefValue = (locBoolValue ? 1 : 0);
                        PlayerPrefs.SetInt (savedKey, prefValue);
                    }
                    break;
                case SettingTypes._int:
                    {
                        int locValue = (int)Convert.ChangeType (localValue, typeof(int));
                        PlayerPrefs.SetInt (savedKey, locValue);
                    }
                    break;      
                case SettingTypes._float:
                    {
                        float locValue = (float)Convert.ChangeType (localValue, typeof(float));
                        PlayerPrefs.SetFloat (savedKey, locValue);
                    }
                    break;
                case SettingTypes._string:
                    {
                        string locValue = (string)Convert.ChangeType (localValue, typeof(string));
                        PlayerPrefs.SetString (savedKey, locValue);
                    }
                    break;
            }
        }
        void loadFromPref()
        {
            switch (type) {
                default:
                    Debug.LogError ("Pref loading not defined for this type");
                    break;
                case SettingTypes._bool:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        int prefValue = PlayerPrefs.GetInt (savedKey);
                        bool prefBool = (prefValue != 0);
                        localValue = (T) Convert.ChangeType (prefBool, typeof(T));
                    }
                    break;
                case SettingTypes._int:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        int prefValue = PlayerPrefs.GetInt (savedKey);
                        localValue = (T) Convert.ChangeType (prefValue, typeof(T));
                    }
                    break;
                case SettingTypes._float:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        float prefValue = PlayerPrefs.GetFloat (savedKey);
                        localValue = (T) Convert.ChangeType (prefValue, typeof(T));
                    }
                    break;
                case SettingTypes._string:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        string prefValue = PlayerPrefs.GetString (savedKey);
                        localValue = (T) Convert.ChangeType (prefValue, typeof(T));
                    }
                    break;
            }
        }

        public enum SettingTypes
        {
            _bool,
            _int,
            _float,
            _string,
            _UNDEFINEDTYPE
        }
    }

}