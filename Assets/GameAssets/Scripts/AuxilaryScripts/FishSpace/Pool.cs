﻿using UnityEngine;
using System.Collections.Generic;
namespace FishSpace
{
    public static class Pool
    {

        private static Dictionary<GameObject, List<GameObject>> poolDictionary = new Dictionary<GameObject, List<GameObject>>();
        private static Dictionary<GameObject, GameObject> prefabMap = new Dictionary<GameObject, GameObject>();
        //public static Vector3 defaultPosition = Vector3.zero;
        //public static Quaternion defaultRotation = Quaternion.identity;

        public static int poolCount = 0;

        public static Transform UnmanagedPooledObjectParent;

        public static GameObject Instantiate(GameObject original, Transform parent = null)
        {
            return Instantiate(original, Vector3.zero, Quaternion.identity, parent);
        }

        public static GameObject Instantiate(GameObject original, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (!poolDictionary.ContainsKey(original))
            {
                poolDictionary.Add(original, new List<GameObject>());
                poolCount++;
            }
            if (UnmanagedPooledObjectParent == null)
            {
                UnmanagedPooledObjectParent = (new GameObject("PoolManager")).transform;
                //UnmanagedPooledObjectParent.gameObject.AddComponent<PoolManager>();
            }
            List<GameObject> currentPool = poolDictionary[original];
            CleanPool(currentPool);

            if (currentPool.Count == 0)
            {
                GameObject go = MonoBehaviour.Instantiate(original, position, rotation) as GameObject;
                if (parent == null)
                {
                    parent = UnmanagedPooledObjectParent;
                }
                if (go.transform.parent != parent)
                {
                    go.transform.SetParent(parent);
                }
                go.AddComponent<PooledItem>();
                go.GetComponent<PooledItem>().original = original;
                go.GetComponent<PooledItem>().alive = true;
                prefabMap.Add(go, original);
                return go;
            }
            else
            {
                GameObject retObject = currentPool[currentPool.Count - 1];
                currentPool.RemoveAt(currentPool.Count - 1);
                if(retObject ==null)Debug.LogError("isnull");
                Transform tr = retObject.transform;
                tr.SetParent(UnmanagedPooledObjectParent);
                PooledItem pitem = retObject.GetComponent<PooledItem>();
                if (pitem.alive) Debug.LogErrorFormat("Pool Error On - {0}", pitem.name); 
                retObject.GetComponent<PooledItem>().alive = true;
                tr.position = position;
                tr.rotation = rotation;
                retObject.SetActive(true);
                return retObject;
            }
        }
        public static bool IsPooled(GameObject gameObject)
        {
            return prefabMap.ContainsKey(gameObject);
        }
        public static void Destroy(GameObject gameObject, bool shouldTrash = false)
        {
            if (gameObject == null)
            {
                ////CM_Deb"Pool.Destroy was called with a destroyed game object...");
                return;
            }
            else if (!prefabMap.ContainsKey(gameObject))
            {
                ////CM_Deb"Pool.Destroy was called with an unpooled object...");
                MonoBehaviour.Destroy(gameObject);
                return;
            }


            GameObject original = prefabMap[gameObject];
            /*should not be necessary
             * if (!poolDictionary.ContainsKey(original))
            {
                poolDictionary.Add(original, new List<GameObject>());
                poolCount++;
            }*/
            List<GameObject> currentPool = poolDictionary[original];
            PooledItem pitem = gameObject.GetComponent<PooledItem>();
            if (!pitem.alive) {
                Debug.LogWarningFormat("Tried to destroy an object twice - {0}", pitem.name);
                return;
            }
            //else { //CM_Deb"{0} pool item destroyed", pitem.name); }
            pitem.alive = false;
            gameObject.SetActive(false);
            currentPool.Add(gameObject);

            if (shouldTrash)
            {
                if (UnmanagedPooledObjectParent == null)
                    UnmanagedPooledObjectParent = (new GameObject("UnmanagedPooledObjects")).transform;

                gameObject.transform.SetParent(UnmanagedPooledObjectParent);
            }
        }


        private static void CleanPool(List<GameObject> currentPool)
        {
            for (int i = currentPool.Count - 1; i >=0; i--)
            {
                if (currentPool[i] == null) currentPool.RemoveAt(i);
            }
        }
        //public static void ReleasePool(GameObject original)
        //{
        //    if (poolDictionary.ContainsKey(original))
        //    {
        //        List<GameObject> currentPool = poolDictionary[original];
        //        for (int i = 0; i < currentPool.Count; i++)
        //        {
        //            prefabMap.Remove(currentPool[i]);
        //            MonoBehaviour.Destroy(currentPool[i]);
        //        }
        //        poolDictionary.Remove(original);
        //        poolCount--;
        //    }
        //}
    }
    public class PooledItem : MonoBehaviour
    {
        public bool alive;
        internal GameObject original;

        public string poolLog;
        void OnDestroy() { if(alive)Pool.Destroy(gameObject); }
    }
}