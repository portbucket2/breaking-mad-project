﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NLog : MonoBehaviour {
    public static NLog instance;
    public GameObject logPanel;
    public NLogPanel A;
    public NLogPanel B;
    //public int maxLogCount=10;
    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (logPanel.activeSelf)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }
    }

    private void Show()
    {
        logPanel.SetActive(true);
    }
    private void Hide()
    {
        logPanel.SetActive(false);
    }

    public static void A_Log(string s)
    {
        if(instance) instance.A.AddLog(s);
    }
    public static void A_AppendLastLog(string s)
    {
        if (instance) instance.A.AppendLog(s);
    }
    public static void B_Log(string s)
    {
        if (instance) instance.B.AddLog(s);
    }
    public static void B_AppendLastLog(string s)
    {
        if (instance) instance.B.AppendLog(s);
    }
    //private static int logIndex = -1;
    //private static string[] logs;
    //public static void Init(int Max)
    //{
    //    logs = new string[Max];
    //}
    //public static void Log(string s)
    //{
    //    logIndex++;
    //    if (logIndex > logs.Length)
    //        logIndex = 0;
    //    logs[logIndex] = s;
    //}
}
