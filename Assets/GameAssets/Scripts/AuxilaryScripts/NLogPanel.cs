﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NLogPanel : MonoBehaviour {
    public List<Text> textList;
    //public int maxLogCount=10;
    private void Awake()
    {
        AddLog("Logging Started");
    }


    int LogIndex = 0;
    int totalLogCount = 0;

    Text lastTextRef;
    public void AddLog(string s)
    {
        if (LogIndex >= textList.Count)
        {
            LogIndex = 0;
        }
        lastTextRef = textList[LogIndex++];
        lastTextRef.text = string.Format("{1}- {0}",s, totalLogCount++);
        lastTextRef.transform.SetAsFirstSibling();
    }
    public void AppendLog( string s)
    {
        lastTextRef.text = string.Format("{0} {1}",lastTextRef.text,s);
    }
}
