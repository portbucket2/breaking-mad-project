﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.NativePlugins;

public class BMadAnalyticsHandler {

    public enum Category
    {
        adview,
        in_app_purchase,
        gameplay,

        UNITY_EDITOR,
    }
    public enum Actions_AdView
    {
        clicked,
        success,
        failure,
    }
    public enum Actions_InApp
    {
        heart10,
        heart25,
        heart60,
    }
    public enum Labels_InApp
    {
        clicked,
        success,
        failure,
    }
    public enum Actions_Gameplay
    {
        level_reached,
        level_death,
        revive_coin_spent,
        reason_of_death,
        retry_attempt,
        powerup_buy_success,
        powerup_buy_fail,
    }
    public static void ReportRetryAttempt(int sessionRetryCount, int deathLevel)
    {
        Report(Category.gameplay, Actions_Gameplay.retry_attempt.ToString(), string.Format("retry_no_{0}", sessionRetryCount), deathLevel);
    }
    public static void ReportShopItemHit(string itemId, int shopLevel, bool success)
    {
        Report(Category.gameplay, (success ? Actions_Gameplay.powerup_buy_success : Actions_Gameplay.powerup_buy_fail).ToString(), itemId, shopLevel);
    }



    public static void ReportAdvertisment(Actions_AdView action)
    {
        Report(Category.adview, action.ToString(),"AdCount", 0);
    }

    public static void ReportIAP(Actions_InApp action, Labels_InApp label)
    {
        Report(Category.in_app_purchase, action.ToString(), label.ToString(), 0);
    }


    public static void ReportLevelReached(int level)
    {
        Report(Category.gameplay, Actions_Gameplay.level_reached.ToString(), string.Format("{0}",level), 0);
    }
    public static void ReportLevelOfDeath(int level)
    {
        Report(Category.gameplay, Actions_Gameplay.level_death.ToString(), string.Format("{0}", level), 0);
    }
    public static void ReportReviveCoinSpent(int coins)
    {
        Report(Category.gameplay, Actions_Gameplay.revive_coin_spent.ToString(), Actions_Gameplay.revive_coin_spent.ToString(), coins);
    }
    public static void ReportReasonOfDeath(string reason)
    {
        Report(Category.gameplay, Actions_Gameplay.reason_of_death.ToString(), reason, 0);
    }

#if UNITY_EDITOR
    public static bool logToConsole = false;
#else
    public static bool logToConsole = false;
#endif
    static void Report(Category eventCategoryName, string action, string label, int value)
    {
#if UNITY_EDITOR
        if (logToConsole)
        {
            Debug.LogFormat("Analytics({0})|| {1} <{2}> {3}", eventCategoryName, action, label, value);
        }
#else
        ReportFinal(eventCategoryName, action, label, value);
#endif
    }

    static void ReportFinal(Category eventCategoryName, string action, string label, int value)
    {

        EventHitBuilder hit = new EventHitBuilder()
        .SetEventCategory(eventCategoryName.ToString())
        .SetEventAction(action)
        .SetEventLabel(label)
        .SetEventValue(value);

        PortblissPlugins.analytic.googleAnalyticsAPIentry.LogEvent(hit);
        PortblissPlugins.analytic.googleAnalyticsAPIentry.DispatchHits();
    }
}
