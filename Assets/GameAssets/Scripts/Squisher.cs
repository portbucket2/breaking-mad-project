﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squisher : MonoBehaviour
{
    public bool logon;

    public IVerticalCollidable selfChar;
   
    private void Start()
    {
        
        selfChar = GetComponentInParent<IVerticalCollidable>();
    }
    private void OnTriggerStay(Collider other)
    {
        IVerticalCollidable otherChar = other.GetComponentInParent<IVerticalCollidable>();
        if (otherChar != null && otherChar!=selfChar)
        {
           
            //if (logon) Debug.Log(other.gameObject.name);
            SquishResolver(primary: selfChar, secondary: otherChar, logon: logon);
        }

    }

    public static void SquishResolver(IVerticalCollidable primary, IVerticalCollidable secondary, bool logon)
    {
        if (primary.IsFalling() && secondary.IsFalling())//both falling
        {
            return;
        }
        else if (primary.IsFalling() || secondary.IsFalling())//one of them is falling, not both
        {

            float primaryY = (primary as MonoBehaviour).transform.position.y;
            float secondaryY = (secondary as MonoBehaviour).transform.position.y;

            IVerticalCollidable dropVC = primaryY > secondaryY ? primary : secondary;
            IVerticalCollidable baseVC = primaryY > secondaryY ? secondary : primary;


            ICharacter baseCharRef = baseVC as ICharacter;
            ICharacter dropCharRef = baseVC as ICharacter;
            if (baseCharRef != null && baseCharRef.GetMovementController() && baseCharRef.GetMovementController().IsAttacking(Dir.UP))
            {
                baseVC.ResolveAsMasterEntity(dropVC, Dir.UP);
            }
            else if (dropCharRef != null && dropCharRef.GetMovementController() && dropCharRef.GetMovementController().IsAttacking(Dir.DOWN))
            {
                dropVC.ResolveAsMasterEntity(dropVC, Dir.UP);
            }
            else if (dropVC.HasHardBoot())
            {
                dropVC.ResolveAsMasterEntity(baseVC, Dir.DOWN);
            }
            else if (baseVC.HasTopSpike())
            {
                baseVC.ResolveAsMasterEntity(dropVC, Dir.UP);
            }
            else
            {
                dropVC.ResolveAsMasterEntity(baseVC, Dir.DOWN);
            }
        }
        else // none falling
        {
            return;
        }

    }

}
