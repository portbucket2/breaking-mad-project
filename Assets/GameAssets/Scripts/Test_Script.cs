﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.NativePlugins;
using UnityEngine.Profiling;
using System.IO;

public class Test_Script : MonoBehaviour {

    [SerializeField] Button testUI, enableLog;
    [SerializeField] Text txt, logTXT;
    public static bool testModeEnabled = false, isLogEnabled = false;
    private void Start()
    {
        testUI.onClick.AddListener(() =>
        {
            testModeEnabled = !testModeEnabled;
            GlobalSettings.instance.infiniteSaveMe = testModeEnabled;
            //GlobalSettings.instance.invincibility = testModeEnabled;
            txt.color = testModeEnabled == true ? new Color(0,1,0,0.4f) : new Color(1, 0, 0, 0.4f);
        });
        txt.color = testModeEnabled == true ? new Color(0, 1, 0, 0.4f) : new Color(1, 0, 0, 0.4f);
        Debug.Log("will try to validate ironsource and also will retrieve GAID of device");
        //PortblissPlugins.ad.ValidateIronSource();
        Debug.Log("validation complete!");
        //enableLog.onClick.AddListener(() =>
        //{
        //    isLogEnabled = !isLogEnabled;
        //    logTXT.color = isLogEnabled == true ? Color.green : Color.red;

            
        //    var profilerDataFilePath = Path.Combine(Application.temporaryCachePath, "myForFrame" + frameN);
        //    //CM_Deb"Profiler file for frame " + frameN + ": " + profilerDataFilePath);
        //    Profiler.logFile = profilerDataFilePath;
        //    Profiler.enableBinaryLog = true;
        //});
        //logTXT.color = isLogEnabled == true ? Color.green : Color.red;
    }
	
	// Update is called once per frame
	void Update ()
    {
        ////CM_Deb"profiler is enabled?: " + Profiler.enabled);
	}
}
