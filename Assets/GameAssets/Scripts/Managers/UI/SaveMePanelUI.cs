﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.NativePlugins;

public class SaveMePanelUI : MonoBehaviour
{
    [SerializeField]
    Button watchVideo, bringHeartPanel, heart10, heart25, heart60;
    [SerializeField] Button saveMeSkip, heartBuyPanelSkip;
    [SerializeField]
    Text currentHeartNumberTXT, neededHeartNumToReviveTXT,
        youNeedMoreHeartTXT, heart10ProdLocalCurrencyTXT, heart25ProdLocalCurrencyTXT,
        heart60ProdLocalCurrencyTXT;
    [SerializeField]
    string heart_10_productID = "heart_10_prod",
        heart_25_productID = "heart_25_prod", heart_60_productID = "heart_60_prod";
    [Space]
    [SerializeField] FlyInOutScript saveMePanelFlier, heartBuyFlier;
    [Space]
    [SerializeField] int allowableSaveMeCountWithVideo = 1;
    const float animateTime_Default = 0.4f;
    public static bool saveMeRequestPending { get; private set; }
    public static bool saveConfirmed { get; private set; }
    public static bool skippedAtSaveMeUI { get; private set; }
    static int saveMeWithVideoCount = 0, currentHeartCount = 0,
        deathCountAtThisPlaySession = 0, lastHeartDemand = 0;
    static SaveMePanelUI instance;
    void Awake()
    {
        instance = this;
        currentHeartCount = PlayerPrefs.GetInt("BM_Heart", 0);
        Cleanup();

    }

    void OnInAppModuleInit()
    {
        string cur_10 = PortblissPlugins.inAppPurchase.GetLocalCurrencyForProductID(heart_10_productID);
        if (string.IsNullOrEmpty(cur_10) == false)
        {
            heart10ProdLocalCurrencyTXT.text = cur_10;
        }

        string cur_25 = PortblissPlugins.inAppPurchase.GetLocalCurrencyForProductID(heart_25_productID);
        if (string.IsNullOrEmpty(cur_25) == false)
        {
            heart25ProdLocalCurrencyTXT.text = cur_25;
        }

        string cur_60 = PortblissPlugins.inAppPurchase.GetLocalCurrencyForProductID(heart_60_productID);
        if (string.IsNullOrEmpty(cur_60) == false)
        {
            heart60ProdLocalCurrencyTXT.text = cur_60;
        }
    }

    void OnApplicationQuit()
    {
        PortblissPlugins.inAppPurchase.OnInAppModuleInitialize -= OnInAppModuleInit;
    }
   // [SerializeField] ParticleSystem[] allSplashParticles;

    static bool willPlay, willPlayCallback = false;
    float timer, timerCallback;
    System.Action particleCallback = null;
    [SerializeField] float splashParticlePlayTime = 2f;
    float callbackFireTime;
    public static void PlaySplashParticle(float fireTime, System.Action AfterParticlePlay)
    {
        //foreach (var item in instance.allSplashParticles)
        //{
        //    item.Play();
        //}
        //Debug.Break();
        instance.callbackFireTime = fireTime;
        willPlay = willPlayCallback = true;
        instance.particleCallback = AfterParticlePlay;
        //instance.splash.Play();
    }

    private void Update()
    {
        if (willPlayCallback)
        {
            timerCallback += Time.unscaledDeltaTime;
            if (timerCallback > callbackFireTime)
            {
                timerCallback = 0;
                willPlayCallback = false;
                ////CM_Deb"cb");
                //Debug.Break();
                if (particleCallback != null)
                {
                    particleCallback();
                    particleCallback = null;
                }
            }
        }

        if (willPlay == true)
        {
            if (Time.timeScale < 1)
            {
                ////CM_Deb"slow mo");
                //foreach (var item in instance.allSplashParticles)
                //{
                //    //item.Simulate(Time.unscaledDeltaTime, true, false);
                //}
            }
            else if (Time.timeScale >= 1)
            {
                ////CM_Deb"normal");
                //foreach (var item in instance.allSplashParticles)
                //{
                //    //item.Simulate(Time.deltaTime, true, false);
                //}
            }

            timer += Time.unscaledDeltaTime;
            if (timer > splashParticlePlayTime)
            {
                timer = 0;
                willPlay = false;
                //willPlay = false;

                //foreach (var item in instance.allSplashParticles)
                //{
                //   // item.Stop();
                //}

            }
        }
        else
        {
            timer = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    // Use this for initialization
    void Start()
    {

        

        PortblissPlugins.inAppPurchase.OnInAppModuleInitialize += OnInAppModuleInit;
        heartBuyPanelSkip.onClick.AddListener(() =>
        {
            heartBuyPanelSkip.interactable = false;
            if (cor != null)
            {
                StopCoroutine(cor);
            }
            Debug.Log("will skip!");

            //improve
            //StartCoroutine(saveMePanelFlier.TransitionCO(false, animateTime_Default, 0));
            StartCoroutine(heartBuyFlier.TransitionCO(false, animateTime_Default, 0));
            //improve end

            //Debug.Break();
            WaitForSeconds(0.08f, () =>
            {
                //CM_Deb"cross particle!");
                //Debug.Break();
                skippedAtSaveMeUI = true;
                saveMeRequestPending = false;
                Debug.Log("will skip at last!");
                //
                heartBuyPanelSkip.interactable = true;


            });
        });

        saveMeSkip.onClick.AddListener(() =>
        {
            saveMeSkip.interactable = false;
            if (cor != null)
            {
                StopCoroutine(cor);
            }
            Debug.Log("will skip 'saveMe' panel!");
            WaitForSeconds(0.08f, () =>
            {
                //CM_Deb"cross particle!");
                //Debug.Break();
                skippedAtSaveMeUI = true;
                saveMeRequestPending = false;
                Debug.Log("will skip 'saveMe' panel at last!");
                //
                saveMeSkip.interactable = true;


            });
        });


        watchVideo.onClick.AddListener(() => 
        {
            watchVideo.interactable = false;
            saveMeRequestPending = true;
            TrySaveMeWithVideo();
            WaitForSeconds(0.08f, () =>
            {
                
            });
        });

        bringHeartPanel.onClick.AddListener(() =>
        {
            bringHeartPanel.interactable = false;
            saveMeRequestPending = true;
            WaitForSeconds(0.08f, () =>
            {
                //jodi enough heart thake then direct revive
                //Or
                //Bring Buy Panel
                int heartNeeded = GetNeededHeart();
                ////CM_Deb"bringHeartPanel time lastDemand: " + lastHeartDemand + " and heartNeeded: " + heartNeeded);
                if (currentHeartCount < heartNeeded)
                {
                    BringBuyPanel();
                }
                else
                {
                    //consume
                    currentHeartCount -= heartNeeded;
                    PlayerPrefs.SetInt("BM_Heart", currentHeartCount);
                    BMadAnalyticsHandler.ReportReviveCoinSpent(heartNeeded);
                    lastHeartDemand = heartNeeded;
                    saveMeRequestPending = false;
                    saveConfirmed = true;
                    bringHeartPanel.interactable = true;
                }
            });
        });

        heart10.onClick.AddListener(() =>
        {
            heart10.interactable = false;
            AnySaveMeButtonClicked = true;
            saveMeRequestPending = true;
            if (cor != null)
            {
                StopCoroutine(cor);
            }
            WaitForSeconds(0.08f, () =>
            {
                BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart10,BMadAnalyticsHandler.Labels_InApp.clicked);
                Debug.Log("will try to buy 10 heart!");
                BuyProduct(heart_10_productID, ()=> {
                    heart10.interactable = true;
                });
            });
        });

        heart25.onClick.AddListener(() =>
        {
            heart25.interactable = false;
            AnySaveMeButtonClicked = true;
            saveMeRequestPending = true;
            if (cor != null)
            {
                StopCoroutine(cor);
            }
            WaitForSeconds(0.08f, () =>
            {
                BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart25, BMadAnalyticsHandler.Labels_InApp.clicked);
                Debug.Log("will try to buy 25 heart!");
                BuyProduct(heart_25_productID, ()=> {
                    heart25.interactable = true;
                });
            });
        });

        heart60.onClick.AddListener(() =>
        {
            heart60.interactable = false;
            AnySaveMeButtonClicked = true;
            saveMeRequestPending = true;
            if (cor != null)
            {
                StopCoroutine(cor);
            }
            WaitForSeconds(0.08f, () =>
            {
                BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart60, BMadAnalyticsHandler.Labels_InApp.clicked);
                Debug.Log("will try to buy 60 heart!");
                BuyProduct(heart_60_productID, ()=> {
                    heart60.interactable = true;
                });
            });
        });
    }

    void WaitForSeconds(float timeInSeconds, System.Action callback)
    {
        StartCoroutine(Waiter(timeInSeconds, callback));
    }

    IEnumerator Waiter(float t, System.Action callback)
    {
        yield return new WaitForSecondsRealtime(t);
        if (callback != null)
        {
            callback();
        }
    }

    public static bool AnySaveMeButtonClicked { get; set; }
    void TrySaveMeWithVideo()
    {
        Debug.Log("video button clicked!");
        AnySaveMeButtonClicked = true;
        BMadAnalyticsHandler.ReportAdvertisment(BMadAnalyticsHandler.Actions_AdView.clicked);
        PortblissPlugins.ad.videoAd.TryShowVideoAd((success, rewardName, rewardAmount) =>
        {
            saveMeRequestPending = false;
            
            if (success)
            {

                BMadAnalyticsHandler.ReportAdvertisment(BMadAnalyticsHandler.Actions_AdView.success);
                Debug.Log("video button ad callback fired!");
                saveConfirmed = true;
                saveMeWithVideoCount++;
            }
            else
            {
                BMadAnalyticsHandler.ReportAdvertisment(BMadAnalyticsHandler.Actions_AdView.failure);
                Debug.Log("could not show video ad!");
            }

            //improve
            StartCoroutine(saveMePanelFlier.TransitionCO(false, animateTime_Default, 0));
            //improve end
        });
    }

    Coroutine cor;
    void BringBuyPanel()
    {
        StartCoroutine(saveMePanelFlier.TransitionCO(false, animateTime_Default, 0));
        StartCoroutine(heartBuyFlier.TransitionCO(true, animateTime_Default, 0, ()=> {

            cor = StartCoroutine(SlowdownTimeToZero());
            bringHeartPanel.interactable = true;
        }));
    }

    IEnumerator SlowdownTimeToZero()
    {
        //second effect
        float startTime = Time.realtimeSinceStartup;
        float secondEffectTime = 3f;
        while (Time.realtimeSinceStartup < startTime + secondEffectTime)
        {
            Time.timeScale = Mathf.Lerp(0.1f, 0f, (Time.realtimeSinceStartup - startTime) / secondEffectTime);
            yield return null;
        }
        Time.timeScale = 0f;
        cor = null;
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            PlayerPrefs.SetInt("BM_Heart", 0);
        }
    }


    void BuyProduct(string id, System.Action OnComplete)
    {
        Time.timeScale = 0f;//ddd
        Debug.Log("buy call for productID: " + id);
        PortblissPlugins.inAppPurchase.BuyProduct(id, (iapResult) =>
        {

            //StartCoroutine(saveMePanelFlier.TransitionCO(false, animateTime_Default, 0));
            if (OnComplete != null)
            {
                OnComplete();
            }

            //what if this callback never fires?? say, due to internet connection issues??!!?
            if (iapResult != IAPresult.fail)
            {
                if (id == heart_10_productID)
                {
                    BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart10, BMadAnalyticsHandler.Labels_InApp.success);
                    currentHeartCount += 10;
                }
                else if (id == heart_25_productID)
                {
                    BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart25, BMadAnalyticsHandler.Labels_InApp.success);
                    currentHeartCount += 25;
                }
                else if (id == heart_60_productID)
                {
                    BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart60, BMadAnalyticsHandler.Labels_InApp.success);
                    currentHeartCount += 60;
                }

                
                

                PlayerPrefs.SetInt("BM_Heart", currentHeartCount);
                Debug.Log("purchase sucess! from callback");
                //now check if we intended to use these hearts and
                //check the last required hearts to resume. If then 
                //we have enough heart we resume
                //Or
                //Send user to purchase window again.
                int heartNeeded = GetNeededHeart();
                ////CM_Deb"buy time lastDemand: " + lastHeartDemand + " and heartNeeded: " + heartNeeded);
                ///
                Debug.Log("after purchase, current heart count: " + currentHeartCount + " and heartNeeded: " + heartNeeded);
                if (currentHeartCount < heartNeeded)
                {
                    //update how many heart is needed still UI
                    instance.neededHeartNumToReviveTXT.text = (heartNeeded - currentHeartCount) + "";
                    instance.youNeedMoreHeartTXT.text = "You need " + (heartNeeded - currentHeartCount);
                    Debug.Log("do not have enough heart, txt updated! value would be: " + (heartNeeded - currentHeartCount) + "");
                }

                if (currentHeartCount >= heartNeeded)
                {
                    //consume
                    currentHeartCount -= heartNeeded;
                    PlayerPrefs.SetInt("BM_Heart", currentHeartCount);
                    BMadAnalyticsHandler.ReportReviveCoinSpent(heartNeeded);
                    lastHeartDemand = heartNeeded;
                    saveConfirmed = true;
                    saveMeRequestPending = false;
                    Debug.Log("after purchase we also consumed it.");

                    //dddd
                    StartCoroutine(heartBuyFlier.TransitionCO(false, animateTime_Default, 0, () => {

                        //Time.timeScale = 1;//ddd
                        //
                    }));
                }
            }
            else
            {
                //saveMeRequestPending = false;
                if (id == heart_10_productID)
                {
                    BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart10, BMadAnalyticsHandler.Labels_InApp.failure);
                    
                }
                else if (id == heart_25_productID)
                {
                    BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart25, BMadAnalyticsHandler.Labels_InApp.failure);
                    
                }
                else if (id == heart_60_productID)
                {
                    BMadAnalyticsHandler.ReportIAP(BMadAnalyticsHandler.Actions_InApp.heart60, BMadAnalyticsHandler.Labels_InApp.failure);
                    
                }
                Debug.Log("purchase failure!");
            }
        });
    }

   

    public static void ResetSwitches()
    {
        saveMeRequestPending = saveConfirmed = skippedAtSaveMeUI = false;
    }

    
    public static void IncrementDeathCount()
    {
        deathCountAtThisPlaySession++;
    }

    public static void Cleanup()
    {
        saveMeRequestPending = saveConfirmed = skippedAtSaveMeUI = AnySaveMeButtonClicked = false;
        saveMeWithVideoCount = deathCountAtThisPlaySession = lastHeartDemand = 0;
    }

    /// <summary>
    /// If user can revive with video then show video and heart both, otherwise show only heart UI
    /// </summary>
    public static void SetUI()
    {
        bool canReviveWithVideo = saveMeWithVideoCount < instance.allowableSaveMeCountWithVideo ||
            GlobalSettings.instance.infiniteSaveMe;
        instance.currentHeartNumberTXT.text = currentHeartCount + "";
        int heartNeeded = GetNeededHeart();
        ////CM_Deb"setUI time lastDemand: " + lastHeartDemand + " and heartNeeded: " + heartNeeded);
        instance.neededHeartNumToReviveTXT.text = heartNeeded + "";
        instance.youNeedMoreHeartTXT.text = "You need " + (heartNeeded - currentHeartCount);

        Debug.Log("set ui time info--- saveMeWithVideoCount: " + saveMeWithVideoCount +
            "  and instance.allowableSaveMeCountWithVideo: " + instance.allowableSaveMeCountWithVideo +
            " and GlobalSettings.instance.infiniteSaveMe: " + GlobalSettings.instance.infiniteSaveMe);

        //EITHER panel with be active, not BOTH active/inactive!
        instance.watchVideo.gameObject.SetActive(canReviveWithVideo);
    }

    static int GetNeededHeart()
    {
        int finalCount = deathCountAtThisPlaySession - saveMeWithVideoCount;
        if (finalCount == 0) {

            throw new System.Exception("It can never be!! 'finalCount' is zero!");
        }
        
        return finalCount == 1 ? 1 : lastHeartDemand * 2;
    }
}