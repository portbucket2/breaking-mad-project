﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyInOutScript : MonoBehaviour {
    public float disabledDistanceY;
    public bool startEnabled=false;
    RectTransform rectT;
    Vector2 enabledPos;
    Vector2 disabledPos;
    
    void Init () {
        rectT = GetComponent<RectTransform>();
        enabledPos = rectT.anchoredPosition;
        disabledPos = rectT.anchoredPosition + new Vector2(0, disabledDistanceY);
        if(!startEnabled) gameObject.SetActive(false);
    }


    public IEnumerator TransitionCO(bool enable,float animateTime, float initialDelay, System.Action callback=null)
    {
        if (rectT == null) Init();

        Vector2 startPos;
        Vector2 endPos;
        if (enable)
        {
            startPos = disabledPos;
            endPos = enabledPos;
        }
        else
        {
            startPos = enabledPos;
            endPos = disabledPos;
        }

        if (enable) gameObject.SetActive(true);
        rectT.anchoredPosition = startPos;
        yield return new WaitForSecondsRealtime(initialDelay);

        float startTime = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < startTime + animateTime)
        {
            float step = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / animateTime);
            rectT.anchoredPosition = Vector2.Lerp(startPos, endPos, step);
            yield return null;
        }
        rectT.anchoredPosition = endPos;
        if (!enable) gameObject.SetActive(false);
        if (callback != null) callback();
    }
}
