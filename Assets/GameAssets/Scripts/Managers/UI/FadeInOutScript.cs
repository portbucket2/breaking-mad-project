﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FadeInOutScript : MonoBehaviour {
    public bool startEnabled=false;
    Image img;

    Color enabledCol;
    Color disabledCol;
    void Init () {
        img = GetComponent<Image>();
        if(!startEnabled) gameObject.SetActive(false);

        enabledCol = img.color;
        disabledCol = enabledCol;
        disabledCol.a = 0;
    }

    

    public IEnumerator TransitionCO(bool enable,float animateTime, float initialDelay, System.Action callback=null)
    {
        if (img == null) Init();
        Color startCol;
        Color endCol;
        if (enable)
        {
            startCol = disabledCol;
            endCol = enabledCol;
        }
        else
        {
            startCol = enabledCol;
            endCol = disabledCol;
        }

        if (enable) gameObject.SetActive(true);
        img.color = startCol;
        yield return new WaitForSecondsRealtime(initialDelay);

        float startTime = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < startTime + animateTime)
        {
            float step = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / animateTime);
            img.color = Color.Lerp(startCol, endCol, step);
            yield return null;
        }
        img.color = endCol;
        if (!enable) gameObject.SetActive(false);
        if (callback != null) callback();
    }
}
