﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OptionsUI : MonoBehaviour {
    public FadeInOutScript faderBG;
    public float animateTime;
    public List<FlyInOutScript> fliers;
    public float flyTimeGap;


    public Button sfxButton;
    public Button musicButton;
    public Button achievementButton;
    public Button leaderBoardButton;
    public Button logoutButton;

    public Image sfxImage;
    public Image musicImage;

    public Sprite sfxOnSprite;
    public Sprite sfxOffSprite;
    public Sprite musicOnSprite;
    public Sprite musicOffSprite;


    public void OnSfxButton()
    {
        SoundKeeper.sfxOn.value = !SoundKeeper.sfxOn.value;
        LoadAppropriateImages();
    }
    public void OnMusicButton()
    {
        SoundKeeper.musicOn.value = !SoundKeeper.musicOn.value;
        LoadAppropriateImages();
    }
    void LoadAppropriateImages()
    {
        sfxImage.sprite = SoundKeeper.sfxOn.value ? sfxOnSprite : sfxOffSprite;
        musicImage.sprite = SoundKeeper.musicOn.value ? musicOnSprite : musicOffSprite;
    }

    private void Start()
    {
        faderBG.gameObject.SetActive(false);

    }

    public void ShowOptionsUI()
    {
        StartCoroutine(ChangeStateCo(true));

        LoadAppropriateImages();

        sfxButton.onClick.RemoveAllListeners();
        musicButton.onClick.RemoveAllListeners();
        sfxButton.onClick.AddListener(OnSfxButton);
        musicButton.onClick.AddListener(OnMusicButton);
        achievementButton.interactable = false;
        leaderBoardButton.interactable = false;
        logoutButton.interactable = false;
    }
    public void HideOptionsUI()
    {
        StartCoroutine(ChangeStateCo(false));
    }

    IEnumerator ChangeStateCo(bool enable)
    {
        if (enable)
        {
            faderBG.gameObject.SetActive(true);
            for (int i = 0; i < fliers.Count; i++)
            {
                StartCoroutine(fliers[i].TransitionCO(enable, animateTime, i * flyTimeGap));
            }

            yield return StartCoroutine(faderBG.TransitionCO(enable,animateTime,0));
        }
        else
        {
            for (int i = 0; i < fliers.Count; i++)
            {
                StartCoroutine(fliers[i].TransitionCO(enable, animateTime, 0));
            }
            yield return StartCoroutine(faderBG.TransitionCO(enable, animateTime, 0));
            faderBG.gameObject.SetActive(false);
        }
    }
    //IEnumerator TransitionCO( float startAlpha, float endAlpha)
    //{
    //    float startTime = Time.time;
    //    faderBG.color = new Color(1, 1, 1, startAlpha);


    //    while (Time.time < startTime + animateTime)
    //    {
    //        float step = Mathf.Clamp01((Time.time - startTime) / animateTime);
    //        float alpha = Mathf.Lerp(startAlpha, endAlpha, step);
    //        faderBG.color = new Color(1, 1, 1, alpha);
    //        yield return null;
    //    }
    //    faderBG.color = new Color(1, 1, 1, endAlpha);

    //}

}
