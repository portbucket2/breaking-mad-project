﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PopInOutScript : MonoBehaviour {
    public bool startEnabled = false;
    Transform rectT;
    Vector3 enabledScale;
    Vector3 disabledScale;

    void Init()
    {
        rectT = GetComponent<RectTransform>();
        enabledScale = Vector3.one;
        disabledScale = Vector3.zero;
        if (!startEnabled) gameObject.SetActive(false);
    }


    public IEnumerator TransitionCO(bool enable, float animateTime, float initialDelay, System.Action callback = null)
    {
        if (rectT == null) Init();

        Vector3 startScale;
        Vector3 endScale;
        if (enable)
        {
            startScale = disabledScale;
            endScale = enabledScale;
        }
        else
        {
            startScale = enabledScale;
            endScale = disabledScale;
        }

        if (enable) gameObject.SetActive(true);
        rectT.localScale = startScale;
        yield return new WaitForSecondsRealtime(initialDelay);

        float startTime = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < startTime + animateTime)
        {
            float step = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / animateTime);
            rectT.localScale = Vector2.Lerp(startScale, endScale, step);
            yield return null;
        }
        rectT.localScale = endScale;
        if (!enable) gameObject.SetActive(false);
        if (callback != null) callback();
    }
}
