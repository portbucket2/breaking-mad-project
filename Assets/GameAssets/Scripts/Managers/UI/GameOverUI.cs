﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameOverUI : MonoBehaviour {
    public UIManager uiman;
    public Text maxLevels;
    public Text scoreText;
    public Text bestScoreText;
    public FadeInOutScript faderBG;
   // [SerializeField] ParticleSystem[] particleListOnGameOver;
    public float animateTime;
    public List<FlyInOutScript> fliers;
    public float flyTimeGap;

    public FishSpace.HardData<int> bestScore;

    private void Start()
    {
        bestScore = new FishSpace.HardData<int>("HIGH_SCORE", 0);
        faderBG.gameObject.SetActive(false);
        //foreach (var ps in particleListOnGameOver)
        //{
        //    ps.gameObject.SetActive(true);
        //}
    }

    public void ShowGameOverUI()
    {
        maxLevels.text = LevelManager.instance.currentLevelNumber.ToString();
        int score = PlayerManager.instance.GetLastScore() ;

        if (score > bestScore.value)
            bestScore.value = score;

        scoreText.text = score.ToString();
        bestScoreText.text = bestScore.value.ToString();

        StartCoroutine(ChangeStateCo(true));
    }
    public void Continue()
    {
        uiman.Restart();
        //StartCoroutine(ChangeStateCo(false,()=> {
        //    uiman.Restart();
        //}));
    }

    IEnumerator ChangeStateCo(bool enable, System.Action callback=null)
    {
        if (enable)
        {
            faderBG.gameObject.SetActive(true);
            for (int i = 0; i < fliers.Count; i++)
            {
                StartCoroutine(fliers[i].TransitionCO(enable, animateTime, i * flyTimeGap));
            }

            yield return StartCoroutine(faderBG.TransitionCO(enable,animateTime,0));
        }
        else
        {
            for (int i = 0; i < fliers.Count; i++)
            {
                StartCoroutine(fliers[i].TransitionCO(enable, animateTime, 0));
            }
            yield return StartCoroutine(faderBG.TransitionCO(enable, animateTime, 0));
            faderBG.gameObject.SetActive(false);
        }
        if (callback != null) callback();
    }
    //IEnumerator TransitionCO( float startAlpha, float endAlpha)
    //{
    //    float startTime = Time.time;
    //    faderBG.color = new Color(1, 1, 1, startAlpha);


    //    while (Time.time < startTime + animateTime)
    //    {
    //        float step = Mathf.Clamp01((Time.time - startTime) / animateTime);
    //        float alpha = Mathf.Lerp(startAlpha, endAlpha, step);
    //        faderBG.color = new Color(1, 1, 1, alpha);
    //        yield return null;
    //    }
    //    faderBG.color = new Color(1, 1, 1, endAlpha);

    //}

}
