﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using FishSpace;
using Portbliss.NativePlugins;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public event System.Action OnPlayerWasSaved;

    public Text scoreText;
    public Animator scoreImageAnim;
    //public ParticleSystem scoreChangeParticle;
    public GameObject bombButtonObject;
    public Text bombCountText;
    public Button bombTriggerButton;

    public GameObject buffHolder;
    public GameObject buffIndicatorPrefab;

    public GameObject levelLoadingIndicator;

    public GameObject pauseIndicator;
    public GameObject countDownDisplay;

    public Image newPowerUpImage;

    public Text countDownText;

    public Text shopTitle;
    public Text shopDescription;
    public Image shopImage;
    [Space]
    public FlyInOutScript instructionFlier;
    public FlyInOutScript startPanelFlier;
    public FlyInOutScript pausePanelFlier;
    public FlyInOutScript inGameTopPanelFlier;
    public FlyInOutScript inGameBottomPanelFlier;
    public FlyInOutScript saveMePanelFlier;
    [SerializeField] FlyInOutScript heartShopFlier;
    public FlyInOutScript shopDetailFlier;

    public FadeInOutScript pausedBGFader;

    public PopInOutScript newPowerUpPopper;

    public GameOverUI gameOverUI;

    [Space]
    public PowerUpProfiles allPowerUps;


    public Button showStartAdBtn;

    private const float animateTime_Default = 0.4f;

   // [SerializeField] ParticleSystem[] flashParticles;
    private void Awake()
    {
        instance = this;
        if (introFlashActive)
        {
            introFlashActive = false;
            //foreach (var ps in flashParticles)
            //{
            //    ps.Play();
            //}
        }
    }

    IEnumerator Start()
    {
        scoreText.text = 0.ToString();
        levelLoadingIndicator.SetActive(false);

        PlayerManager.instance.scoreHasChanged += UpdateScoreUIText;
        PlayerManager.instance.bombCountHasChanged += UpdateBombCountUIText;
        PlayerManager.instance.buffListChanged += UpdateBuffList;
        PlayerManager.instance.newPowerUpAdded += NewPowerUpAdded;
        PlayerManager.instance.playerTookFatalHit += OnPlayerAboutToDie;

        LevelManager.instance.loadingStarted += OnLoadingNewLevel;
        LevelManager.instance.loadingFinished += OnLoadingLevelDone;

        HitBaseTerrain.OnPlayerHitTerrain += HideStartPanel;

        bombTriggerButton.onClick.AddListener(PlayerManager.instance.UseBomb);

        StartCoroutine(RunShopUpdate());

        //instructionClear = false;


        if (TutorialScript.isTutorialRunning)
        {
            startPanelFlier.gameObject.SetActive(false);
            TutorialScript.instance.onTutorialIsOver += () => { EnableInGameElements(true); };
        }
       
        yield return new WaitForSecondsRealtime(3);
        //if (!instructionClear) RemoveInstruction();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            StartAdReward_Callback(true);
        }
    }
#endif

    //bool instructionClear;
    //private void RemoveInstruction()
    //{
    //    StartCoroutine(instructionFlier.TransitionCO(false, 0.75f, 0));
    //    instructionClear = true;
    //}
    private void EnableInGameElements(bool enable)
    {
        StartCoroutine(inGameTopPanelFlier.TransitionCO(enable, animateTime_Default, 0));
        StartCoroutine(inGameBottomPanelFlier.TransitionCO(enable, animateTime_Default, 0));
    }

    void HideStartPanel()
    {
        HitBaseTerrain.OnPlayerHitTerrain -= HideStartPanel;
        StartCoroutine(startPanelFlier.TransitionCO(false,animateTime_Default, 0));
        if (!TutorialScript.isTutorialRunning) EnableInGameElements(true);
        //if (!instructionClear) RemoveInstruction();
    }
    void UpdateScoreUIText(int score)
    {
        if (score == targetScore) return;
        //scoreChangeParticle.Play();
        startScore = runningScore;
        targetScore = score;
        Centralizer.Add_DelayedAct(()=> { UpdateScoreNow( 0.25f); }, 0.1f);
        Centralizer.Add_DelayedAct(() => { UpdateScoreNow( 0.5f); }, 0.2f);
        Centralizer.Add_DelayedAct(() => { UpdateScoreNow( 0.75f); }, 0.3f);
        Centralizer.Add_DelayedAct(() => { UpdateScoreNow( 1); }, 0.4f);
    }

    int runningScore = 0;
    int startScore = 0;
    int targetScore = 0;
    void UpdateScoreNow(float step)
    {
        runningScore = (int)Mathf.Lerp(startScore, targetScore, step);
        if (this.gameObject)
        {
            scoreText.text = runningScore.ToString();
            if (scoreImageAnim.gameObject.activeInHierarchy) scoreImageAnim.SetTrigger("pop");
        }
    }
    void UpdateBombCountUIText(int count)
    {
        if (count <= 0)
        {
            bombButtonObject.SetActive(false);
        }
        else
        {
            bombButtonObject.SetActive(true);
        }
        bombCountText.text = string.Format("{0}", count);
    }
    void UpdateBuffList(List<PowerUpProfile> buffs)
    {
        buffHolder.SetActive(buffs.Count != 0);

        foreach (Transform item in buffHolder.transform)
        {
            Pool.Destroy(item.gameObject);
        }
        foreach (var item in buffs)
        {
            Transform tr = Pool.Instantiate(buffIndicatorPrefab).transform;
            tr.GetComponent<BuffIndicator>().buffImage.sprite = item.sprite;
            tr.position = buffHolder.transform.position;
            tr.SetParent(buffHolder.transform);
            //tr.position = new Vector3(tr.position.x,tr.position.y, 0);
            tr.rotation = Quaternion.identity;
            tr.localScale = Vector3.one;
        }
    }
    void NewPowerUpAdded(PowerUpProfile ppPorfile)
    {
        newPowerUpImage.sprite = ppPorfile.sprite;
        StartCoroutine(newPowerUpPopper.TransitionCO(true, animateTime_Default,0, () =>
        {
            StartCoroutine(newPowerUpPopper.TransitionCO(false, animateTime_Default, 1));
        }));
    }

    void OnLoadingNewLevel()
    {
        levelLoadingIndicator.SetActive(true);
    }
    void OnLoadingLevelDone()
    {

        levelLoadingIndicator.SetActive(false);
    }


    public static bool introFlashActive = false;

    //private static HardData<int> retryCount = new HardData<int>("RETRYCOUNT_LASTSESSION",0);
    private static int retryCount = 0;
    public void Restart()
    {
        retryCount++;
        BMadAnalyticsHandler.ReportRetryAttempt(retryCount,LevelManager.instance.currentLevelNumber);
        Time.timeScale = 1f;
        introFlashActive = true;
        ////CM_Deb"<color=green>now loading will be loaded</color>");
        SceneManager.LoadScene("LoadingScene");
    }
    public bool isPaused { get { return !inGameTopPanelFlier.gameObject.activeSelf; } }
    public void Pause()
    {
        Time.timeScale = 0f;
        StartCoroutine(pausedBGFader.TransitionCO(true, animateTime_Default, 0, () => { pauseIndicator.SetActive(true); }));
        StartCoroutine(pausePanelFlier.TransitionCO(true, animateTime_Default, 0));
        EnableInGameElements(false);
    }
    public void Resume()
    {
        Time.timeScale = 1;
        StartCoroutine(pausedBGFader.TransitionCO(false, animateTime_Default, 0, () => { pauseIndicator.SetActive(false); }));
        StartCoroutine(pausePanelFlier.TransitionCO(false, animateTime_Default, 0));
        EnableInGameElements(true);
    }



    public void StartAdReward()
    {
        showStartAdBtn.interactable = false;
        PortblissPlugins.ad.videoAd.TryShowVideoAd((success, rewardName, rewardAmount) =>
        {
            StartAdReward_Callback(success);
        });
    }
    private List<PowerUpProfile> dropPPlist = new List<PowerUpProfile>();
    void StartAdReward_Callback(bool success)
    {
        showStartAdBtn.interactable = !success;
        if (success)
        {
            dropPPlist.Clear();
            foreach (var item in allPowerUps.powerUpList)
            {
#if UNITY_EDITOR
                //if(item.type==PowerUpType.SHIELD) dropPPlist.Add(item);
                if (item.canDropOnAd) dropPPlist.Add(item);
#else
                if (item.canDropOnAd) dropPPlist.Add(item);
#endif
            }
            PowerUpProfile pup = dropPPlist[Random.Range(0, dropPPlist.Count)];
            PlayerManager.instance.AddPowerUp(pup);
        }
    }
    
    public void OnPlayerAboutToDie()
    {
        StartCoroutine(OnDeathStuff());
    }
    IEnumerator OnDeathStuff()
    {
        //call reset from savemePanel script
        SaveMePanelUI.ResetSwitches();
        SaveMePanelUI.IncrementDeathCount();
        EnableInGameElements(false);
        float startTime = Time.realtimeSinceStartup;
        float effectTime = 1f;
        while (Time.realtimeSinceStartup<startTime+effectTime)
        {
            Time.timeScale = Mathf.Lerp(0.3f, 0.1f, (Time.realtimeSinceStartup - startTime) / effectTime);
            yield return null;
        }
        
        SaveMePanelUI.SetUI();
        StartCoroutine(pausedBGFader.TransitionCO(true, animateTime_Default, 0));//pause background fader will appear gradually
        StartCoroutine(CountdownTask(3f));//start and display the countdown

        //after the saveMe Panel appears in front of, start dying coroutine. Inside the dying coroutine:
        //if we are still counting, check on switches
        StartCoroutine(saveMePanelFlier.TransitionCO(true, animateTime_Default, 0, ()=> {

            StartCoroutine(PlayerDyingCoroutine(OnGameOver));
        }));

        //second effect
        //startTime = Time.realtimeSinceStartup;
        //float secondEffectTime = 3f;
        //while (Time.realtimeSinceStartup < startTime + secondEffectTime)
        //{
        //    //Time.timeScale = Mathf.Lerp(0.1f, 0f, (Time.realtimeSinceStartup - startTime) / secondEffectTime);
        //    yield return null;
        //}
        //Time.timeScale = 0f;
    }

    public bool counting = false;
    IEnumerator CountdownTask(float countTime)
    {
        counting = true;
        countDownDisplay.SetActive(true);
        float startTime = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < startTime + countTime)
        {
            float step = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / countTime);
            int timeLeft = Mathf.CeilToInt(countTime * (1 - step));
            countDownText.text = timeLeft.ToString();
            //we will continue to display countdown until user press cross button or any button 
            //which signifies that user requested to save itself by ad/in app purchase buttons
            ////CM_Deb"counting loop: isCounting? " + counting + " have we pressed skip button any? " + SaveMePanelUI.skippedAtSaveMeUI +
            //    " any req is pending? " + SaveMePanelUI.saveMeRequestPending);
            if (SaveMePanelUI.skippedAtSaveMeUI || SaveMePanelUI.saveMeRequestPending)
            {
                ////CM_Deb"we pressed cross button or any button for the save request");
                countDownDisplay.SetActive(false);
                break;
            }
            yield return null;
        }
        countDownDisplay.SetActive(false);
        counting = false;
    }

    public void OnGameOver()
    {
        gameOverUI.ShowGameOverUI();
    }

    IEnumerator PlayerDyingCoroutine(System.Action OnGameOver)
    {
        while (true)
        {
            //countdown is over, so have we requested to save?
            if (counting == false)
            {
                //Debug.Log("wating and counting <color=red>NOT happening</color>");
                if (SaveMePanelUI.saveMeRequestPending == false || SaveMePanelUI.skippedAtSaveMeUI == true || SaveMePanelUI.saveConfirmed == true)
                {
                    break;
                }
            }
            else
            {
                //Debug.Log("wating and counting <color=green>happening</color>");
                //need to further inspect for any possible bug!!
                if (SaveMePanelUI.AnySaveMeButtonClicked)
                {
                    if (SaveMePanelUI.skippedAtSaveMeUI == true || SaveMePanelUI.saveConfirmed == true || SaveMePanelUI.saveMeRequestPending == false)
                    {
                        break;
                    }
                }
            }
            //Debug.Log("wating co routine!");
            yield return null;
        }

        //Debug.Log("wait is over lets process t: " + Time.realtimeSinceStartup);
        //Debug.Break();
        ////CM_Deb"gameover particle play start!");
        ////CM_Deb"now hide current UI");
        StartCoroutine(saveMePanelFlier.TransitionCO(false, animateTime_Default, 0, () => { }));
        //StartCoroutine(heartShopFlier.TransitionCO(false, animateTime_Default, 0, () => { Time.timeScale = 1f; }));
        StartCoroutine(pausedBGFader.TransitionCO(false, animateTime_Default, 0, () => { }));
        //Debug.Log("wait is over lets process 1: t: " + Time.realtimeSinceStartup);
        if (SaveMePanelUI.skippedAtSaveMeUI)
        {
            //Debug.Log("wait is over lets process 2 t: " + Time.realtimeSinceStartup);
            SaveMePanelUI.PlaySplashParticle(0.03f, () => {
               // Debug.Log("wait is over lets process 3 t: " + Time.realtimeSinceStartup);
                if (SaveMePanelUI.saveConfirmed)
                {
                    //Debug.Log("wait is over lets process 4 t:" + Time.realtimeSinceStartup);
                    ////CM_Deb"save confirmed!");
                    Time.timeScale = 1f;
                    if (OnPlayerWasSaved != null) OnPlayerWasSaved();
                    EnableInGameElements(true);
                    throw new System.Exception("we skipped and we also saved the user?! How come?! This can never be!");
                }
                else
                {
                    //Debug.Log("wait is over lets process 5 t: " + Time.realtimeSinceStartup);
                    SaveMePanelUI.Cleanup();//we ONLY want to clean this up when we are finally exit from the game!
                    if (OnGameOver != null) OnGameOver();
                }
            });
            //Debug.Log("wait is over lets process 6 t: " + Time.realtimeSinceStartup);
        }
        else
        {
            //Debug.Log("wait is over lets process 7 t: " + Time.realtimeSinceStartup);

            //Debug.Log("wait is over lets process 8 t: " + Time.realtimeSinceStartup);
            ////CM_Deb"gameover particle play finish!");
            if (SaveMePanelUI.saveConfirmed)
            {
                ////CM_Deb"save confirmed!");
                //Debug.Log("wait is over lets process 9 t: " + Time.realtimeSinceStartup);
                Time.timeScale = 1f;
                if (OnPlayerWasSaved != null) OnPlayerWasSaved();
                EnableInGameElements(true);
                Debug.LogWarning("now revive");
            }
            else
            {
                //Debug.Log("wait is over lets process 10 t: " + Time.realtimeSinceStartup);
                SaveMePanelUI.Cleanup();//we ONLY want to clean this up when we are finally exit from the game!
                if (OnGameOver != null) OnGameOver();
            }
            
            //SaveMePanelUI.PlaySplashParticle(1.8f, () => {

            

            //});
        }

        //Debug.Log("wait is over lets process 11 t: " + Time.realtimeSinceStartup);
        /*
        ////CM_Deb"now we will finally gameover");
        
        */

        
        SaveMePanelUI.AnySaveMeButtonClicked = false;
    }

    public void NowActivateWatchVideoButton()
    {
        watchVideoBtn.interactable = true;
    }

    [SerializeField] Button watchVideoBtn;

    PowerUpProfile _targetDisplayItem
    {
        get
        {
            if (ShopVicinityCamZoomer.currentSVCamZoomer)
            {
                return ShopVicinityCamZoomer.currentSVCamZoomer.powerUpProfile;
            }
            else return null;
        }
    }

    IEnumerator RunShopUpdate()
    {
        PowerUpProfile currentDisplayItem=null;

        while (true)
        {
            if (currentDisplayItem != _targetDisplayItem)
            {
                if (currentDisplayItem != null)
                {
                    yield return StartCoroutine(shopDetailFlier.TransitionCO(false,animateTime_Default*0.6f,0));
                }
                currentDisplayItem = _targetDisplayItem;

                if (_targetDisplayItem != null)
                {
                    shopImage.sprite = _targetDisplayItem.sprite;
                    shopDescription.text = _targetDisplayItem.description;
                    shopTitle.text = _targetDisplayItem.title;

                    yield return StartCoroutine(shopDetailFlier.TransitionCO(true, animateTime_Default * 0.6f, 0));
                }
            }
            yield return null;
        }
    }
}
