﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spwner : MonoBehaviour {
    private void OnTriggerEnter(Collider other)
    {
        BaseTerrain spwnerTerrain = other.GetComponentInParent<BaseTerrain>();
        if (spwnerTerrain != null)
        {
            spwnerTerrain.SpwnEnemy();
        }
    }
}
