﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using FishSpace;

public class LevelManager : MonoBehaviour
{

    HardData<bool> tutorialShownOnce;

    public const float gridUnitDimension = 1;
    public static LevelManager instance;

    public bool showTutorialLevel = true;
    public bool disableParticles = false;
    public LevelAssets assets;

#if UNITY_EDITOR
    public bool loadTestLevel
    {
        get
        {
            return testLevel.enableTestMode;
        }
    }
#else
    public const bool loadTestLevel = false;
#endif
    public LevelProfile testLevel;
    public LevelProfile[] levels;
    //public ChancedListKeep clr = new ChancedList<GameObject>();

    [HideInInspector]
    public GridMaster grid;

    public static HardData<bool> unlockedFireheadOnLVL1;


    private void Awake()
    {
        if (LoadingSceneScript.instance == null)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            return;
        }

        unlockedFireheadOnLVL1 = new HardData<bool>("UNLOCKED_FIREHEAD_ONLVL01", false);

        tutorialShownOnce = new HardData<bool>("TUT_SHOWN", false);

        assets.basicRowOptions.Init();
        assets.rockyRowOptions.Init();
        assets.spikeCoverOptions.Init();
        instance = this;
        grid = new GridMaster(gridUnitDimension, this.transform, Vector3.down, assets);
        CheckAndLoadLevelIfNeeded(1);
    }


    public static void CheckAndLoadLevelIfNeeded(int requiresLevel)
    {
        if (requiresLevel > instance.currentLevelNumber)
            instance.StartCoroutine(instance.StartLoadingNewLevel());
    }
    public int currentLevelNumber
    {
        get
        {
            return currentLevelIndex + 1;
        }
    }
    int currentLevelIndex = -1;


    public event System.Action loadingStarted;
    public event System.Action loadingFinished;
    private IEnumerator StartLoadingNewLevel()
    {
        if (currentLevelNumber == 0)
            CreateLevelNow();
        else
        {
            //float startingTimeScale = Time.timeScale;
            if (loadingStarted != null) loadingStarted();
            Time.timeScale = 0;
            yield return new WaitForSecondsRealtime(0.2f);
            CreateLevelNow();
            yield return new WaitForSecondsRealtime(0.8f);
            Time.timeScale = 1;// startingTimeScale;
            if (loadingFinished != null) loadingFinished();

        }
    }

    public LevelProfile currentLevelProfile { get; private set; }

    private void CreateLevelNow()
    {
        currentLevelIndex++;
        if (loadTestLevel)
        {
            currentLevelProfile = testLevel;
        }
        else
        {
            if (currentLevelNumber < levels.Length)
                currentLevelProfile = levels[currentLevelIndex];//index is used so incremented letter
            else
            {
                currentLevelProfile = levels[levels.Length - 1];
            }
        }
        if (currentLevelIndex == 2)
        {
            // Debug.LogError("Firehead level 1 unlocked");
            unlockedFireheadOnLVL1.value = true;
        }
        currentLevelProfile.Initialize(levelIndex: currentLevelIndex);
        BMadAnalyticsHandler.ReportLevelReached(currentLevelNumber);
        //nextLevelProfile.Refresh();
        currentLevelProfile.SetRandomChallenge(assets);
        //LevelProfile nextLevelProfile = levels[currentLevel++];//index is used so incremented letter
        ////CM_Deb"Loading level: {0}",currentLevel);
        
        grid.possibleSpwnPoints.Clear();
        VisualBaseTerrain.ClearVBTList();


        int activeRowBudget = currentLevelProfile.rowCount;
        if (currentLevelNumber != 1)
        {
            new ShopLayer(grid, currentLevelProfile);
        }
        else
        {
            if (!tutorialShownOnce.value)
            {
                new TutorialLayer(grid, currentLevelProfile, 3);
                TutorialScript.Init();
                TutorialScript.instance.onTutorialIsOver += () => { tutorialShownOnce.value = true; };
                activeRowBudget -= 7;
            }
            else
                new StartLayer(grid, currentLevelProfile);
        }

        while (activeRowBudget>0)
        {
            ChallengeType challengeType = currentLevelProfile.currentChallengeGroup.GetRandomChallenge();
            if (!unlockedFireheadOnLVL1.value)
            {
                while (challengeType == ChallengeType.FIREHEAD)
                {
                    challengeType = currentLevelProfile.currentChallengeGroup.GetRandomChallenge();
                }
            }
            GridLayer gridLayer = null;
            switch (challengeType)
            {
                case ChallengeType.NOTHREAT:
                    gridLayer = new BasicMixLayer(grid,currentLevelProfile);
                    break;
                case ChallengeType.GUARD:
                case ChallengeType.DOG:
                case ChallengeType.SPIDER:
                case ChallengeType.PORCUPINE:
                    gridLayer = new BasicEnemyLayer(grid, currentLevelProfile,challengeType);
                    break;
                case ChallengeType.NURSE:
                    gridLayer = new DragonsLayer(grid, currentLevelProfile);
                    break;
                case ChallengeType.SMASHER:
                    gridLayer = new RowSmasherLayer(grid, currentLevelProfile);
                    break;
                case ChallengeType.FIREHEAD:
                    gridLayer = new FireHeadLayer(grid, currentLevelProfile);
                    break;
                case ChallengeType.SPIKES:
                    gridLayer = new SpikyLayer(grid, currentLevelProfile);
                    break;
                default:
                    Debug.LogError("Unexpected Situation");
                    gridLayer = new BasicMixLayer(grid, currentLevelProfile);
                    break;
            }
            activeRowBudget -= gridLayer.rows.Count;
        }
#region must be done before transition layer is created
        int butterflyBudget = currentLevelProfile.butterflyCount;
        while (butterflyBudget > 0)
        {
            IGridLocus ig = grid.possibleSpwnPoints[Random.Range(0, grid.possibleSpwnPoints.Count)];
            ig.SetSpwnPrefab(assets.butterFly);
            grid.possibleSpwnPoints.Remove(ig);
            butterflyBudget--;
        }
#endregion
        new TransitionLayer(grid, currentLevelProfile, currentLevelProfile.transitionRows,currentLevelNumber+1);

        VisualBaseTerrain.ManageVBTBackgrounds();

    }

    public static void Play_DefaultBlockParticle(IGridLocus igl, Dir dir)
    {
        if (instance.disableParticles) return;
        Quaternion q = Quaternion.identity;
        switch (dir)
        {
            case Dir.UP:
                q = Quaternion.Euler(45, 0, 0);
                break;
            case Dir.DOWN:
                q = Quaternion.Euler(-45, 0, 0);
                break;
            case Dir.LEFT:
                q = Quaternion.Euler(0, 45, 0);
                break;
            case Dir.RIGHT:
                q = Quaternion.Euler(0, -45, 0);
                break;
            default:
                break;
        }
        instance.assets.blockBreakDefaultParticle.Play(igl.position + new Vector3(0, 0, -2.5f), q, 4);
    }
    public static void Play_DefaultConcreteHitSparkParticle(IGridLocus igl, Dir dir)
    {
        if (instance.disableParticles) return;
        Quaternion q = Quaternion.identity;
        Vector3 v = Vector3.zero;
        switch (dir)
        {
            case Dir.UP:
                q = Quaternion.Euler(0,0,180);
                v = new Vector3(0, -0.35f * gridUnitDimension, 0);
                break;
            case Dir.DOWN:
                q = Quaternion.Euler(0, 0, 0);
                v = new Vector3(0, 0.35f * gridUnitDimension, 0);
                break;
            case Dir.LEFT:
                q = Quaternion.Euler(0, 0, -90);
                v = new Vector3(0.35f * gridUnitDimension, 0, 0);
                break;
            case Dir.RIGHT:
                q = Quaternion.Euler(0, 0, 90);
                v = new Vector3(-0.35f * gridUnitDimension, 0,0);
                break;
            default:
                break;
        }
        instance.assets.concreteHitSparkDefaultParticle.Play(igl.position+v,q,4);
    }
    public static void Play_DefaultEnemyHitParticle(ICharacter ichar)
    {
        if (instance.disableParticles) return;
        instance.assets.characterHitDefaultParticle.Play(ichar.GetGO().transform.position + new Vector3(0, 0.5f, -2.5f), Quaternion.identity, 4);
    }
    public static void Play_SpecificParticleOnGridElement(IGridLocus igl, GameObject prefab)
    {
        if (instance.disableParticles) return;
        ParticlePlayer.PlaySpecific(prefab, false ,igl.position + new Vector3(0, 0, -2.5f), Quaternion.identity, 4);
    }

    public static void Show_ScoreEarned(Vector3 position, int score, IAttacker attacker)
    {
        if (attacker == null) return;
        if (attacker as HayCart) return;
        if (score == 0) return;
        ScoreFlyCanvasScript.ScoreFlyType sft = ScoreFlyCanvasScript.ScoreFlyType.ENEMY;
        if (attacker == PlayerManager.playerAttacker) sft = ScoreFlyCanvasScript.ScoreFlyType.PLAYER;
        Pool.Instantiate(instance.assets.deathPointsCanvas, position + new Vector3(0,0.5f,0), Quaternion.identity).GetComponent<ScoreFlyCanvasScript>().Init(sft, score);
    }
    public static void Show_ComboScoreEarned(int score)
    {
        if (score == 0) return;
        ScoreFlyCanvasScript.ScoreFlyType sft = ScoreFlyCanvasScript.ScoreFlyType.PLAYER_COMBO;
        Pool.Instantiate(instance.assets.deathPointsCanvas, PlayerManager.instance.transform.position + new Vector3(0, 0.5f, 0), Quaternion.identity).GetComponent<ScoreFlyCanvasScript>().Init(sft, score);
    }


    //public static void Play_SpecificParticleOnGridElement(IGridLocus igl, GameObject prefab)
    //{
    //    ParticlePlayer.PlaySpecific(prefab, false, igl.position + new Vector3(0, 0, -2.5f), Quaternion.identity, 4);
    //}
}



