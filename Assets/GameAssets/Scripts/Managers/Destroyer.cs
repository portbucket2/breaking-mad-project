﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {
    private void OnTriggerEnter(Collider other)
    {
        IDestructible reusable = other.GetComponentInParent<IDestructible>();
        if (reusable != null)
        {
            reusable.SelfDestruct();
        }
        else
        {
            Debug.LogErrorFormat("Non reusble element found {0}", other.name);
        }

    }
}
