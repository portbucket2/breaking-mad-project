﻿#define UseLog
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyCloudManager : MonoBehaviour {

    [System.Serializable]
    private class CloudRelatedObjRef
    {
        public Cloud cloudScr;
        [HideInInspector]
        public Transform _transform;
        [HideInInspector]
        public float initX;
        [HideInInspector]
        public GameObject _gameObject;
        public Transform[] spawnPosList;
    }
    
    [SerializeField] List<CloudRelatedObjRef> allClouds;
    [SerializeField] Transform cameraHolder;
    [SerializeField] float distanceFromCameraWhenRespawnShouldHappen = 50f;
    [SerializeField] float respawnOffsetFromCamera = 15f;
    [SerializeField] bool useLog = false;
    void Start()
    {
        foreach (var item in allClouds)
        {
            item._transform = item.cloudScr.transform;
            item.initX = item._transform.position.x;
            item._gameObject = item.cloudScr.transform.gameObject;
            item._gameObject.SetActive(item.cloudScr.willShowAtFirst);
        }
        
    }
    [SerializeField] float[] diffList = new float[4]; 
    void Update()
    {
        for(int i = 0;i<allClouds.Count ;i++)
        {
            float diff = Mathf.Abs(allClouds[i]._transform.position.y - cameraHolder.position.y);
            diffList[i] = diff;
            //Debug.LogWarning("diff is: " + diff + " for cloud named: " + allClouds[i]._gameObject.name);
            //if(useLog)
                //CM_Deb("dist: " + diff + " for cloud: " + allClouds[i]._transform.gameObject.name);
            if ( diff > distanceFromCameraWhenRespawnShouldHappen)
            {
                    //Debug.Log("current diff is: " + diff + " which is greater " +
                    //    "than threshold distance, so the cloud named: " + allClouds[i]._gameObject.name + " will spawn!");
                if (useLog)
                {
                    ////CM_Deb"<color=red>recylc hit for cloud: " + allClouds[i]._transform.gameObject.name + "</color>");
                    //Debug.Break();
                }
                Recycle(allClouds[i]);
            }
        }
    }

    void Recycle(CloudRelatedObjRef cloud)
    {
        Vector3 curPos = cloud._transform.position;
        curPos.x = cloud.initX;
        int selectedLane = Random.Range(0, cloud.spawnPosList.Length);

        //if the lane is free or cloud is after middle way
        if (allClouds[selectedLane].cloudScr.isAfterMiddle || allClouds[selectedLane].cloudScr.isActiveOnLine == false)
        {
            curPos.x = cloud.spawnPosList[selectedLane].position.x;
        }
        
        curPos.y = (cameraHolder.position.y - respawnOffsetFromCamera);
        cloud._transform.position = curPos;
        cloud.cloudScr.SetTargetY(curPos.y);
        cloud._transform.localScale = Random.Range(0, 2) == 0 ? new Vector3(1, 1, 1) : new Vector3(-1, 1, 1);

        if(cloud._gameObject.activeInHierarchy == false)
            cloud._gameObject.SetActive(true);
    }
}