﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyColorManager : MonoBehaviour {
    public List<Color> colorList;
    public float span = 30;
    public Transform target;
    public Material mat;
    // Update is called once per frame

    int currentColorIndex = -1;
    int nextColorIndex = -1;
    Vector3 initialPosition;
    float currentYDistance { get { return Mathf.Abs(target.position.y - initialPosition.y); } }
    float offsetY;
    private void Start()
    {
        initialPosition = target.position;
        offsetY = 0;
        PerformColorRotation();
        PerformColorRotation();
        mat.color = colorList[currentColorIndex];
    }


    void Update ()
    {
        float colorFraction =  (currentYDistance - offsetY) /span;

        if (colorFraction > 1)
        {
            offsetY += span;
            PerformColorRotation();
            colorFraction = (currentYDistance - offsetY) / span;
        }
        mat.color = Color.Lerp(colorList[currentColorIndex],colorList[nextColorIndex],colorFraction);
	
	}



    void PerformColorRotation()
    {
        int r;
        do
        {
            r = Random.Range(0, colorList.Count);
        }
        while (r == currentColorIndex || r== nextColorIndex);

        currentColorIndex = nextColorIndex;
        nextColorIndex = r;
    }
}
