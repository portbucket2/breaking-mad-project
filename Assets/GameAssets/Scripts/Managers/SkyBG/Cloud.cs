﻿#define UseLog
//#undef UseLog

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.NativePlugins;

/// <summary>
/// Is there any curve feature like in particle system for unity?
/// Need it. Define the curve in editor and set it into a thing, 
/// so that value will be modifier in time according to the curve
/// </summary>
/// 
public class Cloud : MonoBehaviour
{
    [SerializeField]
    float travelDistanceInX = 40f, normalSpeed = 2.5f,
        increasedSpeed = 7f, acceleration = 1.4f, deceleration = 2.5f;
    [Range(0f, 5f)]
    [SerializeField]
    float accelerationHappenTimeMinAfterSpawn = 0.5f, accelerationHappenTimeMaxAfterSpawn = 2.5f,
        decelerationHappenTimeMinAfterSpawn = 0.5f, decelerationHappenTimeMaxAfterSpawn = 2.5f;

    [SerializeField] bool isDynamic = false;
    Transform _transform;
    Vector3 target = new Vector3();
    float currentSpeed;
    [SerializeField] bool canAccelerate = false, canDecelerate = false;
    public bool willShowAtFirst = false;
    bool willRunAccTimer = false, willRunDccTimer = false;
    float timerAcc, waitTimeAcc, timerDcc, waitTimeDcc;
    float u_acc, u_dcc;
    float acc_time, dcc_time;
    bool accing, dccing;
    public bool isActiveOnLine { get; private set; }
    public bool isAfterMiddle { get; private set; }
    Vector3 initPos;
    void Awake()
    {
        _transform = transform;
        initPos = _transform.position;
    }

    void Start()
    {
        InitCloud();
    }

    float debugStartTime, debugEndTime;
    void OnEnable()
    {
        InitCloud();
        debugStartTime = Time.time;
        isActiveOnLine = true;
        isAfterMiddle = false;
    }

    void OnDisable()
    {
        debugEndTime = Time.time;
        ////CM_Deb"<color=red>total cloud time is: " + (debugEndTime - debugStartTime) + "</color>");
        isActiveOnLine = false;
        isAfterMiddle = true;
    }

    void InitCloud()
    {
        target = _transform.position;
        target.x = travelDistanceInX;
        currentSpeed = normalSpeed;
        timerAcc = timerDcc = u_acc = u_dcc = acc_time = dcc_time = 0f;
        waitTimeAcc = Random.Range(accelerationHappenTimeMinAfterSpawn, accelerationHappenTimeMaxAfterSpawn);
        waitTimeDcc = Random.Range(decelerationHappenTimeMinAfterSpawn, decelerationHappenTimeMaxAfterSpawn);
        willRunAccTimer = canAccelerate;
        willRunDccTimer = canDecelerate;
        accing = dccing = false;
    }

    void Update()
    {
        if (isDynamic == false) return;
        //first lets run the timer to see if we are eligible to aceleration
        //so run the timer and wait for proper time
        if (willRunAccTimer)
        {
            timerAcc += Time.deltaTime;
            if (timerAcc > waitTimeAcc)
            {
                accing = true;
#if UseLog
                ////CM_Deb"<color=red>will accelerate" + gameObject.name + "</color>");
#endif
                willRunAccTimer = false;
                u_acc = currentSpeed;
                timerAcc = 0;
                acc_time = 0;
            }
        }

        //same thing for deceleration
        if (willRunDccTimer)
        {
            timerDcc += Time.deltaTime;
            if (timerDcc > waitTimeDcc)
            {
                dccing = true;
#if UseLog
                ////CM_Deb"<color=red>will dccelerate" + gameObject.name + "</color>");
#endif
                willRunDccTimer = false;
                u_dcc = currentSpeed;
                timerDcc = 0;
                dcc_time = 0;
            }
        }

        //if we do not set acc and dcc param correctly then it is possible that at the same time we will accelerate and decelerate
        //can't let it happen. Prioratize acceleration
        if (accing && dccing)
        {
            accing = true;
            dccing = false;
        }

        //so now its time for acceleration
        if (accing)
        {
            acc_time += Time.deltaTime;
            currentSpeed += acceleration * acc_time;//v = u + a*t
            if (currentSpeed > increasedSpeed)
            {
                currentSpeed = increasedSpeed;
                accing = false;
                acc_time = 0;
            }
        }

        //now its time for deceleration
        if (dccing)
        {
            dcc_time += Time.deltaTime;
            currentSpeed -= deceleration * dcc_time;
            if (currentSpeed < normalSpeed)
            {
                currentSpeed = normalSpeed;
                dccing = false;
                dcc_time = 0;
            }
        }

        //just in case if we have bug in the above code, this will make sure
        //that range of speed.
        currentSpeed = Mathf.Clamp(currentSpeed, normalSpeed, increasedSpeed);
        _transform.position = Vector3.MoveTowards(_transform.position, target, currentSpeed * Time.deltaTime);

        Vector3 nr_target = target;
        nr_target.y = nr_target.z = 0;
        Vector3 nr_init = initPos;
        nr_init.y = nr_init.z = 0;
        Vector3 nr_now = _transform.position;
        nr_now.y = nr_now.z = 0;

        float fLength = Vector3.Distance(nr_target, nr_init);
        float fNow = Vector3.Distance(nr_init, nr_now);
        if (fNow > (fLength * 0.5f))
        {
            isAfterMiddle = true;
        }
        if (Vector3.Distance(_transform.position, target) < 0.01f)
        {
            gameObject.SetActive(false);
        }
    }

    
    public void SetTargetY(float yValue)
    {
        target.y = yValue;
    }
}