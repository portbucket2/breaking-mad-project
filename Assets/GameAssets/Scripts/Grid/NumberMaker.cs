﻿using UnityEngine;
using System.Collections;

public class NumberMaker
{
    public static GameObject[][] CreateNumberPrefabArray(int N, LevelAssets assets)
    {
        //if (cols < 5) { Debug.LogError("not enoguh cols to show number"); cols = 5; }

        GameObject[][] prefabRefs = new GameObject[5][];
        GameObject X = assets.clayTerrain;
        GameObject _ = assets.blankTerrain;

        switch (N)
        {
            default:
                prefabRefs[0] = new GameObject[] { _, _, _, _, _ };
                prefabRefs[1] = new GameObject[] { _, _, _, _, _ };
                prefabRefs[2] = new GameObject[] { _, _, _, _, _ };
                prefabRefs[3] = new GameObject[] { _, _, _, _, _ };
                prefabRefs[4] = new GameObject[] { _, _, _, _, _ };
                break;

            case 0:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[3] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 1:
                prefabRefs[0] = new GameObject[] { _, _, X, _, _ };
                prefabRefs[1] = new GameObject[] { _, _, X, _, _ };
                prefabRefs[2] = new GameObject[] { _, _, X, _, _ };
                prefabRefs[3] = new GameObject[] { _, _, X, _, _ };
                prefabRefs[4] = new GameObject[] { _, _, X, _, _ };
                break;
            case 2:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, X, _, _, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 3:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 4:
                prefabRefs[0] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, _, _, X, _ };
                break;
            case 5:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, _, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 6:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, _, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 7:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[3] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, _, _, X, _ };
                break;
            case 8:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 9:
                prefabRefs[0] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, X, X, _ };
                prefabRefs[3] = new GameObject[] { _, _, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, X, X, _ };
                break;
            case 10:
                prefabRefs[0] = new GameObject[] { X, _, X, X, X};
                prefabRefs[1] = new GameObject[] { X, _, X, _, X};
                prefabRefs[2] = new GameObject[] { X, _, X, _, X};
                prefabRefs[3] = new GameObject[] { X, _, X, _, X};
                prefabRefs[4] = new GameObject[] { X, _, X, X, X};
                break;
            case 11:
                prefabRefs[0] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[1] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[2] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[3] = new GameObject[] { _, X, _, X, _ };
                prefabRefs[4] = new GameObject[] { _, X, _, X, _ };
                break;
            case 12:
                prefabRefs[0] = new GameObject[] { X, _, X, X, X };
                prefabRefs[1] = new GameObject[] { X, _, _, _, X };
                prefabRefs[2] = new GameObject[] { X, _, X, X, X };
                prefabRefs[3] = new GameObject[] { X, _, X, _, _ };
                prefabRefs[4] = new GameObject[] { X, _, X, X, X };
                break;
        }


        return prefabRefs;
    }
}
