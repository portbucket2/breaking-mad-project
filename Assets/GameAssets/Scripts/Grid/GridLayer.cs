﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FishSpace;

namespace TowerGrid
{

}
public class GridLayer
{
    public float dimension;
    public GridMaster grid;
    public List<GridRow> rows = new List<GridRow>();
    public GridRow lastRow
    {
        get
        {
            if (rows.Count == 0) return grid.lastCreatedRow;
            else return rows[rows.Count - 1];
        }
    }
    public Vector3 nextSpwnPosition
    {
        get
        {
            Vector3 retVal;
            if (rows.Count == 0)
            {
                retVal = grid.nextSpwnPosition;
            }
            else
            {
                retVal = rows[rows.Count-1].position + grid.defaultOffset;
            }
            //Debug.Log(retVal);
            return retVal;
        }
    }
    public LevelProfile levelProfile;

    public GridLayer(GridMaster grid, LevelProfile levelProfile)
    {
        grid.runningLevelProfile = levelProfile;
        dimension = grid.gridUnitDimension;
        this.grid = grid;
        this.levelProfile = levelProfile;
    }

    protected void RegisterBlock(bool addRandomElements = true)
    {
        //rows[0].prevRow = grid.lastCreatedRow;
        //if (grid.lastCreatedRow != null) grid.lastCreatedRow.nextRow = rows[0];
        //grid.lastCreatedRow = rows[rows.Count - 1];
        if (addRandomElements)
        {

            for (int r = 0; r < rows.Count; r++)
            {
                for (int c = 1; c <= rows[r].activeCols; c++)
                {
                    IGridLocus current = rows[r].locationary[c - rows[r].posIndexOffset];
                    if (!current.IsBlank())
                    {
                        IGridLocus uploc = current.relativeLocus(Dir.UP);
                        if (uploc != null && uploc.IsBlank())
                        {
                            AddRandomElement(uploc);
                        }
                    }
                }
            }
        }
    }


    private void AddRandomElement(IGridLocus igl)
    {
        GameObject spwnPref = null;
        float enemyChance = Random.Range(0, 1.0f);
        //Debug.LogFormat("E :{0} / {1}", enemyChance, levelProfile.randomEnemySpwnChance);
        if (levelProfile.currentChallengeGroup.canSpawnRandomEnemy && enemyChance < levelProfile.GetRandomEnemyChance())
        {
            spwnPref = levelProfile.currentChallengeGroup.GetRandomEnemy();
        }
        if (spwnPref == null)
        {
            float haychance = Random.Range(0, 1.0f);
            //Debug.LogFormat("H :{0} / {1}", haychance, levelProfile.hayCartSpwnChance);
            if (haychance < levelProfile.hayCartSpwnChance)
            {
                spwnPref = grid.assets.haycart;
            }
        }

        if (spwnPref != null)
        {
            if (grid.possibleSpwnPoints.Contains(igl))
            {
                grid.possibleSpwnPoints.Remove(igl);
            }
            igl.SetSpwnPrefab(spwnPref);
        }

    }

    public GridRow AddNewBlankRow(int activecols,bool noWindow)
    {
        GridRow thisrow = new GridRow(activecols+2,grid.rowCount++,grid,noWindow);



        thisrow.dimension = dimension;
        thisrow.position = nextSpwnPosition;

        thisrow.prevRow = lastRow;
        if (lastRow != null)
        {
            lastRow.nextRow = thisrow;
        }
        rows.Add(thisrow);//changes block last row
        grid.lastCreatedRow = thisrow;
        return thisrow;
    }
}













public enum GridLayerType
{
    UNSET =0,
    TRANSITION = 10,
    FIREHEAD_LAYER = 100,
    ROWSMASH_LAYER = 200,
    BASIC_MIX = 1000,
    SPIKY_LAYER = 2000,
    BASIC_ENEMY = 3000,
    DRAGON_ENEMY = 3100,
}