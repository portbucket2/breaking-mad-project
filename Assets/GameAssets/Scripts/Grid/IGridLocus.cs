﻿using UnityEngine;
public interface IGridLocus : IDestructible
{
    GridRow row { get; set; }
    int rowOffset { get; set; }
    Vector3 position { get; }
    //Vector3 standingPosition { get; }
    //IGridLocus leftLocus { get; }
    //IGridLocus rightLocus { get; }
    //IGridLocus upLocus { get; }
    //IGridLocus downLocus { get; }
    bool IsUnbreakable();
    bool IsBlank();
    bool IsPartOfPassiveArea();
    void CheckAndLoadLevelIfNeeded();
    void Init(GridRow row, int offset, float dimension, int needsLevel,GameObject spwnPrefab,bool isAreaPassive);
    void SetSpwnPrefab(GameObject spwnPrefab);
    void Replace(IGridLocus original);

    IGridLocus relativeLocus(Dir dir, int additionalIteration = 0);
    Vector3 StandingPosition(Dir agentRaycastDir);
}
