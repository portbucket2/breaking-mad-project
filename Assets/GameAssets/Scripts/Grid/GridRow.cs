﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

[System.Serializable]
public class GridRow
{
    private bool noWindowAllowed;
    public int rowID;
    public GridMaster grid;
    [System.NonSerialized] public GridRow prevRow;
    [System.NonSerialized] public GridRow nextRow; 
    public Vector3 position;
    public float dimension;

    public Dictionary<int, IGridLocus> locationary = new Dictionary<int, IGridLocus>();
    public IGridLocus GetLocus(int offset)
    {
        if (locationary.ContainsKey(offset))
            return locationary[offset];
        else return null;
    }
    public bool hasPath
    {
        get
        {
            foreach (var item in locationary)
            {
                if (!item.Value.IsUnbreakable()) return true;
            }
            return false;
        }
    }
    public IGridLocus centreItem
    {
        get
        {
            if (locationary.Count == 0)
            {
                return null;
            }
            else
            {
                return locationary[0];
            }
        }
    }
    public Vector3 centreItemPosition
    {
        get
        {
            if (centreItem != null)
                return centreItem.position;
            else return position;
        }
    }

    public GridRow(int totalCols,int id, GridMaster grid, bool noWindow)
    {
        noWindowAllowed = noWindow;
        this.grid = grid;
        this.totalCols  = totalCols;
        rowID = id;
    }
    public int totalCols;
    public int activeCols
    {
        get { return totalCols - 2; }
    }
    public int posIndexOffset
    {
        get
        {
            return (totalCols - 1) / 2;
        }
    }

    private static int counter;
    public void Feed(GameObject prefab, int index, int nextLevelNo = 0, GameObject enemyPrefab = null, bool passiveArea = false)
    {
        if ((index == 0)|| (index == totalCols - 1))
        {
            float rv = Random.Range(0, 1.0f);
            if (rv < grid.runningLevelProfile.GetWindowChance() && !noWindowAllowed)
            {
                prefab = grid.assets.wallWindow;
            }
            else
            {
                prefab = grid.assets.wallDefault;
            }
        }
        GameObject go = Pool.Instantiate(prefab);
        //GameObject go = MonoBehaviour.Instantiate(prefab) as GameObject;
        IGridLocus gl = go.GetComponent<IGridLocus>();
        ////CM_Deb(gl.gameObject.name);
        ////CM_Deb(gl.isBlockage);
        go.transform.SetParent(grid.rootTransform);
        //if (gl == null) //CM_Deb(prefab);
        gl.Init(this, index - posIndexOffset, grid.gridUnitDimension, nextLevelNo,enemyPrefab,passiveArea);
        //go.name = (counter++).ToString();
    }

    //public static GridRow CreateRandomRow(GridMaster grid, int activeCols, ChanceKeeper active)
    //{
    //    GridRow thisrow = new GridRow();



    //    thisrow.dimension = grid.gridUnitDimension;
    //    thisrow.position = grid.nextSpwnPosition;

    //    if (grid.lastCreatedRow != null)
    //    {
    //        thisrow.prevRow = grid.lastCreatedRow;
    //        grid.lastCreatedRow.nextRow = thisrow;
    //    }
    //    grid.lastCreatedRow = thisrow;

        

    //    int totalCols = activeCols + 2;
    //    int posIndexOffset = (totalCols - 1) / 2;

    //    bool noPath = true;

    //    GameObject preab = null;
    //    for (int i = 0; i < totalCols; i++)
    //    {
    //        if (i == 0)
    //            preab = grid.leftWallOptions.Roll();
    //        else if (i == totalCols - 1)
    //            preab = grid.rightWallOptions.Roll();
    //        else
    //        {
    //            preab = active.Roll();
    //        }

    //        GameObject go = MonoBehaviour.Instantiate(preab);
    //        IGridLocus gl = go.GetComponent<IGridLocus>();
    //        ////CM_Deb(gl.gameObject.name);
    //        ////CM_Deb(gl.isBlockage);
    //        go.transform.SetParent(grid.rootTransform);
    //        gl.Init(thisrow, i - posIndexOffset, grid.gridUnitDimension);
    //        if (!gl.IsBlocked()) noPath = false;
    //    }
    //    if (noPath)
    //    {
    //        int rIndex = Random.Range(1, totalCols - 1);
    //        int rOffset = rIndex - posIndexOffset;
    //        bool isOkay = false;
    //        while (!isOkay)
    //        {
    //            preab = active.Roll();
    //            isOkay = !preab.GetComponent<IGridLocus>().IsBlocked();
    //        }
    //        IGridLocus original = thisrow.GetLocus(rOffset);
    //        GameObject go = MonoBehaviour.Instantiate(preab);
    //        go.transform.SetParent(grid.rootTransform);
    //        IGridLocus gl = go.GetComponent<IGridLocus>();
    //        gl.Replace(original);
    //        original.Destroy();
    //        //CM_Deb("A block was discarded");
    //    }
    //    else
    //    {
    //        //CM_Deb("has path");
    //    }

    //    return thisrow;

    //}


   


}

