﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridPropagatingExplosive
{
    public List<List<IGridLocus>> nodes = new List<List<IGridLocus>>();

    public GridPropagatingExplosive(IGridLocus startGrid, int propagationCount)
    {
        for (int i = 0; i < propagationCount+1; i++)
        {
            nodes.Add(new List<IGridLocus>());
        }
        TryAddNode(startGrid,0);
        ////CM_Deb"iteration count: {0}", iterationCount);
    }
    int iterationCount;
    private void TryAddNode(IGridLocus g, int propagationIndex)
    {
        iterationCount++;
        if (g == null) return;
        if (!Contains(g) && !g.IsUnbreakable())
        {
            if (propagationIndex >= 0 && propagationIndex < nodes.Count)
            {
                nodes[propagationIndex].Add(g);//first element will not be added since called with -1
            }
            propagationIndex++;
////CM_Deb"PI : {0}", propagationIndex);
            if (g.IsBlank() && propagationIndex < nodes.Count)
            {
                TryAddNode(g.relativeLocus(Dir.UP), propagationIndex);
                TryAddNode(g.relativeLocus(Dir.DOWN), propagationIndex);
                TryAddNode(g.relativeLocus(Dir.LEFT), propagationIndex);
                TryAddNode(g.relativeLocus(Dir.RIGHT), propagationIndex);
            }
        }  
    }

    private bool Contains(IGridLocus g)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[0].Contains(g)) return true;
        }
        return false;
    }
    //private void ArtbitateAmongstLists(int propCount)
    //{

    //}
}
