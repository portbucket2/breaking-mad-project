﻿using UnityEngine;
using System.Collections;

public class TransitionLayer : GridLayer
{
    public TransitionLayer(GridMaster grid, LevelProfile levelPro, int rowCount, int nextLevelNo) : base(grid,levelPro)
    {
        int triggerRowIndex = 4;
        if (rowCount < triggerRowIndex - 1) triggerRowIndex = 0;



        int activeCols = levelPro.colCount;
        GridRow row;
        for (int r = 0; r < rowCount; r++)
        {
            row = AddNewBlankRow(activeCols, noWindow: false);
            for (int i = 0; i < row.totalCols; i++)
            {
                
                row.Feed(grid.assets.blankTerrain, i, (r > triggerRowIndex)?nextLevelNo:-1);

            }
        }
        RegisterBlock();
    }
}
