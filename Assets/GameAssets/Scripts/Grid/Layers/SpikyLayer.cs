﻿using UnityEngine;
using System.Collections;
using FishSpace;

public class SpikyLayer : GridLayer
{

    public SpikyLayer(GridMaster grid, LevelProfile levelPro) : base(grid,levelPro)
    {
        int activeCols = levelPro.colCount;
        GridRow row;
        row = AddNewBlankRow(activeCols,noWindow:false);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.blankTerrain, i);
        }
        int randomIndex = Random.Range(0, activeCols) + 1;
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            GameObject pref = grid.spikeTopTerrainOpts.Roll();
            while (i != randomIndex && pref == grid.assets.blankTerrain)
            {
                pref = grid.spikeTopTerrainOpts.Roll();
            }
            row.Feed(pref, i);
        }
        row = AddNewBlankRow(activeCols, noWindow: false);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == randomIndex)
                row.Feed(grid.assets.blankTerrain, i);
            else
                row.Feed(grid.assets.spikeTerrain, i);
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == randomIndex)
                row.Feed(grid.spikeTopTerrainOpts.Roll(), i);
            else
                row.Feed(grid.assets.stoneTerrain, i);
        }

        RegisterBlock();
    }
     

}