﻿using UnityEngine;
using System.Collections;
using FishSpace;
public class BasicEnemyLayer : GridLayer
{
    public BasicEnemyLayer(GridMaster grid, LevelProfile levelPro, ChallengeType challengeType ) : base(grid,levelPro)
    {
        NewInit(grid,levelPro,challengeType);
    }


    public void NewInit(GridMaster grid, LevelProfile levelPro, ChallengeType challengeType)
    {
        int activeCols = levelPro.colCount;
        GridRow row;
        row = AddNewBlankRow(activeCols, noWindow: false);
        int randomIndex = Random.Range(0, activeCols) + 1;//+1 added to convert to actual index(not only active)
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == randomIndex)
            {
                row.Feed(grid.assets.blankTerrain, i, enemyPrefab: levelPro.currentChallengeGroup.GetEnemyForChallengeType(challengeType));
            }
            else
            {
                row.Feed(grid.assets.blankTerrain, i);
            }
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            GameObject pref = grid.rockyTerrainOpts.Roll();
            if (i != randomIndex)
            {
                pref = grid.rockyTerrainOpts.Roll();
            }
            else
            {
                while (pref == grid.assets.blankTerrain)
                {
                    pref = grid.rockyTerrainOpts.Roll();
                }
            }

            row.Feed(pref, i);
        }
        if (!row.hasPath)
        {
            int clearanceIndex = randomIndex;//so that it atleast randomises once
            do
            {
                clearanceIndex = Random.Range(1, row.totalCols - 1);
            }
            while (clearanceIndex == randomIndex);
            int rOffset = clearanceIndex - row.posIndexOffset;
            bool isOkay = false;
            GameObject pref = null;
            while (!isOkay)
            {
                pref = grid.rockyTerrainOpts.Roll();
                isOkay = !pref.GetComponent<IGridLocus>().IsUnbreakable();
            }
            IGridLocus original = row.GetLocus(rOffset);
            GameObject go = MonoBehaviour.Instantiate(pref);
            go.transform.SetParent(grid.rootTransform);
            IGridLocus gl = go.GetComponent<IGridLocus>();
            gl.Replace(original);
            original.SelfDestruct();
            ////CM_Deb"A block was discarded");
        }

        RegisterBlock();
    }

    public void OldInit(GridMaster grid, LevelProfile levelPro, ChallengeType challengeType)
    {
        int activeCols = levelPro.colCount;
        GridRow row;
        row = AddNewBlankRow(activeCols, noWindow: false);
        int randomIndex = Random.Range(0, activeCols) + 1;//+1 added to convert to actual index(not only active)
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == randomIndex)
            {
                row.Feed(grid.assets.blankTerrain, i, enemyPrefab: levelPro.currentChallengeGroup.GetEnemyForChallengeType(challengeType));
            }
            else
            {
                row.Feed(grid.basicTerrainOpts.Roll(), i);
            }
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            GameObject pref = grid.rockyTerrainOpts.Roll();
            if (i != randomIndex)
            {
                pref = grid.rockyTerrainOpts.Roll();
            }
            else
            {
                while (pref == grid.assets.blankTerrain)
                {
                    pref = grid.rockyTerrainOpts.Roll();
                }
            }

            row.Feed(pref, i);
        }
        if (!row.hasPath)
        {
            int clearanceIndex = randomIndex;//so that it atleast randomises once
            do
            {
                clearanceIndex = Random.Range(1, row.totalCols - 1);
            }
            while (clearanceIndex == randomIndex);
            int rOffset = clearanceIndex - row.posIndexOffset;
            bool isOkay = false;
            GameObject pref = null;
            while (!isOkay)
            {
                pref = grid.rockyTerrainOpts.Roll();
                isOkay = !pref.GetComponent<IGridLocus>().IsUnbreakable();
            }
            IGridLocus original = row.GetLocus(rOffset);
            GameObject go = MonoBehaviour.Instantiate(pref);
            go.transform.SetParent(grid.rootTransform);
            IGridLocus gl = go.GetComponent<IGridLocus>();
            gl.Replace(original);
            original.SelfDestruct();
            //CM_Deb"A block was discarded");
        }

        RegisterBlock();
    }
}
