﻿using UnityEngine;
using System.Collections;

public class TutorialLayer : GridLayer
{
    public TutorialLayer(GridMaster grid, LevelProfile levelPro, int activeCols) : base(grid, levelPro)
    {
        activeCount = activeCols;
        //int activeCols = levelPro.colCount;
        GameObject blank = grid.assets.blankTerrain;
        GameObject clay = grid.assets.clayTerrain;
        GameObject stone = grid.assets.stoneTerrain;


        GridRow row;
        row = AddNewBlankRow(activeCols + 2, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.blankTerrain, i, passiveArea: true);
        }

        row = AddNewBlankRow(activeCols + 2, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.blankTerrain, i, passiveArea: true);
        }


        row = AddNewBlankRow(activeCols + 2, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == 1 || i == row.totalCols - 2)
            {

                row.Feed(grid.assets.stoneTerrain, i, passiveArea: true);
            }
            else
            {
                row.Feed(grid.assets.clayTerrain, i, passiveArea: true);
            }
        }


        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.clayTerrain, i, passiveArea: true);
        }
        //AddRow(blank, blank, blank);
        //AddRow(blank, blank, blank);
        //AddRow(clay, clay, clay);
        //AddRow(clay, clay, clay);
        AddRow(clay, clay, clay);
        AddRow(stone, stone, clay);

        AddRow(blank, blank, blank);
        AddRow(blank, stone, stone);
        AddRow(blank, blank, blank,s2: grid.assets.butterFly);

        AddRow(stone, stone, clay);
        AddRow(blank, blank, clay, s2: grid.assets.enemyGuard);
        AddRow(blank, stone, stone);

        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank, s2: grid.assets.butterFly);
        AddRow(blank, blank, blank);
        AddRow(stone, stone, blank);

        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);
        AddRow(blank, blank, blank);

        RegisterBlock(addRandomElements: false);
    }

    public int activeCount;
    public void AddRow(GameObject p1, GameObject p2, GameObject p3, GameObject s1=null, GameObject s2 = null, GameObject s3 = null)
    {
        GridRow row;
        row = AddNewBlankRow(activeCount, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {

            switch (i)
            {
                case 1:
                    row.Feed(p1, i, passiveArea: true, enemyPrefab: s1);
                    break;
                case 2:
                    row.Feed(p2, i, passiveArea: true, enemyPrefab: s2);
                    break;
                case 3:
                    row.Feed(p3, i, passiveArea: true, enemyPrefab: s3);
                    break;
                default:
                    row.Feed(grid.assets.clayTerrain, i, passiveArea: true);
                    break;
            }
        }
    }
}
