﻿using UnityEngine;
using System.Collections;
using FishSpace;

public class ShopLayer : GridLayer
{



    public ShopLayer(GridMaster grid, LevelProfile levelPro) : base(grid,levelPro)
    {
        int lastColCount = grid.lastCreatedRow.activeCols;
        int activeCols = levelPro.colCount;

        GridRow row;

        if (lastColCount < activeCols)
        {
            row = AddNewBlankRow(activeCols, noWindow: true);
            for (int i = 0; i < row.totalCols; i++)
            {
                if (i == 1 || i == row.totalCols - 2)
                {

                    row.Feed(grid.assets.stoneTerrain, i, passiveArea:true);
                }
                else
                {
                    row.Feed(grid.assets.blankTerrain, i, passiveArea: true);
                }
            }
        }
        for (int i = 0; i < 2; i++)
        {
            row = AddNewBlankRow(activeCols, noWindow: true);
            for (int j = 0; j < row.totalCols; j++)
            {

                    row.Feed(grid.assets.blankTerrain, j, passiveArea: true);
            }
        }


        if (activeCols < 5) { Debug.LogError("not enough space to show number"); }
        else
        {
            GameObject[][] numFabs = NumberMaker.CreateNumberPrefabArray(LevelManager.instance.currentLevelNumber, grid.assets);
            int pad = (activeCols - 5) / 2;
            for (int i = 0; i < 5; i++)
            {
                GameObject[] rowFabs = numFabs[i];
                row = AddNewBlankRow(activeCols, noWindow: true);
                for (int j = 0; j < row.totalCols; j++)
                {
                    int prefabIndex = j - 1 - pad;
                    if (prefabIndex >= 0 && prefabIndex < rowFabs.Length)
                    {
                        row.Feed(rowFabs[prefabIndex], j, passiveArea: true);
                    }
                    else
                    {
                        row.Feed(grid.assets.blankTerrain, j, passiveArea: true);
                    }
                }
            }
        }


        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int j = 0; j < row.totalCols; j++)
        {

            row.Feed(grid.assets.blankTerrain, j, passiveArea: true);
        }
        row = AddNewBlankRow(activeCols, noWindow:true);
        int m = row.totalCols / 2;
        for (int j = 0; j < row.totalCols; j++)
        {
            if (Mathf.Abs(j - m) <= 1)
            {
                row.Feed(grid.assets.shopItemTerrain, j, passiveArea: true);
            }
            else
            {
                row.Feed(grid.assets.blankTerrain, j, passiveArea: true);
            }
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int j = 0; j < row.totalCols; j++)
        {

            row.Feed(grid.assets.blankTerrain, j, passiveArea: true);
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int j = 0; j < row.totalCols; j++)
        {

            row.Feed(grid.assets.clayTerrain, j, passiveArea: true);
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int j = 0; j < row.totalCols; j++)
        {

            row.Feed(grid.assets.clayTerrain, j, passiveArea: true);
        }
        //int randomIndex = Random.Range(0, activeCols) + 1;
        //row = AddNewBlankRow(activeCols);
        //for (int i = 0; i < row.totalCols; i++)
        //{
        //    GameObject pref = spikeCoverOptions.Roll();
        //    while (i != randomIndex && pref == grid.assets.blankTerrain)
        //    {
        //        pref = spikeCoverOptions.Roll();
        //    }
        //    if (i == randomIndex)
        //        row.Feed(pref, i);
        //    else
        //        row.Feed(pref, i);
        //}
        //row = AddNewBlankRow(activeCols);
        //for (int i = 0; i < row.totalCols; i++)
        //{
        //    if (i == randomIndex)
        //        row.Feed(grid.assets.blankTerrain, i);
        //    else
        //        row.Feed(grid.assets.spikeTerrain, i);
        //}
        //row = AddNewBlankRow(activeCols);
        //for (int i = 0; i < row.totalCols; i++)
        //{
        //    if (i == randomIndex)
        //        row.Feed(grid.assets.blankTerrain, i);
        //    else
        //        row.Feed(grid.assets.stoneTerrain, i);
        //}

        RegisterBlock(addRandomElements: false);
    }


}