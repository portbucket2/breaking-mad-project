﻿using UnityEngine;
using System.Collections;
using FishSpace;

public class FireHeadLayer : GridLayer
{
    public FireHeadLayer(GridMaster grid, LevelProfile levelPro) : base(grid,levelPro)
    {
        int activeCols = levelPro.colCount;
        int randomIndex = Random.Range(0, activeCols) + 1;
        GridRow row;
        row = AddNewBlankRow(activeCols, noWindow: false);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == randomIndex)
                row.Feed(grid.assets.fireHeadTerrain, i);
            else
                row.Feed(grid.assets.blankTerrain, i);
        }
        RegisterBlock();
    }
}
public class RowSmasherLayer : GridLayer
{
    public RowSmasherLayer(GridMaster grid, LevelProfile levelPro) : base(grid,levelPro)
    {
        int activeCols = levelPro.colCount;
        int randomIndex = Random.Range(0, activeCols) + 1;
        GridRow row;
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.basicTerrainOpts.Roll(), i);
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == randomIndex)
                row.Feed(grid.assets.rowSmasherTerrain, i);
            else
                row.Feed(grid.assets.blankTerrain, i);
        }
        RegisterBlock();
    }
}
//public class EmptyLayer : GridLayer
//{
//    public EmptyLayer(GridMaster grid, int activeCols, int rowCount) : base(grid,null)
//    {
//        //int activeCols = levelPro.colCount;
//        GridRow row;
//        for (int r = 0; r < rowCount; r++)
//        {
//            row = AddNewBlankRow(activeCols);
//            for (int i = 0; i < row.totalCols; i++)
//            {
//                row.Feed(grid.assets.blankTerrain, i);
//            }
//        }
//        RegisterBlock(addRandomElements: false);
//    }
//}

public class StartLayer : GridLayer
{
    public StartLayer(GridMaster grid, LevelProfile levelPro) : base(grid, levelPro)
    {
        int activeCols = levelPro.colCount;
        GridRow row;
        row = AddNewBlankRow(activeCols+2, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.blankTerrain, i, passiveArea: true);
        }

        row = AddNewBlankRow(activeCols+2, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.blankTerrain, i,passiveArea:true);
        }


        row = AddNewBlankRow(activeCols+2, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i == 1 || i == row.totalCols - 2)
            {

                row.Feed(grid.assets.stoneTerrain, i, passiveArea: true);
            }
            else
            {
                row.Feed(grid.assets.clayTerrain, i, passiveArea: true);
            }
        }


        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.assets.clayTerrain, i, passiveArea: true);
        }
        RegisterBlock(addRandomElements: false);
    }
}
public class ClayLayer : GridLayer
{
    public ClayLayer(GridMaster grid, LevelProfile levelPro, int rowCount) : base(grid, levelPro)
    {
        int activeCols = levelPro.colCount;
        GridRow row;
        for (int r = 0; r < rowCount; r++)
        {
            row = AddNewBlankRow(activeCols, noWindow: false);
            for (int i = 0; i < row.totalCols; i++)
            {
                row.Feed(grid.assets.clayTerrain, i);
            }
        }
        RegisterBlock(addRandomElements: false);
    }
}
public class BasicMixLayer : GridLayer
{
    public BasicMixLayer(GridMaster grid, LevelProfile levelPro) : base(grid,levelPro)
    {
        //ChanceKeeper[] optionsArray = new ChanceKeeper[] { grid.assets.basicRowOptions, grid.assets.rockyRowOptions };
        //int activeCols = levelPro.colCount;
        //for (int i = 0; i < optionsArray.Length; i++)
        //{
        //    AddRowToBlock(this, activeCols, optionsArray[i].clist);
        //}
        int activeCols = levelPro.colCount;

        AddRowToBlock(this, activeCols, grid.assets.basicRowOptions.clist, noWindow: false);
        AddRowToBlock(this, activeCols, grid.assets.rockyRowOptions.clist, noWindow: true);
        RegisterBlock();
    }
    public static void AddRowToBlock(GridLayer block, int activeCols, PseudoChancedList<GameObject> active, bool noWindow)
    {
        GridRow thisrow = block.AddNewBlankRow(activeCols,noWindow:noWindow);
        GameObject preab = null;
        for (int i = 0; i < thisrow.totalCols; i++)
        {
            preab = active.Roll();

            thisrow.Feed(preab, i);
        }
        if (!thisrow.hasPath)
        {
            int rIndex = Random.Range(1, thisrow.totalCols - 1);
            int rOffset = rIndex - thisrow.posIndexOffset;
            bool isOkay = false;
            while (!isOkay)
            {
                preab = active.Roll();
                isOkay = !preab.GetComponent<IGridLocus>().IsUnbreakable();
            }
            IGridLocus original = thisrow.GetLocus(rOffset);
            GameObject go = MonoBehaviour.Instantiate(preab);
            go.transform.SetParent(block.grid.rootTransform);
            IGridLocus gl = go.GetComponent<IGridLocus>();
            gl.Replace(original);
            original.SelfDestruct();
            ////CM_Deb"A block was discarded");
        }
        //else
        //{
        //    //CM_Deb"has path");
        //}


    }
}