﻿using UnityEngine;
using System.Collections;
using FishSpace;

public class DragonsLayer : GridLayer
{
    int[] sepIndexes = new int[] { 1, 2, 4, 6, 7 };

    public DragonsLayer(GridMaster grid, LevelProfile levelPro) : base(grid,levelPro)
    {
        int activeCols = levelPro.colCount;
        GridRow row;
        int enemyIndex = Random.Range(1, activeCols - 1) + 1;//+1 added to convert to actual index(not only active), 1 and -1 is in range to ignore the end blocks

        
        
        int seperatorIndex = sepIndexes[ Random.Range(0, sepIndexes.Length) ];
        //bool willSeperateAirSpace = (Random.Range(0,2)==0);

        row = AddNewBlankRow(activeCols, noWindow: false);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i!=enemyIndex && i == seperatorIndex)
            {
                row.Feed(grid.assets.clayTerrain, i);
            }
            else
            {
                row.Feed(grid.assets.blankTerrain, i);
            }
        }
        row = AddNewBlankRow(activeCols, noWindow: false);
        for (int i = 0; i < row.totalCols; i++)
        {
            if (i != enemyIndex)
            {
                row.Feed(grid.assets.blankTerrain, i);
            }
            else
            {
                row.Feed(grid.assets.blankTerrain, i, enemyPrefab: grid.assets.enemyDragon);
            }
        }
        row = AddNewBlankRow(activeCols, noWindow: true);
        for (int i = 0; i < row.totalCols; i++)
        {
            row.Feed(grid.rockyTerrainOpts.Roll(), i);
        }
        if (!row.hasPath)
        {
            int clearanceIndex = Random.Range(1, row.totalCols - 1);
            int rOffset = clearanceIndex - row.posIndexOffset;
            bool isOkay = false;
            GameObject pref = null;
            while (!isOkay)
            {
                pref = grid.rockyTerrainOpts.Roll();
                isOkay = !pref.GetComponent<IGridLocus>().IsUnbreakable();
            }
            IGridLocus original = row.GetLocus(rOffset);
            GameObject go = MonoBehaviour.Instantiate(pref);
            go.transform.SetParent(grid.rootTransform);
            IGridLocus gl = go.GetComponent<IGridLocus>();
            gl.Replace(original);
            original.SelfDestruct();
            //CM_Deb"A block was discarded");
        }

        RegisterBlock();
    }
}
