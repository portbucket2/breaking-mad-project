﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FishSpace;



public class GridMaster
{
    public LevelAssets assets;
    public ChanceKeeper rockyTerrainOpts { get { return assets.rockyRowOptions; } }
    public ChanceKeeper basicTerrainOpts { get { return assets.basicRowOptions; } }
    public ChanceKeeper spikeTopTerrainOpts { get { return assets.spikeCoverOptions; } }
    //public ChanceKeeper leftWallOptions;
    //public ChanceKeeper rightWallOptions;
    //public float windowChance = 0.1f;
    public float gridUnitDimension;
    public GridRow lastCreatedRow;
    public LevelProfile runningLevelProfile;
    public Vector3 direction;
    public Transform rootTransform;
    public Vector3 rootPosition;
    public Vector3 defaultOffset
    {
        get
        {
            return direction * gridUnitDimension;
        }
    }

    public Vector3 nextSpwnPosition {
        get
        {
            Vector3 retVal;
            if (lastCreatedRow == null)
            {
                retVal = rootPosition;
            }
            else
            {
                retVal = lastCreatedRow.position + defaultOffset;
            }
            ////CM_DebretVal);
            return retVal;

        }
    }

    public int rowCount = 0;

    public GridMaster(float dim, Transform rootTransform, Vector3 dir, LevelAssets ass)
    {
        assets = ass;
        //leftWallOptions = leftopt;
        //rightWallOptions = rightopt;
        this.rootTransform = rootTransform;
        this.rootPosition = rootTransform.position;// + new Vector3(0, -dim*0.5f, 0);
        ////CM_DebrootPosition);
        gridUnitDimension = dim;
        direction = dir;
        lastCreatedRow = null;
        //return this;
    }

    public List<IGridLocus> possibleSpwnPoints = new List<IGridLocus>();
}


