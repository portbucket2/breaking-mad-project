﻿using UnityEngine;
using System.Collections;

public interface IHitable
{
    GameObject GetGO();
    void TakeHit(Dir forceDirection, IAttacker attacker);
    float MaxHitDistance();
    int GetScoreOnHit();
    float xPosition { get; }
}

public interface ICharacter : IHitable
{
    bool logEnabled { get; }
    float GetPosXDifferenceFrom(ICharacter from);
    Dir GetRelativeDirectionFrom(ICharacter relativeTo);
    void Init();
    Collider togglingCollider_OnMove { get; }
    bool isDead { get; set; }
    bool isUntargetable { get; set; }
    bool canCrawlWalls { get; }
    bool canRetreat { get; }
    bool canFly { get; }
    MovementController GetMovementController();
}


public interface IAttacker
{
    string attackerid { get; }
    void AddScore(int score);
    float GetAttackPreparationTime();
    float GetAttackHittingTime();
    float GetAttackRange();
    void TriggerAttackEffects(Vector3 point);
    void NullifyThreat(IAttacker nullifier);
    float TerrainHitParticleDelay(Dir direction,bool unbreakable);
    float EnemyHitParticleDelay(Dir direction);
    

}
public interface IVerticalCollidable 
{
    void ResolveAsMasterEntity(IVerticalCollidable theGuyToBeSquished, Dir squishDir);//manages squishing
    int GetEnslaved_AndReturnScore(Dir squishDir, IAttacker squishAttacker);//handles self management on getting squished (for example self destruction)

    bool HasTopSpike();//helps resolving squish winner
    bool HasHardBoot();//helps resolving squish winner
    bool IsFalling();
}

public interface IDestructible
{
    void SelfDestruct();
}

public interface IBorderManager
{
    void SetBorder(Dir dir, bool enabled);
}


public enum Dir { UP, DOWN, LEFT, RIGHT }

public static class InterfaceExtenstion
{
    public static string NameXT(this IVerticalCollidable squishy)
    {
#if UNITY_EDITOR
        MonoBehaviour mono = squishy as MonoBehaviour;
        if (mono != null) return mono.gameObject.name;
#endif
        return "unknownname";
    }


    public static GameObject GetGameObject(this IGridLocus gridloc)
    {
        MonoBehaviour mono = gridloc as MonoBehaviour;
        if (mono != null) return mono.gameObject;
        return null;
    }

    public static bool IsNextLevelTransitionGrid(this IGridLocus gridLocus)
    {
        BaseTerrain bt = gridLocus as BaseTerrain;
        if (bt != null)
        {
            return bt.HasSeedForLevel();
        }
        return false;
    }
    public static bool HasSpike(this IGridLocus gridLocus)
    {
        SpikeTerrain st = gridLocus as SpikeTerrain;
        if (st != null) return true;
        return false;
    }
}