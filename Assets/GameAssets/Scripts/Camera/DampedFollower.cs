﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DampedFollower : MonoBehaviour
{
    public static Transform mainCamPosTransform;
    public Transform target;
    //public float attraction;
    private float Y_offset = 0;
    private float Y
    {
        get
        {
            return target.position.y - transform.position.y - Y_offset;
        }
    }


    private void Start()
    {
        Y_offset = Y;
        mainCamPosTransform = this.transform;
        //Y_offset = target.position.y - transform.position.y;

        lastTargetYPos = target.position.y;
    }


    //private Vector3 targetPos
    //{
    //    get
    //    {
    //        return new Vector3(0, target.position.y- Y_offset, 0);
    //    }
    //}
    // Update is called once per frame
    //   void LateUpdate () {
    //       //this.transform.position = Vector3.Lerp(this.transform.position,targetPos, attraction*Time.deltaTime);
    //       this.transform.Translate(0, attraction * Y* Time.deltaTime,0);

    //}

    float speed = 0;

    public float speedGain = 10;
    public float springC = 1;
    
    void LateUpdate()
    {
        //float targetSpeed = springC*Y;
        //speed = Mathf.Lerp(speed,targetSpeed,speedGain*Time.deltaTime);
        ////if (speed > 0) speed = 0;

        //this.transform.Translate(0, speed* Time.deltaTime, 0);

        float targetVel = (lastTargetYPos - target.position.y)/Time.deltaTime;

        float ofst = 0;
        float lerprate = 10;

        if ( targetVel > max_velocity_threshold )
        {
            ofst = Y_offset * 0;

            //this.transform.position =new Vector3( this.transform.position.x, target.position.y - ofst, this.transform.position.z );

            
        }
        else if ( targetVel > min_velocity_threshold )
        {
            ofst = Y_offset * ( 1 - ( targetVel / max_velocity_threshold ) );

            //this.transform.position = new Vector3 ( this.transform.position.x, target.position.y - ofst, this.transform.position.z );

            
        }
        else if ( targetVel >= 0 )
        {
            ofst =  Y_offset * ( 1 - ( targetVel / max_velocity_threshold ) );

            //this.transform.position = new Vector3 ( this.transform.position.x, target.position.y - ofst, this.transform.position.z );

            
        }
        else
        {
            ofst =  Y_offset;

            //this.transform.position = new Vector3 ( this.transform.position.x, target.position.y - ofst, this.transform.position.z );
        }

        this.transform.position = Vector3.Lerp ( this.transform.position, new Vector3 ( this.transform.position.x, target.position.y - ofst, this.transform.position.z ), lerprate * Time.deltaTime );

        lastTargetYPos = target.position.y;

    }

    float lastTargetYPos;

    public float max_velocity_threshold = 50;
    public float min_velocity_threshold = 1;

}
