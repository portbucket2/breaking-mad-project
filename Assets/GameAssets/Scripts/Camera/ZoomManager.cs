﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomManager : MonoBehaviour {
    private static ZoomManager instance;
    public static ShopVicinityCamZoomer currentShopZoomer
    {
        get
        {
            if (instance.zoomerList.Count > 0)
            {
                return instance.zoomerList[0];
            }
            else
                return null;
        }
    }



    public float zoomTime = 0.5f;

    // Use this for initialization
    void OnEnable ()
    {
        instance = this;
        ShopVicinityCamZoomer.arrivedAtShop += OnShopRecieved;
        ShopVicinityCamZoomer.leftShop += OnShopLeft;
	}
    void OnDisable()
    {
        ShopVicinityCamZoomer.arrivedAtShop -= OnShopRecieved;
        ShopVicinityCamZoomer.leftShop -= OnShopLeft;
    }

    bool shouldBeZoomed
    {
        get
        {
            return zoomerList.Count > 0;
        }
    }
    private List<ShopVicinityCamZoomer> zoomerList = new List<ShopVicinityCamZoomer>();
    void OnShopLeft(ShopVicinityCamZoomer zoomer)
    {
        if (zoomerList.Contains(zoomer)) {
            zoomerList.Remove(zoomer); }

    }
    void OnShopRecieved(ShopVicinityCamZoomer zoomer)
    {
        if (!zoomerList.Contains(zoomer))
        {
            _lastZoomedSize = zoomer.zoomValue;
            _lastZoomPosY = zoomer.zoomPosY;
            sizeGainRate = zoomer.sizeGainRate;
            zoomerList.Add(zoomer);
        }
    }


    [SerializeField] float normalSize;
    public Vector3 normalPos;

    Camera cam;
    float sizeGainRate =1;//dummy value
    float _lastZoomedSize=10;//dummy value
    float zoomedSize
    {
        get
        {
            if (zoomerList.Count > 0)
                return zoomerList[0].zoomValue;
            return _lastZoomedSize;
        }
    }
    float _lastZoomPosY;
    float zoomPosY
    {
        get
        {
            if (zoomerList.Count > 0)
                return zoomerList[0].zoomPosY;
            return _lastZoomPosY;
        }
    }

    Vector3 zoomedPos
    {
        get
        {
            return normalPos + new Vector3(0, zoomPosY, 0);
        }
    }
    private void Awake()
    {
        cam = GetComponent<Camera>();
    }

    float size;
    private void Update()
    {
        if (shouldBeZoomed)
        {
             size -= sizeGainRate*Time.deltaTime; 
        }
        else
        {
            size +=sizeGainRate*Time.deltaTime;
        }
        size = Mathf.Clamp(size, zoomedSize, normalSize);


        if (Mathf.Abs((size / cam.orthographicSize) - 1) > 0.0001f)
        {
            if (size >= normalSize)
            {
                cam.orthographicSize = normalSize;
                cam.transform.localPosition = normalPos;
            }
            else if (size <= zoomedSize)
            {
                cam.orthographicSize = zoomedSize;
                cam.transform.localPosition = zoomedPos;
            }
            else
            {
                cam.orthographicSize = size;
                cam.transform.localPosition = Vector3.Lerp(normalPos, zoomedPos, (size - normalSize) / (zoomedSize - normalSize));
            }
        }
    }
}
