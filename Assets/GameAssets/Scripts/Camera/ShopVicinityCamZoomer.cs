﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ShopVicinityCamZoomer : MonoBehaviour {
    public static ShopVicinityCamZoomer currentSVCamZoomer
    {
        get
        {
            return ZoomManager.currentShopZoomer;
        }
    }

    public float zoomValue = 5;
    public float zoomPosY  = 7;
    public float sizeGainRate = 7.5f;
    private ShopItemTerrain terrainRef;
    public PowerUpProfile powerUpProfile {
        get
        {
            if (terrainRef)
                return terrainRef.GetProfile();
            else return null;
        }
    }

    private void Start()
    {
        terrainRef = GetComponentInParent<ShopItemTerrain>();
    }

    public static event Action<ShopVicinityCamZoomer> arrivedAtShop;
    public static event Action<ShopVicinityCamZoomer> leftShop;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (arrivedAtShop != null) arrivedAtShop(this);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (leftShop != null) leftShop(this);
        }
    }
    private void OnDisable()
    {
        if (leftShop != null) leftShop(this);
    }
    private void OnDestroy()
    {
        if (leftShop != null) leftShop(this);
    }
}
