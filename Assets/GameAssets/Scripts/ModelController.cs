﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelController : MonoBehaviour {

    public float hitAnimationGraceMultiplier = 1;
    public RotationScheme rotationScheme = RotationScheme.MIXED;
    public float rotationRateConstant = 30;

    Quaternion targetRotation;

    Animator _animator;
    Animator animator
    {
        get
        {
            if (!_animator) _animator = GetComponent<Animator>();
            return _animator;
        }
    }
    bool isPlayer;
    // Use this for initialization
    public void OnInit()
    {

        if (animator != null)
        {
            animator.Rebind();

        }
        isPlayer = (this.GetComponentInParent<PlayerManager>()!=null);
        //SetFallingState(false);
        //SetMovingState(false);
        targetRotation = this.transform.rotation;
    }

    public void SetHitTimeMultiplier(float time)
    {
        float speed = (1 / (time * hitAnimationGraceMultiplier));
        ////CM_Deb"hit speed was set to {0}", speed);
        if (animator != null) animator.SetFloat("hitSpeed", speed);
    }
    public void SetMoveTimeMultiplier(float time)
    {
        float speed = (1 / (time));
        ////CM_Deb"walk speed was set to {0}", speed);
        if (animator != null) animator.SetFloat("walkSpeed", speed);
    }

    public void SetFallingState(bool isFalling)
    {
        if (animator != null) animator.SetBool("FALL", isFalling);
    }
    public void SetMovingState(bool isMoving)
    {
        if (animator != null)
        {
            if (isPlayer)
            {
                if (isMoving) animator.SetTrigger("W");
            }
            else
            {
                animator.SetBool("WALK", isMoving);
            }
        }
    }
    public void SetHittingState(bool isHitting)
    {
        if (animator != null)
        {
            if(isPlayer)animator.SetBool("HIT", isHitting);
        }
    }

    public void TriggerAttack(Dir direction, System.Action callbackOnHit)
    {
        onHitAct = callbackOnHit;
        if (animator)
        {
            string s="";
            switch (direction)
            {
                case Dir.UP:
                    //s ="U";
                    //break;
                case Dir.DOWN:
                    //s="D";
                    //break;
                case Dir.LEFT:
                case Dir.RIGHT:
                    s="S";
                    break;
            }
            NLog.A_AppendLastLog(string.Format("type<{0}>",s));
            animator.SetTrigger(s);
        }
    }

    public void TriggerAttackForPlayer(Dir direction, bool isTheFrontBlockUnbreakable, System.Action callbackOnHit)
    {
        onHitAct = callbackOnHit;
        if (animator)
        {
            string s = "";
            switch (direction)
            {
                case Dir.UP:
                    s = isTheFrontBlockUnbreakable ? "UC" : "U";
                    break;
                case Dir.DOWN:
                    s = isTheFrontBlockUnbreakable ? "DC" : "D";
                    break;
                case Dir.LEFT:
                case Dir.RIGHT:
                    s = isTheFrontBlockUnbreakable ? "SC" : "S";
                    break;
            }
            NLog.A_AppendLastLog(string.Format("type<{0}>", s));
            ////CM_Deb"<color=green>we will play hit animation and s: "+s+"</color>");
            animator.SetTrigger(s);
        }
    }

    System.Action onHitAct;
    public void OnAttackHitPoint()
    {
        //if(GetComponentInParent<PlayerManager>())//CM_Deb"OnHitPoint");
        if (onHitAct != null)
        {
            onHitAct();
            onHitAct = null;
        }
    }

    public void SetAnimatorVariableFlip ( bool flip )
    {
        if ( animator != null )
            animator.SetBool ( "Flip", flip );
    }


    public void SetModelOrientation(Dir d)
    {
        switch (rotationScheme)
        {
            case RotationScheme.MIXED:
                {
                    float sx = Mathf.Abs(transform.localScale.x);
                    if (d == Dir.LEFT)
                        transform.localScale = new Vector3(-sx, transform.localScale.y, transform.localScale.z);
                    else
                        transform.localScale = new Vector3(sx, transform.localScale.y, transform.localScale.z);


                    switch (d)
                    {
                        case Dir.UP:
                        case Dir.DOWN:
                            targetRotation = Quaternion.Euler(0, 180, 0);
                            break;
                        case Dir.LEFT:
                            targetRotation = Quaternion.Euler(0, 240, 0);
                            break;
                        case Dir.RIGHT:
                            targetRotation = Quaternion.Euler(0, 130, 0);
                            break;
                    }
                }
                break;
            case RotationScheme.SCALE_Z_ONLY:
                {
                    float sz = Mathf.Abs(transform.localScale.z);
                    switch (d)
                    {
                        case Dir.LEFT:
                            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, -sz);
                            break;
                        case Dir.RIGHT:
                            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, sz);
                            break;
                    }
                }
                break;
            case RotationScheme.SCALE_INVERT_X:
                {
                    float x = Mathf.Abs(transform.localScale.x);
                    switch (d)
                    {
                        case Dir.LEFT:
                            if(isPlayer)SetAnimatorVariableFlip ( transform.localScale.x * (-1) < 0 );
                            transform.localScale = new Vector3(-x, transform.localScale.y, transform.localScale.z);
                            break;
                        case Dir.RIGHT:
                            if(isPlayer)SetAnimatorVariableFlip ( transform.localScale.x  < 0 );
                            transform.localScale = new Vector3(x, transform.localScale.y, transform.localScale.z);
                            break;
                    }
                }
                break;
            case RotationScheme.SCALE_INVERT_ZX:
                {
                    float z = Mathf.Abs(transform.localScale.z);
                    float x = Mathf.Abs(transform.localScale.x);
                    switch (d)
                    {
                        case Dir.LEFT:
                            transform.localScale = new Vector3(-x, transform.localScale.y, -z);
                            break;
                        case Dir.RIGHT:
                            transform.localScale = new Vector3(x, transform.localScale.y, z);
                            break;
                    }
                }
                break;
            case RotationScheme.ROTATE_FULL_Y:
                {
                    switch (d)
                    {
                        //case Dir.UP:
                        //case Dir.DOWN:
                        //    targetRotation = Quaternion.Euler(0, 180, 0);
                            //break;
                        case Dir.LEFT:
                            targetRotation = Quaternion.Euler(0, 270, 0);
                            break;
                        case Dir.RIGHT:
                            targetRotation = Quaternion.Euler(0, 90, 0);
                            break;
                    }
                }
                break;
        }





    }
    public void FixedUpdate()
    {
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, rotationRateConstant * Time.deltaTime);
    }

    float recordStartTime;
    private void RecordStart()
    {
        NLog.A_AppendLastLog(string.Format("E1"));
        recordStartTime = Time.realtimeSinceStartup;
    }
    private void RecordFinish()
    {
        ////CM_Deb"f");
        NLog.A_AppendLastLog(string.Format("Time {0:f3}",(Time.realtimeSinceStartup-recordStartTime)));
    }




}
public enum RotationScheme
{
    MIXED = 1,
    SCALE_Z_ONLY = 2,
    ROTATE_FULL_Y = 3,
    SCALE_INVERT_X = 4,
    SCALE_INVERT_ZX = 5
}
