﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoneTerrain : HitBaseTerrain, IHitable
{
    public List<Sprite> sprites;
    public SpriteRenderer spriteRend;
    public GameObject bottomShadow;

    public override void OnInit(ITerrainInitData initData)
    {
        spriteRend.sprite = sprites[Random.Range(0,sprites.Count)];
        int n = Random.Range(0,4);
        spriteRend.transform.rotation = Quaternion.Euler(new Vector3(0,0,90*n));
        bottomShadow.SetActive(rowID != 2);
    }
    float IHitable.MaxHitDistance()
    {
        return 1;
    }

}

