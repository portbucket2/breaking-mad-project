﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WallTerrain : HitBaseTerrain, IHitable
{
    public Transform modelRoot;
    public List<GameObject> wallOptions;
    //public List<GameObject> nextToWindowOption;
    //public GameObject nextToWindowOption;


    public override void OnInit(ITerrainInitData initData)
    {
        if (((IGridLocus)this).relativeLocus(Dir.RIGHT) == null)
        {
            modelRoot.localScale = new Vector3(-1, 1, 1);
        }
        if (((IGridLocus)this).relativeLocus(Dir.LEFT) == null)
        {
            modelRoot.localScale = new Vector3(1, 1, 1);
        }
        DoModelSelection();
    }

    public void DoModelSelection()
    {
        //WindowTerrain upwin = (this as IGridLocus).relativeLocus(Dir.UP) as WindowTerrain;
        //WindowTerrain downwin = (this as IGridLocus).relativeLocus(Dir.DOWN) as WindowTerrain;
        //bool isNextTowindow = upwin || downwin;

        int visIndex;
        //if (isNextTowindow)
        //{
        //    visIndex = Random.Range(0, nextToWindowOption.Count);
        //}
        //else
        //{
            visIndex = Random.Range(0, wallOptions.Count);
        //}

        //for (int i = 0; i < wallOptions.Count; i++)
        //{
        //    wallOptions[i].SetActive(i == visIndex && !isNextTowindow);
        //}
        //for (int i = 0; i < nextToWindowOption.Count; i++)
        //{
        //    nextToWindowOption[i].SetActive(i == visIndex && isNextTowindow);
        //}
        for (int i = 0; i < wallOptions.Count; i++)
        {
            wallOptions[i].SetActive(i == visIndex);
        }

    }


    float IHitable.MaxHitDistance()
    {
        return 1;
    }
}

