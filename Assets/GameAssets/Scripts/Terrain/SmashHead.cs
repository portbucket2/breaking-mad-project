﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashHead  :MonoBehaviour{
    public IAttacker smasherRef;
    public Dir hitDirection = Dir.UP;

    private void OnTriggerEnter(Collider other)
    {
        ICharacter character = other.gameObject.GetComponentInParent<ICharacter>();
        IHitable hitable = other.GetComponentInParent<IHitable>();

        if (character != null)
        {
            hitable.TakeHit(hitDirection, smasherRef);
            ////CM_Debhitable);
        }

    }
}
