﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FishSpace;

public class VisualBaseTerrain : BaseTerrain
{
    public List<BackWallDesign> designs;
    public int noDesignWeight;
    public GameObject designBGItem;

    public bool pendingDesign;
    public override void OnInit(ITerrainInitData initData)
    {
        VisualBaseTerrainInitData idata = initData as VisualBaseTerrainInitData;
        if (idata == null)
        {
            Debug.LogError("initial data is invalid");
        }

        if (!idata.isReplacementObject)
        {
            pendingDesign = true;
            AddToVisualBaseTerrainListForCurrentLevel(this);
        }
        else
        {
            ChooseSingleBlockDesign();
        }
    }

    public void ChooseSingleBlockDesign()
    {
        int designIndex = Random.Range(0, designs.Count + noDesignWeight);
        bool choiceWasMade = false ;
        for (int i = 0; i < designs.Count; i++)
        {
            if (designs[i].x == 1 && designs[i].y == 1 && designIndex == i)
            {
                choiceWasMade = true;
                designs[i].Apply(this);
            }
        }
        if (!choiceWasMade)
            SetDesign_None();
    }

    //public bool choseSingle;
    public bool disableWasCalled;
    public void SetDesign_None(bool keepBasicBG=true)
    {
        disableWasCalled = true;
        foreach (var item in designs)
        {
            item.gameObject.SetActive(false);
        }
        designBGItem.SetActive(keepBasicBG);
        pendingDesign = false;
    }


    private static List<VisualBaseTerrain> vbTerrainListForLevel = new List<VisualBaseTerrain>();
    private static void AddToVisualBaseTerrainListForCurrentLevel(VisualBaseTerrain vbt)
    {
        if (!vbTerrainListForLevel.Contains(vbt))
            vbTerrainListForLevel.Add(vbt);
    }
    public static void ManageVBTBackgrounds()
    {
        foreach (VisualBaseTerrain rootVBT in vbTerrainListForLevel)
        {
            if(rootVBT.rowOffset == -1)
                ManageForVBT(rootVBT);
        }
        foreach (VisualBaseTerrain rootVBT in vbTerrainListForLevel)
        {
            if (rootVBT.rowOffset != -1)
                ManageForVBT(rootVBT);
        }
    }

    private static List<VisualBaseTerrain> templist = new List<VisualBaseTerrain>();
    private static void ManageForVBT(VisualBaseTerrain rootVBT)
    {
        if (!rootVBT.pendingDesign) return;
        bool designWasSelected = false;
        foreach (BackWallDesign design in rootVBT.designs)
        {




            bool isDesignAcceptableSoFar = true;
            if (design.x == 3)
            {
                if (rootVBT.rowOffset != -1)
                {
                    isDesignAcceptableSoFar = false;
                }
            }
            templist.Clear();
            if (isDesignAcceptableSoFar)
            {
                for (int x = 0; x < design.x; x++)
                {
                    VisualBaseTerrain colStartPoint = (rootVBT as IGridLocus).relativeLocus(Dir.RIGHT, x - 1) as VisualBaseTerrain;
                    if (colStartPoint == null || !colStartPoint.pendingDesign)
                    {
                        isDesignAcceptableSoFar = false;
                        break;
                    }
                    for (int y = 0; y < design.y; y++)
                    {
                        VisualBaseTerrain colItems = (colStartPoint as IGridLocus).relativeLocus(Dir.DOWN, y - 1) as VisualBaseTerrain;
                        if (colItems == null || !colItems.pendingDesign)
                        {
                            isDesignAcceptableSoFar = false;
                            break;
                        }
                        else
                            templist.Add(colItems);
                    }
                }
            }

            if (isDesignAcceptableSoFar)
            {
                float rollVal = Random.Range(.0f, 1.0f);
                if (rollVal < design.weight)
                {
                    designWasSelected = true;
                    design.Apply(templist);
                    break;
                }
            }


        }
        if (!designWasSelected) //no design selected from options => set designless mode as default
            rootVBT.SetDesign_None();
    }

    public static void ClearVBTList()
    {
        vbTerrainListForLevel.Clear();
    }


}

public class VisualBaseTerrainInitData : ITerrainInitData
{
    public bool isReplacementObject;
}