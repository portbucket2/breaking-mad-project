﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBaseTerrain : BaseTerrain, IHitable {
    public static event System.Action OnPlayerHitTerrain;

    public virtual void PlaySoundOnHit()
    {
        //if (attacker == PlayerManager.playerAttacker)
        SoundKeeper.PlayHitSound(isUnbreakable ? HitAudioType.Hit_Concreate : HitAudioType.Hit_Shatter, this.transform.position);

    }

    public virtual void OnTakeHit(Dir dir,IAttacker attacker)
    {
        if (OnPlayerHitTerrain != null && attacker==PlayerManager.playerAttacker) OnPlayerHitTerrain();

        //FishSpace.Centralizer.Add_DelayedAct(() => { PlayHitParticle(dir); }, 2);//rumman
        PlayHitParticle(dir,attacker);
        PlaySoundOnHit();
        }
    public virtual void PlayHitParticle(Dir dir, IAttacker attacker)
    {
        bool isUnbreakable = (this as IGridLocus).IsUnbreakable();
        float delay = attacker == null ? 0 : attacker.TerrainHitParticleDelay(dir, isUnbreakable);
        
        
        if (isUnbreakable)
        {
            FishSpace.Centralizer.Add_DelayedAct(() =>
            {
                LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir);
                //if (attacker == PlayerManager.playerAttacker)
                //{
                //    //CM_Deb"Hit particle will be played because the block was hit from direction: " + dir + " and is it Unbreakable? " + isUnbreakable);
                //    Debug.Break();
                //}

            }, delay);
        }
        else
        {
            FishSpace.Centralizer.Add_DelayedAct(() =>
            {
                LevelManager.Play_DefaultBlockParticle(this, dir);
                //if (attacker == PlayerManager.playerAttacker)
                //{
                //    //CM_Deb"Hit particle will be played because the block was hit from direction: " + dir + " and is it Unbreakable? " + isUnbreakable);
                //    Debug.Break();
                //}
                
            }, delay);
        }


        //rumman
        //if (isUnbreakable)
        //{

        //    FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play up unbreakable particle"); }, attacker.TerrainHitParticleDelay(dir, isUnbreakable));



        //    if (dir == Dir.UP) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play up unbreakable particle"); }, PlayerManager.instance.upNonHitableParticleDelay); }
        //    else if (dir == Dir.DOWN) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play down unbreakable particle"); }, PlayerManager.instance.downNonHitableParticleDelay); }
        //    else if (dir == Dir.LEFT) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play left unbreakable particle"); }, PlayerManager.instance.leftNonHitableParticleDelay); }
        //    else { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir); //CM_Deb"will play right unbreakable particle"); }, PlayerManager.instance.rightNonHitableParticleDelay); }
        //}
        //else
        //{
        //    if (dir == Dir.UP) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultBlockParticle(this, dir); //CM_Deb"will play up breakable particle"); }, PlayerManager.instance.upHitableParticleDelay); }
        //    else if (dir == Dir.DOWN) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultBlockParticle(this, dir); //CM_Deb"will play down breakable particle"); }, PlayerManager.instance.downHitableParticleDelay); }
        //    else if (dir == Dir.LEFT) { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultBlockParticle(this, dir); //CM_Deb"will play left breakable particle"); }, PlayerManager.instance.leftHitableParticleDelay); }
        //    else { FishSpace.Centralizer.Add_DelayedAct(() => { LevelManager.Play_DefaultBlockParticle(this, dir); //CM_Deb"will play right breakable particle"); }, PlayerManager.instance.rightHitableParticleDelay); }
        //}

        //if ((this as IGridLocus).IsUnbreakable())
        //    LevelManager.Play_DefaultConcreteHitSparkParticle(this, dir);
        //else
        //    LevelManager.Play_DefaultBlockParticle(this, dir);
    }

    

    GameObject IHitable.GetGO()
    {
        return this.gameObject;
    }
    int IHitable.GetScoreOnHit()
    {
        return 0;
    }

    float IHitable.MaxHitDistance()
    {
        return 10000;
    }

    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        OnTakeHit(dir, attacker);
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }
}
