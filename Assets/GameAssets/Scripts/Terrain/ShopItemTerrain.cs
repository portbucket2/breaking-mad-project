﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ShopItemTerrain : HitBaseTerrain,IHitable
{
    public static int shopRowIndex=-1;
    public PowerUpProfiles profileDataSet;
    private PowerUpProfile profile;
    public Text costText;
    public Image itemVisual;
    public Text notEnoguhGold;
    private Color originalNotEnoguhGoldColor;

    private bool didReportNotEnoughGoldAnalytics;

    public int currentLevel { get { return LevelManager.instance.currentLevelNumber; } }

    public PowerUpProfile GetProfile()
    {
        return profile;
    }

    public override void OnAwake()
    {
        base.OnAwake();
        originalNotEnoguhGoldColor = notEnoguhGold.color;
    }
    public override void OnInit(ITerrainInitData initData)
    {
        shopRowIndex = (this as IGridLocus).row.rowID;
        profile = profileDataSet.GetProfileFor(this, currentLevel);
        if (profile == null)
        {
            UseReplacement();
            return;
        }
        notEnoguhGold.color = new Color(0, 0, 0, 0);
        costText.text = profile.Cost(currentLevel).ToString();
        itemVisual.sprite = profile.sprite;
        itemVisual.preserveAspect = true;
        didReportNotEnoughGoldAnalytics = false;
        StopAllCoroutines();
    }

    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        if (PlayerManager.playerAttacker != attacker) return;
        if (PlayerManager.CoinsAvailable() < profile.Cost(currentLevel))
        {
            StopAllCoroutines();
            StartCoroutine(NotEnoughRoutine());
            if (!didReportNotEnoughGoldAnalytics)
            {
                BMadAnalyticsHandler.ReportShopItemHit(itemId: profile.type.ToString(), shopLevel: currentLevel, success: false);
                didReportNotEnoughGoldAnalytics = true;
            }
        }
        else
        {
            PlayerManager.PurchasePowerUp(profile, profile.Cost(currentLevel));
            UseReplacement();
            BMadAnalyticsHandler.ReportShopItemHit(itemId: profile.type.ToString(), shopLevel: currentLevel, success: true);
        }
    }

    float IHitable.xPosition { get { return this.transform.position.x; } }
    IEnumerator NotEnoughRoutine()
    {
        notEnoguhGold.color = originalNotEnoguhGoldColor;
        yield return new WaitForSeconds(0.5f);
        float ft = 0.5f;
        float st = Time.time;
        while (Time.time<st + ft)
        {
            notEnoguhGold.color = Color.Lerp(originalNotEnoguhGoldColor, new Color(0, 0, 0, 0), (Time.time - st) / ft);
            yield return null;
        }
        notEnoguhGold.color = new Color(0, 0, 0, 0);
    }
}
