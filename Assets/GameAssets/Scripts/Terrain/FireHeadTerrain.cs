﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FishSpace;

public class FireHeadTerrain : HitBaseTerrain, IHitable, IAttacker
{
    public float fireSpeed;
    public Transform directionalPart;
    public GameObject firePrefab;
    public List<FireTiming> timings;

    //public ParticleSystem spillageParticle;
   // public ParticleSystem preGasParticle;
    public Transform spwnPoint;


    bool isActive;
    int killPoints;
    FireTiming mytiming;
    Coroutine fireCoroutine;
    Vector3 dirVec;
    Dir dir;
    Vector3 scaleVec;
    public override void OnInit(ITerrainInitData initData)
    {
        isActive = true;
        killPoints = 0;
        int rightFaceChance = 0;
        int leftFaceChance = 0;
        for (int i = 0; i < row.activeCols; i++)
        {

            if (i+1-row.posIndexOffset< rowOffset)
            {
                leftFaceChance++;
            }
            else if (i+1 - row.posIndexOffset > rowOffset)
            {
                rightFaceChance++;
            }
        }

        leftFaceChance = leftFaceChance * leftFaceChance;
        rightFaceChance = rightFaceChance * rightFaceChance;
        float rollval = Random.Range(0.0f, leftFaceChance + rightFaceChance);
        if (rollval < leftFaceChance)
        {
            dirVec = Vector3.left;
            ////CM_Deb"{0}-{1} : {2}",leftFaceChance,rightFaceChance, rollval);
            dir = Dir.LEFT;
            scaleVec = new Vector3(-1,1,1);
        }
        else
        {
            ////CM_Deb"{0}-{1} : {2}", leftFaceChance, rightFaceChance, rollval);
            dirVec =Vector3.right;
            dir = Dir.RIGHT;
            scaleVec = new Vector3(1, 1, 1);
        }
        directionalPart.localScale = scaleVec;
        int timingIndex = 0;
        if (LevelManager.instance.currentLevelNumber > 3)
        {
            timingIndex = Random.Range(0, timings.Count);
        }
        mytiming = timings[timingIndex];
        if (fireCoroutine != null) StopCoroutine(fireCoroutine);
        fireCoroutine = StartCoroutine(FireUpdate());
    }
    public override void PlaySoundOnHit()
    {
        //if (attacker == PlayerManager.playerAttacker)
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Cart, this.transform.position);

    }
    float pregasDelay = 0.6f;
    private IEnumerator FireUpdate()
    {
        yield return new WaitForSeconds(Random.Range(0,2.5f));
        while (isActive)
        {
            yield return new WaitForSeconds(mytiming.gap1 - pregasDelay);
            if (isActive)
            {
                //preGasParticle.Play();
                SoundKeeper.PlayATS(SoundKeeper.instance.poison_shoot, this.transform.position);
            }
            yield return new WaitForSeconds(pregasDelay);
            if(isActive) BreathFire();
            if (mytiming.gap2 > 0)
            {
                yield return new WaitForSeconds(mytiming.gap2 - pregasDelay);
                if (isActive)
                {
                    //preGasParticle.Play();
                    SoundKeeper.PlayATS(SoundKeeper.instance.poison_shoot,this.transform.position);
                }
                yield return new WaitForSeconds(pregasDelay);
                if (isActive) BreathFire();
            }
            
        }
    }
    private void BreathFire()
    {
        Quaternion q = Quaternion.identity;
        if (dir == Dir.LEFT) q = Quaternion.Euler(0, 180, 0);
        
        Rigidbody rb = Pool.Instantiate(firePrefab, spwnPoint.position, q).GetComponent<Rigidbody>() ;
        ////CM_Deb"source: " + transform.name + " and fireprefab name: " + firePrefab.name + " and rigidbody name: "+rb.gameObject.name);
        rb.GetComponent<Fire>().Init(this,dir);
       // spillageParticle.Play();
        rb.velocity = dirVec*fireSpeed;

    }
    [SerializeField]int scorePoints = 10;
    int IHitable.GetScoreOnHit()
    {
        return scorePoints + killPoints;
    }
    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        UseReplacement();
        OnTakeHit(dir,attacker);

        LevelManager.Show_ScoreEarned(this.transform.position, (this as IHitable).GetScoreOnHit(), attacker);
        //LevelManager.Play_DefaultBlockParticle(this, dir);
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }
    string IAttacker.attackerid
    {
        get
        {
            return "poisonhead";
        }
    }
    void IAttacker.AddScore(int score)
    {
        killPoints += score;
    }

    float IAttacker.GetAttackHittingTime()
    {
        throw new System.NotImplementedException();
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return 0;
    }
    float IAttacker.GetAttackRange()
    {
        throw new System.NotImplementedException();
    }

    void IAttacker.TriggerAttackEffects(Vector3 point)
    {
        throw new System.NotImplementedException();
    }
    void IAttacker.NullifyThreat(IAttacker nullifier)
    {
        ////CM_Deb"deactivated");
        isActive = false;
    }
    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable) { return 0; }
    float IAttacker.EnemyHitParticleDelay(Dir direction) { return 0; }
    //IEnumerator 

}
[System.Serializable]
public class FireTiming
{
    public float gap1 =1;
    public float gap2 =-1;
}

