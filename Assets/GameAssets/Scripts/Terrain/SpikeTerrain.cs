﻿using UnityEngine;
using System.Collections;
using System;

public class SpikeTerrain : HitBaseTerrain,IHitable, IAttacker
{
    public GameObject spikeBreakParticle;
    public override void PlayHitParticle(Dir dir, IAttacker attacker)
    {
        LevelManager.Play_SpecificParticleOnGridElement(this, spikeBreakParticle);//rumman vai
        ////CM_Deb"tried to play particle");
    }

    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        //Debug.LogError("!!!!!!!!");
        //Debug.Break();
        base.OnTakeHit(dir, attacker);
        UseReplacement();
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }
    public override void PlaySoundOnHit()
    {
        //if (attacker == PlayerManager.playerAttacker)
        SoundKeeper.PlayHitSound(HitAudioType.Hit_Cart, this.transform.position);

    }

    void IAttacker.AddScore(int score)
    {
    }

    float IAttacker.GetAttackHittingTime()
    {
        return 1000;
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return 0;
    }

    float IAttacker.GetAttackRange()
    {
        return 0;
    }

    void IAttacker.TriggerAttackEffects(Vector3 point)
    {

    }

    void IAttacker.NullifyThreat(IAttacker nullifier)
    {

    }

    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable)
    {
        return 0;
    }

    float IAttacker.EnemyHitParticleDelay(Dir direction)
    {
        return 0;
    }
    string IAttacker.attackerid
    {
        get
        {
            return "spike";
        }
    }
}

