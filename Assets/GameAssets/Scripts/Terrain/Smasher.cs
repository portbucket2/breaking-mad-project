﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class Smasher : HitBaseTerrain ,IAttacker, IHitable
{
    float IHitable.MaxHitDistance()
    {
        return 1;
    }
    string IAttacker.attackerid
    {
        get
        {
            return "smasher";
        }
    }
    void IAttacker.AddScore(int score)
    {
    }

    float IAttacker.GetAttackHittingTime()
    {
        return -1;
    }
    float IAttacker.GetAttackPreparationTime()
    {
        return 0;
    }
    float IAttacker.GetAttackRange()
    {
        return -1;
    }
    void IAttacker.TriggerAttackEffects(Vector3 point)
    {
    }
    void IAttacker.NullifyThreat(IAttacker nullifier)
    {
        isActive = false;
    }
    float IAttacker.TerrainHitParticleDelay(Dir direction, bool unbreakable) { return 0; }
    float IAttacker.EnemyHitParticleDelay(Dir direction) { return 0; }

    public float waitTime;
    public float waitTime2;
    [Range(0,1)]
    public float warnRatio;
    public float travelSpeed;
    public float retractSpeed;
    public float maxDistance = 6;
    public Transform rotatingPart;
    public Transform movingPart;
    public GameObject smashHead;
    public GameObject extentionPrefab;
    //public List<GameObject> extensions = new List<GameObject>();
    public SmasherExtension extention;

    private SmashHead smashHeadScript;
    private Animator anim;
    float rotorZangle = 0;
    float spriteYScale = 1;
    bool isActive;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    float movingPartInitialX;
    public override void OnInit(ITerrainInitData initData)
    {
        isActive = true;
        if (smashHeadScript == null) smashHeadScript = smashHead.GetComponent<SmashHead>();
        smashHeadScript.smasherRef = this;

        int rightFaceChance = 0;
        int leftFaceChance = 0;
        for (int i = 0; i < row.activeCols; i++)
        {

            if (i + 1 - row.posIndexOffset < rowOffset)
            {
                leftFaceChance++;
            }
            else if (i + 1 - row.posIndexOffset > rowOffset)
            {
                rightFaceChance++;
            }
        }
        leftFaceChance = leftFaceChance * leftFaceChance;
        rightFaceChance = rightFaceChance * rightFaceChance;
        float rollval = Random.Range(0.0f, leftFaceChance + rightFaceChance);
        if (rollval < leftFaceChance)
        {
            rotorZangle = -180;
            spriteYScale = -1;
            smashHeadScript.hitDirection = Dir.LEFT;
            ////CM_Deb"{0}-{1} : {2}",leftFaceChance,rightFaceChance, rollval);
        }
        else
        {
            ////CM_Deb"{0}-{1} : {2}", leftFaceChance, rightFaceChance, rollval);
            rotorZangle = 0;
            spriteYScale = 1;
            smashHeadScript.hitDirection = Dir.RIGHT;
        }
        rotatingPart.rotation = Quaternion.Euler(0,0,rotorZangle);
        StopAllCoroutines();
        StartCoroutine(InitializeSmashing());

        anim.SetBool("shake",false);
        //extention.SetSpriteYScaleMult(spriteYScale);
        movingPart.localScale = new Vector3(1, spriteYScale, 1);
        movingPartInitialX = movingPart.position.x;
    }

    IEnumerator InitializeSmashing () {
        yield return new WaitForSeconds(Random.Range(0,2.0f));
        while(true)
        {
            extention.Hide();
            smashHead.SetActive(false);
            yield return new WaitForSeconds(waitTime*( 1-warnRatio));
            anim.SetBool("shake", true);
            yield return new WaitForSeconds(waitTime*warnRatio);
            anim.SetBool("shake", false);
            smashHead.SetActive(true);

            //for (int i = extensions.Count - 1; i >= 0; i++)
            //{
            //    GameObject go = extensions[i];
            //    extensions.RemoveAt(i);
            //    Pool.Destroy(go);
            //}
            //extensions.Clear();
            float d = 0;
            float travelDistance = maxDistance;
            
            IHitable hitable = null;
            PredefinedRayCast rc = LevelManager.instance.assets.rc_HitableLayers_withColliderOrTrigger;
            if (rc.DoubleCastAll(LevelManager.instance.assets.rc_HitableLayers_withColliderWithoutTrigger, movingPart.position,movingPart.right,maxDistance))//Physics.Raycast(movingPart.position, rotatingPart.right, out rch, maxDistance, LevelManager.instance.assets.allHitableMask, QueryTriggerInteraction.Ignore))
            {
                hitable = rc.collider.gameObject.GetComponentInParent<IHitable>();
                if (hitable != null)
                {
                    travelDistance = (rc.point - movingPart.position).magnitude-0.5f;
                }
            }

            while (d < travelDistance && isActive)
            {
                d += travelSpeed * Time.deltaTime;
                d = Mathf.Clamp(d, 0, travelDistance);
                movingPart.localPosition = new Vector3(d, 0, 0);
                extention.OnMoverUpdate(movingPart.position);
                yield return null;
            }
            SoundKeeper.PlayATS(SoundKeeper.instance.piston_hit,this.transform.position);
            yield return null;
            yield return null;
            smashHead.SetActive(false);
            yield return new WaitForSeconds(waitTime2);
            while (d > 0)
            {
                d -= retractSpeed * Time.deltaTime;
                d = Mathf.Clamp(d,0,travelDistance);

                movingPart.localPosition = new Vector3(d, 0, 0);
                extention.OnMoverUpdate(movingPart.position);
                yield return null;
            }
            movingPart.localPosition = Vector3.zero;
        }
       
    }


}
