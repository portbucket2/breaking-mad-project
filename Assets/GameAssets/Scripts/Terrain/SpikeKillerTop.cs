﻿using UnityEngine;
using System.Collections;
using System;

public class SpikeKillerTop : MonoBehaviour, IVerticalCollidable
{
    [SerializeField] SpikeTerrain terrainRef;

    void IVerticalCollidable.ResolveAsMasterEntity(IVerticalCollidable slave, Dir squishDir)
    {
        slave.GetEnslaved_AndReturnScore(squishDir, terrainRef as IAttacker);
        (terrainRef as IHitable).TakeHit(Dir.DOWN, terrainRef as IAttacker);

    }
    int IVerticalCollidable.GetEnslaved_AndReturnScore(Dir squishDir, IAttacker squishAttacker)
    {
        (terrainRef as IHitable).TakeHit(squishDir,squishAttacker);
        return 0;
    }

    bool IVerticalCollidable.HasHardBoot()
    {
        return false;
    }

    bool IVerticalCollidable.HasTopSpike()
    {
        return true;
    }
    bool IVerticalCollidable.IsFalling()
    {
        return false;
    }


}

