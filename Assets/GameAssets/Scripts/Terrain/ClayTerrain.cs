﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FishSpace;
public class ClayTerrain : HitBaseTerrain, IHitable, IBorderManager
{
    public List<Sprite> sprites;
    public SpriteRenderer sprenderer;

    public List<Sprite> borderSprites;
    public List<SpriteRenderer> borderRenderers;

    public GameObject borderRight;
    public GameObject borderUp;
    public GameObject borderLeft;
    public GameObject borderDown;
    IGridLocus selfLoc;
    IBorderManager selfBorderManager;
    public override void OnInit(ITerrainInitData initData)
    {

        sprenderer.sprite = sprites[Random.Range(0, sprites.Count)];
        foreach (var item in borderRenderers)
        {
            item.sprite = borderSprites[Random.Range(0,borderSprites.Count)];
        }

        selfLoc = this as IGridLocus;
        ManageBorders(Dir.UP);
        ManageBorders(Dir.DOWN);
        ManageBorders(Dir.LEFT);
        ManageBorders(Dir.RIGHT);
    }



    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        base.OnTakeHit(dir, attacker);
       //Pressurizer.WakeUp();
        ActivateNeighboursBorder(Dir.UP);
        ActivateNeighboursBorder(Dir.DOWN);
        ActivateNeighboursBorder(Dir.LEFT);
        ActivateNeighboursBorder(Dir.RIGHT);
        UseReplacement();;
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }

    void ManageBorders(Dir dir)
    {
        IBorderManager otherBorderMan = selfLoc.relativeLocus(dir) as IBorderManager;
        if (otherBorderMan!=null)
        {
            //Debug.DrawRay(this.transform.position - Vector3.forward, MovementController.Dir2Vec(dir)/1.15f , Color.blue, 5);
            (this as IBorderManager).SetBorder(dir, false);
            otherBorderMan.SetBorder(MovementController.OppositeDir(dir), false);
        }
        else
        {
            //Debug.DrawRay(this.transform.position - Vector3.forward, MovementController.Dir2Vec(dir)/2, Color.red, 5);
            (this as IBorderManager).SetBorder(dir, true);
        }
    }
    void ActivateNeighboursBorder(Dir dir)
    {
        IBorderManager otherBorderMan = selfLoc.relativeLocus(dir) as IBorderManager;
        if (otherBorderMan!=null)
        {
            otherBorderMan.SetBorder(MovementController.OppositeDir(dir), true);
        }
    }

    void IBorderManager.SetBorder(Dir dir, bool enabled)
    {
        //if(external) Debug.DrawRay(this.transform.position - Vector3.forward, -Vector3.forward, Color.cyan, 5);
        switch (dir)
        {
            case Dir.UP:
                borderUp.SetActive(enabled);
                break;
            case Dir.DOWN:
                borderDown.SetActive(enabled);
                break;
            case Dir.LEFT:
                borderLeft.SetActive(enabled);
                break;
            case Dir.RIGHT:
                borderRight.SetActive(enabled);
                break;
        }
    }
}

