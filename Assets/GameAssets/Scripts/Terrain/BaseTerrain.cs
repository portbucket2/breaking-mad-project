﻿using UnityEngine;
using System.Collections;
using System;
using FishSpace;

public class BaseTerrain : MonoBehaviour, IGridLocus
{
    public void SelfDestruct()
    {
        Pool.Destroy(gameObject);
    }
    public int rowID = -1;
    public bool isUnbreakable = false;
    public bool isBlank = false;
    [HideInInspector] public bool isPartOfPassiveArea;
    public GameObject replacementOnDestroy;
    public virtual void UseReplacement()
    {
        if (replacementOnDestroy != null)
        {
            GameObject rep = Pool.Instantiate(replacementOnDestroy, transform.position, transform.rotation, transform.parent);
            IGridLocus repLoc = rep.GetComponent<IGridLocus>();
            repLoc.Replace(this);
            this.SelfDestruct();
        }
        else
        {
            Debug.LogErrorFormat("{0} object has no replacement defined!", gameObject.name);
        }
    }


    private int needsLevel = -1;
    private GameObject spwnPrefab;


    public GridRow row { get; set; }
    public int rowOffset { get; set; }
    public Vector3 position
    {
        get
        {
            return row.position + new Vector3(rowOffset * row.dimension, 0, 0);
        }
    }
    public Vector3 standingPosition
    {
        get
        {
            return position + new Vector3(0, row.dimension * 0.5f, 0);
        }
    }

    IGridLocus IGridLocus.relativeLocus(Dir dir, int additionalIteration)
    {

        //if (additionalIteration != 0)//CM_Deb"called with iter" + additionalIteration.ToString());
        if (additionalIteration < 0) return this;
        IGridLocus igl = null;
        switch (dir)
        {
            case Dir.UP:
                if (row.prevRow == null) return null;
                igl = row.prevRow.GetLocus(rowOffset);
                break;
            case Dir.DOWN:
                if (row.nextRow == null) return null;
                igl = row.nextRow.GetLocus(rowOffset);
                break;
            case Dir.LEFT:
                igl = row.GetLocus(rowOffset - 1);
                break;
            case Dir.RIGHT:
                igl = row.GetLocus(rowOffset + 1);
                break;
        }

        if (igl == null) return null;
        else
        {
            return igl.relativeLocus(dir, additionalIteration - 1);
        }
    }
    Vector3 IGridLocus.StandingPosition(Dir agentRaycastDir)
    {
        int x = 0;
        int y = 0;
        float m = row.dimension * 0.5f;
        switch (agentRaycastDir)
        {
            case Dir.UP:
                y = -1;
                break;
            case Dir.DOWN:
                y = 1;
                break;
            case Dir.LEFT:
                x = 1;
                break;
            case Dir.RIGHT:
                x = -1;
                break;
            default:
                Debug.LogError("Unexpected Situation!");
                return Vector3.zero;
        }


        return position + new Vector3(x * m, y * m, 0);
    }

    bool IGridLocus.IsPartOfPassiveArea()
    {
        return isPartOfPassiveArea;
    }

    public bool IsUnbreakable()
    {
        return isUnbreakable;
    }
    public bool IsBlank()
    {
        if (thisGo == null) return true;
        return isBlank;
    }
    public void CheckAndLoadLevelIfNeeded()
    {
        LevelManager.CheckAndLoadLevelIfNeeded(needsLevel);
    }
    public int RequiredLevel()
    {
        return needsLevel;
    }
    public bool HasSeedForLevel()
    {
        return needsLevel>0;
    }

    public void SpwnEnemy()
    {
        if (spwnPrefab != null)
        {
            Vector3 spwnPosition = (this as IGridLocus).position + new Vector3(0, -LevelManager.gridUnitDimension*0.5f, 0);
            GameObject nGO = Pool.Instantiate(spwnPrefab, spwnPosition, transform.rotation);//,LevelManager.instance.enemiesRoot);
            ICharacter ch = nGO.GetComponent<ICharacter>();
            if (ch != null) ch.Init();
            spwnPrefab = null;
        }
    }

    GameObject thisGo;
    static int counter = 0;
    public void Awake()
    {
        OnAwake();
    }

    public virtual void OnAwake()
    {
        this.gameObject.name = string.Format("Terrain - {0}", counter);
        counter++;
    }
    public void Init(GridRow row, int offset, float dimension, int needsLevel, GameObject spwnPrefab, bool isAreaPassive)
    {
        //this part : necessary to add also in replace
        thisGo = gameObject;
        this.row = row;
        this.rowID = row.rowID;
        this.rowOffset = offset;
        row.locationary.Add(rowOffset, this);
        this.transform.position = position;
        this.isPartOfPassiveArea = isAreaPassive;
        //this part : will be cleaned instead in replace as not proven to be necessary so far
        this.needsLevel = needsLevel;
        //if (!isAreaPassive)
        //{
        this.spwnPrefab = spwnPrefab;
        if (!isAreaPassive && ((IGridLocus)this).IsBlank() && this.spwnPrefab == null) row.grid.possibleSpwnPoints.Add(this);
        //}
        ITerrainInitData itd = null;
        if (this as VisualBaseTerrain)
        {
            VisualBaseTerrainInitData specific = new VisualBaseTerrainInitData();
            specific.isReplacementObject = false;
            itd = specific;

        }
        OnInit(itd);
    }
    public void Replace(IGridLocus original)
    {
        //this part : necessary to copy from original
        thisGo = gameObject;
        this.row = original.row;
        this.rowID = row.rowID;
        this.rowOffset = original.rowOffset;
        row.locationary[rowOffset] = this;
        this.transform.position = position;
        this.isPartOfPassiveArea = original.IsPartOfPassiveArea();
        //this part : will be cleaned instead as not proven to be necessary so far
        this.spwnPrefab = null;
        this.needsLevel = -1;
        if (row.grid.possibleSpwnPoints.Contains(original))
            row.grid.possibleSpwnPoints.Remove(original);
        if (!original.IsPartOfPassiveArea())
        {
            if (((IGridLocus)this).IsBlank() && this.spwnPrefab == null) row.grid.possibleSpwnPoints.Add(this);
        }
        ITerrainInitData itd = null;
        if (this as VisualBaseTerrain)
        {
            VisualBaseTerrainInitData specific = new VisualBaseTerrainInitData();
            specific.isReplacementObject = true;
            itd = specific;

        }
        OnInit(itd);
    }
    public void SetSpwnPrefab(GameObject spwnPrefab)
    {
        this.spwnPrefab = spwnPrefab;
    }
    public virtual void OnInit(ITerrainInitData initData)
    {
    }



}

public interface ITerrainInitData
{

}