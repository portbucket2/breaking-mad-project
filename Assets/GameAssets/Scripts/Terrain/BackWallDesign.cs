﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackWallDesign : MonoBehaviour {
    public int x = 1;
    public int y = 1;
    public float weight = 0.05f;
    public bool useBasicBG = true;

    public void Apply(List<VisualBaseTerrain> all)
    {
        //sequence is crucial
        foreach (var item in all)
        {
            item.SetDesign_None(keepBasicBG: useBasicBG);
        }
        this.gameObject.SetActive(true);
        this.transform.localPosition = Vector3.zero;

    }
    public void Apply(VisualBaseTerrain vbt)
    {
        //sequence is crucial
        vbt.SetDesign_None(keepBasicBG: useBasicBG);
        this.gameObject.SetActive(true);
        this.transform.localPosition = Vector3.zero;

    }
}
