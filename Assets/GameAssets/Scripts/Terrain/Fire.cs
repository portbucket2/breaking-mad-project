﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class Fire : MonoBehaviour, IHitable {
    //public Transform spwnerRef;
    public IAttacker attackerRef;
    
    public Dir forceDirection = Dir.UP;

    public void OnEnable()
    {
        attackerRef = null;
    }

    public void Init(IAttacker attacker, Dir forceDir)
    {
        noFuel = false;
        attackerRef = attacker;
        //spwnerRef = spawner;
        forceDirection = forceDir;
    }
    bool noFuel;
    void OnDisable()
    {
        noFuel = true;

    }

    GameObject IHitable.GetGO()
    {
        return this.gameObject;
    }
    void IHitable.TakeHit(Dir dir, IAttacker attacker)
    {
        ////CM_Deb("Fire was touched");
        attackerRef = attacker;
        //spwnerRef = this.transform;
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = rb.velocity * -1;
    }
    float IHitable.xPosition { get { return this.transform.position.x; } }

    float IHitable.MaxHitDistance()
    {
        return 10000;
    }
    int IHitable.GetScoreOnHit()
    {
        return 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (noFuel) return;
        //if (spwnerRef)
        //{
            ////CM_Deb(other.name);
            IAttacker targetAttacker = other.gameObject.GetComponentInParent<IAttacker>();
            if (targetAttacker!=attackerRef) //(other.transform != spwnerRef  || attackerRef!=null)
            {
                IHitable hitable = other.gameObject.GetComponentInParent<IHitable>();
                if(hitable!=null)Explode(hitable);
                
            }
        //}
    }

    public void Explode(IHitable hitable)
    {


        SoundKeeper.PlayATS(SoundKeeper.instance.poison_explode, this.transform.position);

        LevelManager.instance.assets.canisterExplodeParticle.Play(this.transform.position, this.transform.rotation,4);
        //ParticleSystem p = Pool.Instantiate(RefMan.ST.fireParticle, this.transform.position, this.transform.rotation).GetComponent<ParticleSystem>();
        //p.Play();
        //Centralizer.Add_DelayedAct(() => { if(p!=null)Pool.Destroy(p.gameObject); }, p.main.duration);
        hitable.TakeHit(forceDirection,attackerRef);
        if ((MonoBehaviour)attackerRef) attackerRef.AddScore(hitable.GetScoreOnHit());
        Pool.Destroy(this.gameObject);
    }


}
