﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WindowTerrain : HitBaseTerrain, IHitable
{

    public Transform model;
    public Vector3 shootDirVec;
    public Dir pressurizerDir;

    PredefinedRayCast pRayCast;

    private void Start()
    {
        PlayerManager.instance.OnPlayerReachedNewRow += OnPlayerReachedNewRow;
    }
    bool searchPlayer;
    private void OnPlayerReachedNewRow(int reachedRowID)
    {
        if (reachedRowID - 1 == row.rowID)
        {
            searchPlayer = true;
        }
        else
        {
            searchPlayer = false;
        }

    }

    float IHitable.MaxHitDistance()
    {
        return 1;
    }
    public override void OnInit(ITerrainInitData initData)
    {
        searchPlayer = false;

        pRayCast = LevelManager.instance.assets.rc_PressurizerSearch;
        if (((IGridLocus)this).relativeLocus(Dir.RIGHT) == null)
        {
            model.localScale = new Vector3(-1, 1, 1);
            shootDirVec = new Vector3(-1, 0, 0);
            pressurizerDir = Dir.LEFT;
        }
        if (((IGridLocus)this).relativeLocus(Dir.LEFT) == null)
        {
            model.localScale = new Vector3(1, 1, 1);
            shootDirVec = new Vector3(1, 0, 0);
            pressurizerDir = Dir.RIGHT;
        }


        WallTerrain upwall = (this as IGridLocus).relativeLocus(Dir.UP) as WallTerrain;
        if (upwall != null) upwall.DoModelSelection();

        StopAllCoroutines();
        StartCoroutine(CallOfTheDragon());
    }
    IEnumerator CallOfTheDragon()
    {
        float rcgap = 0.1f;
        while (true)
        {
            if (searchPlayer)
            {
                if (pRayCast.Cast(this.transform.position, shootDirVec, 20))
                {
                    if (pRayCast.lastHit.collider.GetComponentInParent<PlayerManager>())
                    {
                        Pressurizer.CreatePressure(this);
                    }
                }
                yield return rcgap;
            }


            yield return null;
        }
    }
}

