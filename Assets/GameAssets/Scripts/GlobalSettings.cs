﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSettings : MonoBehaviour {
    public bool invincibility;
    public bool infiniteSaveMe;
    public float forceAmountInDownPhysics = -25f;
    public static GlobalSettings instance;
    [SerializeField] SkinnedMeshRenderer[] allAnims;
	// Use this for initialization
	void Awake () {
        instance = this;
        

    }

    //float timer;
    //[SerializeField] float GC_Spike_check_Interval = 1.5f;
    ////[SerializeField] long genInterval = 304;
    //private void Update()
    //{
    //    timer += Time.deltaTime;
    //    if (timer > GC_Spike_check_Interval)
    //    {
    //        timer = 0;

    //        //Debug.Log("total mem: "+ System.GC.);
    //    }

    //   // System.GC.Collect();
    //}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            allAnims = FindObjectsOfType<SkinnedMeshRenderer>();
        }
    }

}