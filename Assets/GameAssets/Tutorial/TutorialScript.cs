﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour {

    public static bool isTutorialRunning
    {
        get
        {
            if (instance)
            {
                if (instance.gameObject.activeSelf) return true;
            }

            return false;
        }
    }
    public static TutorialScript instance;

    private void Awake()
    {
        instance = this;
        this.gameObject.SetActive(false);
    }
    
    public static void Init()
    {
        instance.gameObject.SetActive(true);
    }

    //bool slowAttackComplete = false;
    //public static void SpeedCheckPoint(bool enemy)
    //{
    //    if (enemy)
    //    {
    //        if (!instance.slowAttackComplete)
    //        {
    //            Time.timeScale = 0.1f;
    //            instance.slowAttackComplete = true;
    //        }
    //    }
    //    else
    //    {
    //        Time.timeScale = 1.0f;
    //    }
    //}


    public Animator anim;
    //public GameObject startPanelRef;
    public InstructionType itype;

    int r { get { return PlayerManager.instance.rowIndex; } }
    int c { get { return PlayerManager.instance.colOffset; } }

    public event System.Action onTutorialIsOver;

    bool _reachedFightScene = false;
    bool _fightIsOver = false;
    private static void ReachedFightScene()
    {
        if (!instance._reachedFightScene && !instance._fightIsOver)
        {
            instance._reachedFightScene = true;
            Time.timeScale = 0.075f;
        }
    }
    public static void FightIsOver()
    {
        if (!instance._fightIsOver)
        {
            instance._fightIsOver = true;
            Time.timeScale = 1.0f;
        }
    }
    void FixedUpdate() {
        
        if (r > 19)
        {
            SetInstruction(InstructionType.NONE);
            this.gameObject.SetActive(false);
            if (onTutorialIsOver != null) onTutorialIsOver();
        }
        else if (!PlayerManager.instance.isTutorialBusy)
        {
            if (r == 2)
            {
                if (c == -2)
                {
                    SetInstruction(InstructionType.RIGHT);
                }
                else if (c == 2)
                {
                    SetInstruction(InstructionType.LEFT);
                }
                else
                {
                    SetInstruction(InstructionType.DOWN);
                }
            }
            else if (r == 3 || r == 4 || r == 10)
            {
                SetInstruction(InstructionType.DOWN);
            }
            else if (r == 5)
            {
                if (c == 1)
                    SetInstruction(InstructionType.DOWN);
                else
                    SetInstruction(InstructionType.RIGHT);
            }
            else if (r == 7)
            {
                SetInstruction(InstructionType.LEFT);
            }
            else if (r == 9)
            {
                if (c == 1)
                    SetInstruction(InstructionType.DOWN);
                else
                    SetInstruction(InstructionType.RIGHT);
            }
            else if (r == 11)
            {
                SetInstruction(InstructionType.LEFT);
                if (c == 1)
                {
                    ReachedFightScene();
                }
                else
                {
                    FightIsOver();
                }
            }
            else if (r == 15)
            {
                if (c == 0 && PlayerManager.instance.hasHitableAbove)
                    SetInstruction(InstructionType.UP);
                else
                    SetInstruction(InstructionType.RIGHT);

            }
            else
            {
                SetInstruction(InstructionType.NONE);
            }
        }
        else
        {
            ////CM_Deb"is bz");
            SetInstruction(InstructionType.NONE);
        }

	}

    void SetInstruction(InstructionType it)
    {
        anim.SetBool("U",false);
        anim.SetBool("R", false);
        anim.SetBool("D", false);
        anim.SetBool("L", false);

        switch (it)
        {
            case InstructionType.DOWN:
                anim.SetBool("D",true);
                break;
            case InstructionType.RIGHT:
                anim.SetBool("R", true);
                break;
            case InstructionType.LEFT:
                anim.SetBool("L", true);
                break;
            case InstructionType.UP:
                anim.SetBool("U", true);
                break;
        }
    }


    public enum InstructionType
    {
        NONE = 0,
        DOWN  = 1,
        RIGHT =2,
        LEFT = 3,
        UP=4,
    }

}
