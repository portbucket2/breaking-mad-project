﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class SoundKeeper : MonoBehaviour {
    public static SoundKeeper instance;
    public static HardData<bool> sfxOn;
    public static HardData<bool> musicOn;

    private GameObject audPref;

    public AudioPermanentSourceManager bg_ambient;
    public AudioPermanentSourceManager bg_shop;
    public AudioPermanentSourceManager bg_menu;

    public AudioPermanentSourceManager hero_walking;
    public AudioPermanentSourceManager hero_lightningambience;
    public AudioPermanentSourceManager boss_flying;

    bool inGameScene { get { return PlayerManager.instance; } }
    
    private void Awake()
    {
        //if (instance != null)
        //{
        //    Destroy(this.gameObject);
        //    return;
        //}
        instance = this;
        //DontDestroyOnLoad(this.gameObject);

        audPref = new GameObject("AudioPrefabSample");
        AudioSource asource = audPref.AddComponent<AudioSource>();
        asource.minDistance = 2;
        asource.maxDistance = 9f;
        asource.rolloffMode = AudioRolloffMode.Linear;
        asource.spatialBlend = 1;
        

        sfxOn = new HardData<bool>("SFX_SETTINGS", true);
        musicOn = new HardData<bool>("MUSIC_SETTINGS", true);

        //bgmusic
        //source_ambient = this.GetComponent<AudioSource>();
        //runningMusicList.Add(source_ambient);


        bg_ambient.Init(this.transform, false,0.75f,"ambient");
        bg_shop.Init(this.transform, false,0.75f,"shop");
        bg_menu.Init(this.transform, false,0.75f,"menu");
        hero_walking.Init(this.transform, false,10,"footstep");
        hero_lightningambience.Init(this.transform,false,10,"lightningball");
        boss_flying.Init(this.transform, false, 10, "bossengine");

    }



    bool nearShop = false;

    Transform lisnerTrans;
    private void Update()
    {
        if (!inGameScene) return;
        if (lisnerTrans == null)
        {
            if (Camera.main != null) lisnerTrans = PlayerManager.instance.transform;
        }
        if (lisnerTrans != null)
        {
            this.transform.position = lisnerTrans.position;
        }

        bool nowNearShop = PlayerManager.instance.rowIndex == ShopItemTerrain.shopRowIndex+2;
        bool shouldPlayMenu = PlayerManager.instance.rowIndex == 2 || PlayerManager.instance.isDead || UIManager.instance.isPaused;
        bg_ambient.UpdateState(musicOn.value && !nowNearShop && !shouldPlayMenu);
        bg_shop.UpdateState(musicOn.value && nowNearShop && !shouldPlayMenu);
        bg_menu.UpdateState(musicOn.value && shouldPlayMenu);

        hero_walking.UpdateState(sfxOn.value && PlayerManager.instance.isMoving);
        hero_lightningambience.UpdateState(sfxOn.value && PlayerManager.instance.IsAttackUpgraded());
        boss_flying.UpdateState(sfxOn.value && Pressurizer.instance.IsFlying(), Pressurizer.instance.transform);
        if (nowNearShop != nearShop)
        {
            if (!nowNearShop)
            { 
                //PlayATS(next_level);
            }
            nearShop = nowNearShop;
        }


    }
    [Header("Hit Sounds")]
    public AudTypeSetting hit_concreate;
    public AudTypeSetting hit_shatter;
    public AudTypeSetting hit_cart;
    public AudTypeSetting hit_body;
    public AudTypeSetting hit_butterfly;

    [Header("Character Sounds")]
    public AudTypeSetting hero_swish;
    public AudTypeSetting hero_lightningAttack;
    public AudTypeSetting hero_fireLanding;
    public AudTypeSetting character_landing;

    [Header("Enemy Attack Sounds")]
    public AudTypeSetting guard_attack;
    public AudTypeSetting dog_attack;
    public AudTypeSetting spider_attack;
    public AudTypeSetting porcupine_attack;
    public AudTypeSetting nurse_attack;
    public AudTypeSetting nurse_attackgas;

    [Header("Enemy Death Sounds")]
    public AudTypeSetting death_Guard;
    public AudTypeSetting death_Dog;
    public AudTypeSetting death_Spider;
    public AudTypeSetting death_Porcu;
    public AudTypeSetting death_Nurse;

    [Header("Misc Sounds")]
    public AudTypeSetting next_level;
    public AudTypeSetting poison_shoot;
    public AudTypeSetting poison_explode;
    public AudTypeSetting boss_attack;
    public AudTypeSetting piston_hit;
    public AudTypeSetting fragile_brick;

    [Header("UI Sounds")]
    public AudTypeSetting hero_newpowerup;
    public AudTypeSetting ui_tap;
    //public AudTypeSetting poison_move;

    public static void PlayHeroSound(CharacterAudioType audType, Vector3 position)
    {
        AudTypeSetting ats = null;

        switch (audType)
        {
            case CharacterAudioType.Hit_Swish:
                ats = instance.hero_swish;
                break;
            case CharacterAudioType.Hit_Lightning:
                ats = instance.hero_lightningAttack;
                break;
            case CharacterAudioType.Land_Fire:
                ats = instance.hero_fireLanding;
                break;
            case CharacterAudioType.Land_Normal:
                ats = instance.character_landing;
                break;
        }

        PlayATS(ats,position);
    }
    public static void PlayEnemySound(EnemyAudioType audType, Vector3 position)
    {
        AudTypeSetting ats = null;

        switch (audType)
        {
            case EnemyAudioType.guard_attack:
                ats = instance.guard_attack;
                break;
            case EnemyAudioType.dog_attack:
                ats = instance.dog_attack;
                break;
            case EnemyAudioType.spider_attack:
                ats = instance.spider_attack;
                break;
            case EnemyAudioType.porcupine_attack:
                ats = instance.porcupine_attack;
                break;
            case EnemyAudioType.nurse_attack:
                ats = instance.nurse_attack;
                break;
            case EnemyAudioType.nurse_attackgas:
                ats = instance.nurse_attackgas;
                break;
        }

        PlayATS(ats, position);
    }
    public static void PlayHitSound(HitAudioType audType, Vector3 position)
    {
        AudTypeSetting ats = null;

        switch (audType)
        {
            case HitAudioType.Hit_Guard:
                ats = instance.death_Guard;
                break;
            case HitAudioType.Hit_Dog:
                ats = instance.death_Dog;
                break;
            case HitAudioType.Hit_Spider:
                ats = instance.death_Spider;
                break;
            case HitAudioType.Hit_Porcupine:
                ats = instance.death_Porcu;
                break;
            case HitAudioType.Hit_Nurse:
                ats = instance.death_Nurse;
                break;

            case HitAudioType.Hit_Concreate:
                ats = instance.hit_concreate;
                break;
            case HitAudioType.Hit_Shatter:
                ats = instance.hit_shatter;
                break;
            case HitAudioType.Hit_Cart:
                ats = instance.hit_cart;
                break;
            case HitAudioType.Hit_Body:
                ats = instance.hit_body;
                break;
            case HitAudioType.Hit_Butterfly:
                ats = instance.hit_butterfly;
                break;
        }

        PlayATS(ats, position);
    }
    public static void  PlayATS(AudTypeSetting ats, Vector3 position)
    {
        if (!sfxOn.value) return;
        AudClip ac = ExtractATS(ats);
        GameObject newPlayer = Pool.Instantiate(instance.audPref, position, Quaternion.identity);
        AudioSource asource = newPlayer.GetComponent<AudioSource>();
        asource.clip = ac.clip;
        asource.volume = ac.volume;
        asource.Play();
        Centralizer.Add_DelayedAct(() => { Pool.Destroy(newPlayer); }, ac.clip.length, true);
    }

    public static AudClip ExtractATS(AudTypeSetting ats)
    {
        return ats.clipList[Random.Range(0, ats.clipList.Count)];
    }
    public static void PlayATS(AudTypeSetting ats)
    {
        PlayATS(ats, instance.transform.position);
    }
}
[System.Serializable]
public class AudioPermanentSourceManager
{
    public AudTypeSetting ats;
    AudioSource source;
    AudClip ac;
    float lerpFactor = 1;
    public void Init(Transform anchorObject, bool playOnStart,float lerpFactor, string nametag ="UnnamedSoundSource")
    {
        GameObject go = new GameObject(nametag);
        go.transform.position = anchorObject.transform.position;
        go.transform.parent = anchorObject.transform;
        source = go.AddComponent<AudioSource>();
        source.loop = true;
        ac = SoundKeeper.ExtractATS(ats);
        source.clip = ac.clip;
        source.volume = ac.volume;
        if(playOnStart) source.Play();

        this.lerpFactor = lerpFactor;
    }

    public void UpdateState(bool enabled, Transform chaseTransform = null)
    {
        if (chaseTransform != null)
        {
            source.transform.position = chaseTransform.position;
        }
        if (enabled)
        {
            if (!source.isPlaying)
            {
                source.volume = 0;
                source.Play();
            }
            source.volume = Mathf.Lerp(source.volume,ac.volume,lerpFactor*Time.unscaledDeltaTime);
        }
        else
        {
            source.volume = Mathf.Lerp(source.volume, 0, lerpFactor * Time.unscaledDeltaTime);
            if (source.volume<=0)
            {
                source.Stop();
            }
        }
    }
}