﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudTypeSetting : ScriptableObject
{
    //public AudType type;
    public List<AudClip> clipList;
}
[System.Serializable]
public class AudClip
{
    public AudioClip clip;
    [Range(0, 1)]
    public float volume;
}

public enum HitAudioType
{



    Hit_Guard = 1001,
    Hit_Dog = 1002,
    Hit_Spider = 1003,
    Hit_Porcupine = 1004,
    Hit_Nurse = 1005,

    Hit_Concreate = 2001,
    Hit_Shatter = 2002,
    Hit_Body = 2003,
    Hit_Cart = 2004,
    Hit_Butterfly = 2005,
}


public enum CharacterAudioType
{



    Hit_Swish = 1001,
    Hit_Lightning =1002,
    Land_Normal = 1010,
    Land_Fire =1011,
}
public enum EnemyAudioType
{
    guard_attack = 1001,
    dog_attack = 1002,
    spider_attack = 1003,
    porcupine_attack = 1004,
    nurse_attack = 1005,
    nurse_attackgas = 1006,
}
