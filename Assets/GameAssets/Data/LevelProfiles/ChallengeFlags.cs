﻿using UnityEngine;
//public enum ChallengeType
//{
//    NOTHREAT = 0,

//    FIREHEAD = 1,
//    SPIKES = 2,
//    SMASHER = 3,


//    GUARD = 101,
//    DOG = 102,
//    SPIDER = 103,
//    PORCUPINE = 104,
//    NURSE = 151,
//}
public enum EnemyType
{
    NONE = -1,
    
    GUARD = 101,
    DOG = 102,
    SPIDER = 103,
    PORCUPINE = 104,
    NURSE = 151,
}
//[System.Flags]
public enum ChallengeType
{
    NOTHREAT = (1 << 0),
    FIREHEAD = (1 << 1),
    SPIKES = (1 << 2),
    SMASHER = (1 << 3),

    GUARD = (1 << 11),
    DOG = (1 << 12),
    SPIDER = (1 << 13),
    PORCUPINE = (1 << 14),
    NURSE = (1 << 15),
}
//public class EnumFlagsAttribute : PropertyAttribute
//{
//    public EnumFlagsAttribute() { }
//}