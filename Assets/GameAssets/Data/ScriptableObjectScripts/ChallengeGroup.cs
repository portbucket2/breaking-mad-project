﻿using System.Collections.Generic;
using UnityEngine;
using FishSpace;



[System.Serializable]
public class ChallengeGroup // : ScriptableObject
{

    public int pseudoSize=10;
    public int pseudoStrength=10;

    public List<ChallengeChance> challengeOptions = new List<ChallengeChance>();

    [System.NonSerialized] public LevelAssets assetsRef;
    [System.NonSerialized] public PseudoChancedList<ChallengeType> challengeList;
    public void Refresh()
    {
        challengeList = new PseudoChancedList<ChallengeType>();
        challengeList.Refresh(pseudoSize, pseudoStrength);
        foreach (var item in challengeOptions)
        {
            challengeList.Add(item.type, item.chance);
        }
        challengeList.Normalize();
        EnemyInit();
    }
    public ChallengeType GetRandomChallenge()
    {
        if (challengeList == null)
        {
            //CM_DebError("Chance List not initialized...");
            return ChallengeType.NOTHREAT;
        }
        return challengeList.Roll();
    }

    public bool canSpawnRandomEnemy;
    private void EnemyInit()
    {
        canSpawnRandomEnemy = false;
        enemyList = new PseudoChancedList<EnemyType>();
        enemyList.Refresh(pseudoSize, pseudoStrength);
        foreach (var item in challengeOptions)
        {
            EnemyType et = EnemyType.NONE;
            switch (item.type)
            {
                case ChallengeType.GUARD:
                    et = EnemyType.GUARD;
                    canSpawnRandomEnemy = true;
                    break;
                case ChallengeType.DOG:
                    et = EnemyType.DOG;
                    canSpawnRandomEnemy = true;
                    break;
                case ChallengeType.SPIDER:
                    et = EnemyType.SPIDER;
                    canSpawnRandomEnemy = true;
                    break;
                case ChallengeType.PORCUPINE:
                    et = EnemyType.PORCUPINE;
                    canSpawnRandomEnemy = true;
                    break;
                //case ChallengeType.NURSE:
                //    et = EnemyType.NURSE;
                    break;
                default:
                    continue;
            }
            enemyList.Add(et, item.chance);
        }
        enemyList.Normalize();
    }

    [System.NonSerialized] public PseudoChancedList<EnemyType> enemyList;
    public GameObject GetRandomEnemy()
    {
        if (!canSpawnRandomEnemy) return null;
        EnemyType et = EnemyType.NONE;
        if (enemyList == null)
        {
            //CM_DebError("Chance List not initialized...");
            return null;
        }
        et = enemyList.Roll();

        return PrefabForEnemyType(et);

    }

    public GameObject GetEnemyForChallengeType(ChallengeType type)
    {
        EnemyType et = EnemyType.NONE;
        switch (type)
        {
            case ChallengeType.GUARD:
                et = EnemyType.GUARD;
                break;
            case ChallengeType.DOG:
                et = EnemyType.DOG;
                break;
            case ChallengeType.SPIDER:
                et = EnemyType.SPIDER;
                break;
            case ChallengeType.PORCUPINE:
                et = EnemyType.PORCUPINE;
                break;
            case ChallengeType.NURSE:
                et = EnemyType.NURSE;
                break;
        }

        return PrefabForEnemyType(et);
    }
    private GameObject PrefabForEnemyType(EnemyType et)
    {
        switch (et)
        {
            case EnemyType.GUARD:
                return assetsRef.enemyGuard;
            case EnemyType.DOG:
                return assetsRef.enemyHog;
            case EnemyType.SPIDER:
                return assetsRef.enemySpider;
            case EnemyType.PORCUPINE:
                return assetsRef.enemyCrab;
            case EnemyType.NURSE:
                return assetsRef.enemyDragon;
            default:
                return null;
        }
    }
}
[System.Serializable]
public class ChallengeChance
{
    public ChallengeType type;
    public float chance = 1;
}
