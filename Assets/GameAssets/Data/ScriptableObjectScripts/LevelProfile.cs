﻿using System.Collections.Generic;
using UnityEngine;
using FishSpace;



[System.Serializable]
public class LevelProfile : ScriptableObject
{

    int usingLevelIndex;
    public int colCount;
    public int rowCount;
    public int transitionRows;

    public int pseudoSize=10;
    public int pseudoStrength=10;

    public int butterflyCount = 9;
    //public float randomEnemySpwnChance = 0.005f;
    public float hayCartSpwnChance = 0.001f;


    public void Initialize(int levelIndex)
    {
        usingLevelIndex = levelIndex;
    }
    private const float _minbossFlySpeed = 0.7f;
    private const float _maxbossFlySpeed = 1.55f;
    public float GetBossFlySpeed()
    {
        return Mathf.Lerp(_minbossFlySpeed, _maxbossFlySpeed, Mathf.Clamp01(usingLevelIndex / 10.0f));
    }
    private const float _minWindowChance = 0.2f;
    private const float _maxWindowChance = 0.35f;
    public float GetWindowChance()
    {
        return Mathf.Lerp(_minWindowChance, _maxWindowChance, Mathf.Clamp01(1 - (1.0f / (usingLevelIndex + 1))));
    }
    private const float _minEnemeyChance = 0.05f;
    private const float _maxEnemyChance = 0.1f;
    public float forceRandomEnemyChance = -1;
    public float GetRandomEnemyChance()
    {
        if (forceRandomEnemyChance >= 0) return forceRandomEnemyChance;
        return Mathf.Lerp(_minEnemeyChance, _maxEnemyChance, Mathf.Clamp01(usingLevelIndex / 10.0f));
    }



    //[SerializeField] [EnumFlagsAttribute] public ChallengeType mandatory;
    //[SerializeField] [EnumFlagsAttribute] public ChallengeType optional;


    public List<ChallengeType> mandatoryList = new List<ChallengeType>();
    public List<ChallengeType> optionalList = new List<ChallengeType>();
    public int maxChallengeTypes = 2;
    public int minChallengeTypes = 1;


    List<ChallengeType> tempOptList = new List<ChallengeType>();

    ChallengeType[] finalList;
    public void SetRandomChallenge(LevelAssets assetsRef)
    {
        //optionalList.Clear();
        //mandatoryList.Clear();

        //foreach (ChallengeType item in System.Enum.GetValues(typeof(ChallengeType)))
        //{
        //    if ((optional & item) != 0)
        //    {
        //        optionalList.Add(item);
        //    }
        //}
        //foreach (ChallengeType item in System.Enum.GetValues(typeof(ChallengeType)))
        //{
        //    if ((mandatory & item) != 0)
        //    {
        //        mandatoryList.Add(item);
        //    }
        //}
        ////CM_Deb(optionalList.Count);
        ////CM_Deb(mandatoryList.Count);

        int min = (minChallengeTypes > mandatoryList.Count) ? minChallengeTypes : mandatoryList.Count;
        int max = (maxChallengeTypes > mandatoryList.Count) ? maxChallengeTypes : mandatoryList.Count;

        int count = Random.Range(min, max + 1);

        finalList = new ChallengeType[count];

        tempOptList.Clear();
        tempOptList.AddRange(optionalList);
        for (int i = 0; i < count; i++)
        {
            if (i < mandatoryList.Count)
                finalList[i] = mandatoryList[i];

            else
            {
                if (tempOptList.Count <= 0)
                {
                    Debug.LogError("Out of options!!!");
                    return;
                }
                int index = Random.Range(0, tempOptList.Count);
                finalList[i] = tempOptList[index];
                tempOptList.RemoveAt(index);
            }
        }

        currentChallengeGroup = new ChallengeGroup();
        currentChallengeGroup.pseudoSize = pseudoSize;
        currentChallengeGroup.pseudoStrength = pseudoStrength;
        currentChallengeGroup.assetsRef = assetsRef;
        for (int i = 0; i < finalList.Length; i++)
        {
            ////CM_Deb"{0} - {1}",i, finalList[i]);
            ChallengeChance cc = new ChallengeChance();
            cc.type = finalList[i];
            cc.chance = 1;
            currentChallengeGroup.challengeOptions.Add(cc);
        }

        currentChallengeGroup.Refresh();
    }


    //public List<ChallengeGroupChance> groupOptions = new List<ChallengeGroupChance>();

    [System.NonSerialized] public ChallengeGroup currentChallengeGroup;

    [Header("For Test Level Only")]
    public bool enableTestMode;
}
[System.Serializable]
public class ChallengeGroupChance
{
    public ChallengeGroup group;
    public float chance = 1;
}
