﻿using System.Collections.Generic;
using UnityEngine;
using FishSpace;


[System.Serializable]
public class LevelAssets : ScriptableObject
{
    public ChanceKeeper basicRowOptions;
    public ChanceKeeper rockyRowOptions;
    public ChanceKeeper spikeCoverOptions;

    public GameObject wallDefault;
    public GameObject wallWindow;
    public GameObject blankTerrain;
    public GameObject clayTerrain;
    public GameObject sandTerrain;
    public GameObject stoneTerrain;
    public GameObject spikeTerrain;
    public GameObject fireHeadTerrain;
    public GameObject rowSmasherTerrain;
    public GameObject shopItemTerrain;

    //public LayerMask allCharacterMask;
    //public LayerMask playerAndTerrain;

    public PredefinedRayCast rc_Walkable;
    public PredefinedRayCast rc_Locus;

    public PredefinedRayCast rc_HitableLayers_withColliderOrTrigger;
    public PredefinedRayCast rc_HitableLayers_withColliderWithoutTrigger;
    public PredefinedRayCast rc_TargetableCharacters;
    public PredefinedRayCast rc_PressurizerSearch;



    public GameObject butterFly;
    public GameObject haycart;
    public GameObject enemyGuard;
    public GameObject enemyHog;
    public GameObject enemySpider;
    public GameObject enemyCrab;
    public GameObject enemyDragon;


    public ParticlePlayer blockBreakDefaultParticle;
    public ParticlePlayer characterHitDefaultParticle;
    public ParticlePlayer concreteHitSparkDefaultParticle;
    public ParticlePlayer canisterExplodeParticle;

    public GameObject deathPointsCanvas;
    public GameObject comboPrefab;

}
[System.Serializable]
public class ParticlePlayer
{
    public GameObject particleRef;
    public bool hasAutoPlay;
    public void Play(Vector3 position, Quaternion rotation, float lifetime)
    {
        //if (hasAutoPlay)
        //{
        //    GameObject pg = Pool.Instantiate(particleRef, position, rotation);
        //    Centralizer.Add_DelayedAct(() => { Pool.Destroy(pg); }, lifetime);
        //}
        //else
        //{

        //    GameObject pg = Pool.Instantiate(particleRef, position, rotation);
        //    ParticleSystem ps = pg.GetComponent<ParticleSystem>();
        //    ps.Stop();
        //    ps.Play();
        //    Centralizer.Add_DelayedAct(() => { Pool.Destroy(pg); }, lifetime);
        //}
        PlaySpecific(particleRef, hasAutoPlay, position, rotation, lifetime);
    }
    public static void PlaySpecific(GameObject go, bool auto, Vector3 position, Quaternion rotation, float lifetime)
    {
        if (auto)
        {
            GameObject pg = Pool.Instantiate(go, position, rotation);
            Centralizer.Add_DelayedAct(() => {if(pg!=null) Pool.Destroy(pg); }, lifetime);
        }
        else
        {

            GameObject pg = Pool.Instantiate(go, position, rotation);
            //ParticleSystem ps = pg.GetComponent<ParticleSystem>();
            //ps.Stop();
            //ps.Play();
           // Centralizer.Add_DelayedAct(() => { if (pg != null) Pool.Destroy(pg); }, lifetime);
        }
    }
}

[System.Serializable]
public class PredefinedRayCast
{
    //private const int maxHitCount = 15;
    private class DrawSettings
    {
        public Color missColor;
        public Color hitColor;
        public float duration;
        public DrawSettings(Color hit, Color miss, float time)
        {
            missColor = miss;
            hitColor = hit;
            duration = time;
        }
    }
    [System.NonSerialized] DrawSettings ds;

    public void DebugRaySettings(Color hitColor, Color missColor, float time)
    {
        ds = new DrawSettings(hitColor,missColor,time);
    }
    public LayerMask layerMask;
    public QueryTriggerInteraction triggerHitSettings;
    private bool canDraw { get { return ds != null; } }
    //public void EnableLog(bool enable)
    //{
    //    logOn = enable;
    //}



    [System.NonSerialized] public RaycastHit[] lastHitArray ;
    private void OrderHitArrayByDistance()
    {
        if (lastHitArray == null || lastHitArray.Length <= 0) return;

        int zeroDistCount = 0;
        for (int i = 0; i < lastHitArray.Length; i++)
        {
            if (lastHitArray[i].distance < 0.001f) zeroDistCount++;
        }

        RaycastHit[] freshArray = new RaycastHit[lastHitArray.Length - zeroDistCount];
        int z = 0;
        for (int i = 0; i < lastHitArray.Length; i++)
        {
            if (lastHitArray[i].distance < 0.001f)
            {
                z++;
                continue;
            }
            else
            {
                freshArray[i - z] = lastHitArray[i];
            }
        }
        lastHitArray = freshArray;

        for (int w = 0; w < lastHitArray.Length; w++)
        {
            int nextClosestIndex = w;
            for (int i = w + 1; i < lastHitArray.Length; i++)
            {
                if (lastHitArray[i].distance < lastHitArray[nextClosestIndex].distance)
                {
                    nextClosestIndex = i;
                }
            }
            tempRCH = lastHitArray[w];
            lastHitArray[w] = lastHitArray[nextClosestIndex];
            lastHitArray[nextClosestIndex] = tempRCH;
        }

    }

    //[System.NonSerialized] public List<RaycastHit> lastHitList = new List<RaycastHit>();
    public RaycastHit lastHit { get { return lastHitArray[0]; } }
    public Vector3 point { get { return lastHit.point; } }
    public Collider collider { get { return lastHit.collider; } }

    RaycastHit tempRCH;
    public bool Cast(Vector3 source, Vector3 direction, float maxD, bool resetDrawSettings = false)
    {
        bool didHit = Physics.Raycast(source, direction, out tempRCH, maxD, layerMask, triggerHitSettings);
        lastHitArray = new RaycastHit[] { tempRCH };
        //if (logOn&&didHit) //CM_Deb(raycastHit.collider.gameObject);
        if (canDraw)
        {

            if(didHit)
                Debug.DrawLine(source, lastHit.point, ds.hitColor, ds.duration);
            else
                Debug.DrawRay(source, direction * maxD, ds.missColor, ds.duration);
            if (resetDrawSettings) ds = null;
        }
        return didHit;
    }

    public bool CastAll(Vector3 source, Vector3 direction, float maxD, bool resetDrawSettings = false)
    {
        RaycastHit[] rcha = Physics.RaycastAll(source, direction, maxD, layerMask, triggerHitSettings);
        lastHitArray = rcha;
        OrderHitArrayByDistance();
        if (canDraw)
        {
            foreach (var item in lastHitArray)
            {

                Debug.DrawLine(source, item.point, new Color(ds.hitColor.r, ds.hitColor.g, ds.hitColor.b, ds.hitColor.a / lastHitArray.Length), ds.duration);
            }
            if (lastHitArray.Length == 0)
            {
                Debug.DrawRay(source, direction * maxD, ds.missColor, ds.duration);
            }
            if (resetDrawSettings) ds = null;
        }
        return (lastHitArray.Length != 0);
    }

    public bool DoubleCastAll(PredefinedRayCast firstRC, Vector3 source, Vector3 direction, float maxD, bool resetDrawSettings = false)
    {
        firstRC.CastAll(source, direction, maxD, false);
        RaycastHit[] rcha = Physics.RaycastAll(source, direction, maxD, layerMask, triggerHitSettings);
        lastHitArray = new RaycastHit[rcha.Length + firstRC.lastHitArray.Length];

        for (int i = 0; i < rcha.Length + firstRC.lastHitArray.Length; i++)
        {
            if (i < rcha.Length)
            {
                lastHitArray[i] = rcha[i];
            }
            else
            {
                lastHitArray[i] = firstRC.lastHitArray[i - rcha.Length];
            }
        }
        OrderHitArrayByDistance();
        if (canDraw)
        {
            foreach (var item in lastHitArray)
            {

                Debug.DrawLine(source, item.point, new Color(ds.hitColor.r, ds.hitColor.g, ds.hitColor.b, ds.hitColor.a / lastHitArray.Length), ds.duration);
            }
            if (lastHitArray.Length == 0)
            {
                Debug.DrawRay(source, direction * maxD, ds.missColor, ds.duration);
            }
            if (resetDrawSettings) ds = null;
        }

        return (lastHitArray.Length != 0);
    }


    public bool SphereCast(Vector3 source, Vector3 direction, float rad, float maxD, bool resetDrawSettings = false)
    {
        bool didHit = Physics.SphereCast(source, rad, direction, out tempRCH, maxD, layerMask, triggerHitSettings);
        lastHitArray = new RaycastHit[] { tempRCH };
        //if (logOn&&didHit) //CM_Deb(raycastHit.collider.gameObject);
        if (canDraw)
        {

            if (didHit)
                Debug.DrawLine(source,lastHit.point, ds.hitColor, ds.duration);
            else
                Debug.DrawRay(source, direction * maxD, ds.missColor, ds.duration);
            if (resetDrawSettings) ds = null;
        }
        return didHit;
    }

    public bool SphereCastAll(Vector3 source, Vector3 direction, float rad, float maxD, bool resetDrawSettings = false)
    {
        RaycastHit[] rcha = Physics.SphereCastAll(source, rad, direction, maxD-rad, layerMask, triggerHitSettings);
        ////CM_Deb(rcha.Length);
        lastHitArray = rcha;
        OrderHitArrayByDistance();
        //lastHitArray = rcha.OrderBy(x=>x.distance).ToArray();
        ////CM_Deb("first " + lastHitArray.Length);
        if (canDraw)
        {
            foreach (var item in lastHitArray)
            {

                Debug.DrawLine(source, item.point, new Color(ds.hitColor.r, ds.hitColor.g, ds.hitColor.b, ds.hitColor.a / lastHitArray.Length), ds.duration);
            }
            if (lastHitArray.Length == 0)
            {
                Debug.DrawRay(source, direction * maxD, ds.missColor, ds.duration);
            }
            if (resetDrawSettings) ds = null;
        }
        return (lastHitArray.Length != 0);
        //lastHit = raycastHit;
        //if (logEnabled) //CM_Deb(raycastHit.collider.gameObject);
        //return didHit;
    }

    public bool SphereDoubleCastAll(PredefinedRayCast firstRC, Vector3 source,  Vector3 direction, float rad, float maxD, bool resetDrawSettings = false)
    {
        //lastHitList.Clear();
        firstRC.SphereCastAll(source, direction,rad, maxD);
        //lastHitList.AddRange(firstRC.lastHitList);
        RaycastHit[] rcha = Physics.SphereCastAll(source, rad, direction, maxD-rad, layerMask, triggerHitSettings);
        lastHitArray = new RaycastHit[rcha.Length + firstRC.lastHitArray.Length];

        for (int i = 0; i < rcha.Length + firstRC.lastHitArray.Length; i++)
        {
            if (i < rcha.Length)
            {
                lastHitArray[i] = rcha[i];
            }
            else
            {
                lastHitArray[i] = firstRC.lastHitArray[i - rcha.Length];
            }
        }
        OrderHitArrayByDistance();
        //lastHitArray = lastHitArray.OrderBy(x => x.distance).ToArray();
       
        ////CM_Deb(rcha.Length);
        //lastHitList.AddRange(rcha);
        //lastHitList = lastHitList.OrderBy(x =>x.distance).ToList();
        if (canDraw)
        {
            foreach (var item in lastHitArray)
            {
                
                Debug.DrawLine(source, item.point, new Color(ds.hitColor.r, ds.hitColor.g, ds.hitColor.b, ds.hitColor.a / lastHitArray.Length), ds.duration);
            }
            if (lastHitArray.Length == 0)
            {
                Debug.DrawRay(source, direction * maxD, ds.missColor, ds.duration);
            }
            if (resetDrawSettings) ds = null;
        }
        return (lastHitArray.Length != 0);
    }

    public class RCHKeeper
    {
        public RaycastHit rch;
        public bool isValid;
    }

}