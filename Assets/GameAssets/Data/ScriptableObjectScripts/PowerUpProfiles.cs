﻿using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class PowerUpProfiles : ScriptableObject
{
    public List<PowerUpProfile> powerUpList = new List<PowerUpProfile>();

    [System.NonSerialized] private static int listGeneratedForLevel=-1;
    [System.NonSerialized] private static List<int> tempList = new List<int>();
    [System.NonSerialized] private static Dictionary<int, ShopItemTerrain> finalIndexDictionary = new Dictionary<int, ShopItemTerrain>();
    private const int itemCountPerLevel = 3;
    

    public void RefreshDictionaryIfNeeded(int currentLevel)
    {
        if (currentLevel != listGeneratedForLevel)
        {
            listGeneratedForLevel = currentLevel;
            tempList.Clear();
            for (int i = 0; i < powerUpList.Count; i++)
            {
                if (powerUpList[i].IsValidForLevel(currentLevel))
                    tempList.Add(i); //all valid indexes are added
            }
            while (tempList.Count > itemCountPerLevel)
            {
                tempList.RemoveAt(Random.Range(0,tempList.Count)); // randomly removed until only 3 left
            }
            finalIndexDictionary.Clear();
            for (int i = 0; i < tempList.Count; i++)
            {
                finalIndexDictionary.Add(tempList[i], null); //indices moved to dictionary with null pairing
            }
        }
    }

    public PowerUpProfile GetProfileFor(ShopItemTerrain sit, int currentLevel)
    {
        RefreshDictionaryIfNeeded(currentLevel);
        foreach (var item in finalIndexDictionary)
        {
            if (item.Value == null)
            {
                finalIndexDictionary[item.Key] = sit; //pair is created
                return powerUpList[item.Key]; //fetching of actual data
            }
            else if (item.Value == sit)
            {
                return powerUpList[item.Key];//fetching of actual data
            }
        }
        Debug.LogWarning("Couldnt find appropriate item");
        return null;
    }
}
[System.Serializable]
public class PowerUpProfile
{
    public PowerUpType type;
    public int baseCost;
    public int minimumShopLevel = 0;
    public bool canDropOnAd = true;
    public Sprite sprite;
    public string title;
    public string description;

    private const int shopLevelOffset = 2;
    private const float priceIncrementPerShopLevel = 0.1f;
    public int Cost(int gameLevel)
    {
        int shopLevel = gameLevel - shopLevelOffset;
        float newCost = baseCost*(1 + priceIncrementPerShopLevel*shopLevel);
        return Mathf.RoundToInt(newCost);
    }
    public bool IsValidForLevel(int gameLevel)
    {
        foreach (var item in  PlayerManager.instance.buffs)
        {
            if (item.type == type) return false; 
        }
        int shopLevel = gameLevel - shopLevelOffset;
        if (shopLevel < minimumShopLevel)
        {
            return false;
        }
        return true;
    }
}

public enum PowerUpType
{
    FORTUNE,
    BOMB,
    BOMB3,
    LIGHTNING,
    FIRE,
    MAGNET,
    HARDBOOT,
    SHIELD,
}