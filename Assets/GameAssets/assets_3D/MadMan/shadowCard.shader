﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Custom/Unlit/TextureFollower" 
{
	Properties 
	{
		_Color("col", color) = (1,1,1,1)
		_ClipAmount("Clip amount", float) = 0.1
	}

	SubShader 
	{
		//Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
		//Tags { "Queue" = "Transparent" } 
		Tags { "Queue" = "Geometry" "RenderType"="Opaque"} 
		
		LOD 100

		Pass 
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				#pragma multi_compile_fwdbase


				#include "UnityCG.cginc"
				#include "AutoLight.cginc"

				struct appdata_t 
				{
					float4 vertex : POSITION;
				};

				struct v2f 
				{
					float4 vertex : SV_POSITION;
					LIGHTING_COORDS(1,2) // NEEDED FOR SHADOWS.
				};

				uniform float _ClipAmount;
				uniform float4 _Color;

				v2f vert (appdata_t v)
				{
					v2f o;
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
					o.vertex = UnityObjectToClipPos(v.vertex);
					TRANSFER_VERTEX_TO_FRAGMENT(o); // NEEDED FOR SHADOWS.
					return o;
				}

				fixed4 frag (v2f i) : SV_Target
				{
					fixed atten = LIGHT_ATTENUATION(i);// NEEDED FOR SHADOWS.
					fixed4 finalCol = _Color * atten;
					if(atten > _ClipAmount){discard;}
					return finalCol;
				}
			ENDCG
		}

		// Pass to render object as a shadow caster
		/*Pass 
		{
			Name "ShadowCaster"
			Tags { "LightMode" = "ShadowCaster" }
       
			Fog {Mode Off}
			ZWrite On ZTest LEqual Cull Off
			Offset 1, 1
 
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_shadowcaster
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"
 
				struct v2f {
					V2F_SHADOW_CASTER;
				};
 
				v2f vert( appdata_base v )
				{
					v2f o;
					TRANSFER_SHADOW_CASTER(o)
					return o;
				}
 
				float4 frag( v2f i ) : COLOR
				{
					SHADOW_CASTER_FRAGMENT(i)
				}
			ENDCG
		}*/
   

		// Pass to render object as a shadow collector
		/*Pass 
		{
			Name "ShadowCollector"
			Tags { "LightMode" = "ShadowCollector" }
       
			Fog {Mode Off}
			ZWrite On ZTest LEqual
 
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma multi_compile_shadowcollector
 
				#define SHADOW_COLLECTOR_PASS
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"
 
				struct appdata 
				{
					float4 vertex : POSITION;
				};
 
				struct v2f 
				{
					V2F_SHADOW_COLLECTOR;
				};
 
				v2f vert (appdata v)
				{
					v2f o;
					TRANSFER_SHADOW_COLLECTOR(o)
					return o;
				}
 
				fixed4 frag (v2f i) : COLOR
				{
					SHADOW_COLLECTOR_FRAGMENT(i)
				}
			ENDCG
		}*/
	}
}