﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCameraEventScript : MonoBehaviour {
    public void TalkToLoadingObject()
    {
        ////CM_Deb"Tried to call loading scene");
        if(LoadingSceneScript.instance) LoadingSceneScript.OnIntroOver();
    }
}
